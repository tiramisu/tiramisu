"config.set() or config.setoption() or option.setoption()"
from .autopath import do_autopath
do_autopath()

import pytest
from os import environ

from tiramisu.i18n import _
from tiramisu.error import display_list, ConfigError
from tiramisu.setting import owners, groups
from tiramisu import ChoiceOption, BoolOption, IntOption, FloatOption, \
    StrOption, OptionDescription, Leadership, Config, undefined, \
    Calculation, Params, ParamOption, ParamValue, ParamIndex, \
    calc_value, calc_value_property_help
from tiramisu.error import PropertiesOptionError
from tiramisu.storage import list_sessions
import warnings
from .config import config_type, get_config, event_loop


def make_description():
    gcoption = ChoiceOption('name', 'GC name', ('ref', 'framework'), 'ref')
    gcdummy = BoolOption('dummy', 'dummy', default=False)
    objspaceoption = ChoiceOption('objspace', 'Object space',
                                  ('std', 'thunk'), 'std')
    booloption = BoolOption('bool', 'Test boolean option', default=True)
    intoption = IntOption('int', 'Test int option', default=0)
    floatoption = FloatOption('float', 'Test float option', default=2.3)
    stroption = StrOption('str', 'Test string option', default="abc")
    boolop = BoolOption('boolop', 'Test boolean option op', default=True)
    wantref_option = BoolOption('wantref', 'Test requires', default=False)
    wantframework_option = BoolOption('wantframework', 'Test requires',
                                      default=False)
    gcgroup = OptionDescription('gc', '', [gcoption, gcdummy, floatoption])
    descr = OptionDescription('tiramisu', '', [gcgroup, booloption, objspaceoption,
                                               wantref_option, stroption,
                                               wantframework_option,
                                               intoption, boolop])
    return descr


#____________________________________________________________
# change with __setattr__
@pytest.mark.asyncio
async def test_attribute_access(config_type):
    "Once set, option values can't be changed again by attribute access"
    s = StrOption("string", "", default="string")
    descr = OptionDescription("options", "", [s])
    async with await Config(descr) as cfg:
        cfg = await get_config(cfg, config_type)
        # let's try to change it again
        await cfg.option('string').value.set('foo')
        assert await cfg.option('string').value.get() == 'foo'
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_mod_read_only_write():
    "default with multi is a list"
    s = StrOption("string", "", default=[], default_multi="string", multi=True)
    descr = OptionDescription("options", "", [s])
    async with await Config(descr) as config:
        async with await Config(descr) as config2:
            assert await config.property.getdefault() == {'cache', 'validator', 'warnings'}
            assert await config.property.getdefault('read_only', 'append') == {'frozen',
                                                                               'disabled',
                                                                               'validator',
                                                                               'everything_frozen',
                                                                               'mandatory',
                                                                               'empty',
                                                                               'force_store_value'}
            assert await config.property.getdefault('read_only', 'remove') == {'permissive',
                                                                               'hidden'}
            assert await config.property.getdefault('read_write', 'append') == {'frozen',
                                                                                'disabled',
                                                                                'validator',
                                                                                'hidden',
                                                                                'force_store_value'}
            assert await config.property.getdefault('read_write', 'remove') == {'permissive',
                                                                                'everything_frozen',
                                                                                'mandatory',
                                                                                'empty'}
            #
            await config.property.setdefault(frozenset(['cache']))
            await config.property.setdefault(type='read_only', when='append', properties=frozenset(['disabled']))
            await config.property.setdefault(type='read_only', when='remove', properties=frozenset(['hidden']))
            await config.property.setdefault(type='read_write', when='append', properties=frozenset(['disabled', 'hidden']))
            await config.property.setdefault(type='read_write', when='remove', properties=frozenset([]))
            with pytest.raises(ValueError):
                await config.property.setdefault(type='unknown', when='append', properties=frozenset(['disabled']))
            with pytest.raises(ValueError):
                await config.property.setdefault(type='read_only', when='unknown', properties=frozenset(['disabled']))
            with pytest.raises(TypeError):
                await config.property.setdefault(type='read_only', when='append', properties=['disabled'])

            assert await config.property.getdefault() == {'cache'}
            assert await config.property.getdefault('read_only', 'append') == {'disabled'}
            assert await config.property.getdefault('read_only', 'remove') == {'hidden'}
            assert await config.property.getdefault('read_write', 'append') == {'disabled',
                                                                          'hidden'}
            assert await config.property.getdefault('read_write', 'remove') == set([])
            #
            await config.property.read_only()
            assert await config.property.get() == {'cache', 'disabled'}
            await config.property.read_write()
            assert await config.property.get() == {'cache', 'disabled', 'hidden'}
            await config.property.read_only()
            assert await config.property.get() == {'cache', 'disabled'}
            #
            assert await config2.property.getdefault() == {'cache', 'validator', 'warnings'}
            assert await config2.property.getdefault('read_only', 'append') == {'frozen',
                                                                                'disabled',
                                                                                'validator',
                                                                                'everything_frozen',
                                                                                'mandatory',
                                                                                'empty',
                                                                                'force_store_value'}
            assert await config2.property.getdefault('read_only', 'remove') == {'permissive',
                                                                                'hidden'}
            assert await config2.property.getdefault('read_write', 'append') == {'frozen',
                                                                                 'disabled',
                                                                                 'validator',
                                                                                 'hidden',
                                                                                 'force_store_value'}
            assert await config2.property.getdefault('read_write', 'remove') == {'permissive',
                                                                                 'everything_frozen',
                                                                                 'mandatory',
                                                                                 'empty'}
            with pytest.raises(ValueError):
                await config2.property.getdefault('unknown', 'remove')
            with pytest.raises(ValueError):
                await config2.property.getdefault('read_write', 'unknown')
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_setitem(config_type):
    s = StrOption("string", "", default=["string", "sdfsdf"], default_multi="prout", multi=True)
    descr = OptionDescription("options", "", [s])
    async with await Config(descr) as cfg:
        cfg = await get_config(cfg, config_type)
        await cfg.option('string').value.set([undefined, 'foo'])
        assert await cfg.option('string').value.get() == ['string', 'foo']
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_reset(config_type):
    "if value is None, resets to default owner"
    s = StrOption("string", "", default="string")
    descr = OptionDescription("options", "", [s])
    async with await Config(descr) as cfg:
        cfg = await get_config(cfg, config_type)
        await cfg.option('string').value.set('foo')
        assert await cfg.option('string').value.get() == "foo"
        assert await cfg.option('string').owner.get() ==owners.user
        await cfg.option('string').value.reset()
        assert await cfg.option('string').value.get() == 'string'
        assert await cfg.option('string').owner.get() ==owners.default
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_reset_with_multi(config_type):
    s = StrOption("string", "", default=["string"], default_multi="string", multi=True)
    descr = OptionDescription("options", "", [s])
    async with await Config(descr) as cfg:
        cfg = await get_config(cfg, config_type)
    #    await cfg.option('string').value.set([])
        await cfg.option('string').value.reset()
        assert await cfg.option('string').value.get() == ["string"]
        assert await cfg.option('string').owner.get() =='default'
        await cfg.option('string').value.set(["eggs", "spam", "foo"])
        assert await cfg.option('string').owner.get() =='user'
        await cfg.option('string').value.set([])
        await cfg.option('string').value.reset()
    #    assert await cfg.option('string').value.get() == ["string"]
        assert await cfg.option('string').owner.get() =='default'
        with pytest.raises(ValueError):
            await cfg.option('string').value.set(None)
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_property_get_unique_empty():
    s = StrOption("string", "", default=["string"], default_multi="string", multi=True)
    s2 = StrOption("string2", "", default=["string"], default_multi="string", multi=True, properties=('notunique',))
    s3 = StrOption("string3", "", default=["string"], default_multi="string", multi=True, properties=('notempty',))
    s4 = StrOption("string4", "", default=["string"], default_multi="string", multi=True, properties=('notunique', 'notempty'))
    descr = OptionDescription("options", "", [s, s2, s3, s4])
    async with await Config(descr) as cfg:
        await cfg.property.read_write()
        assert await cfg.option('string').property.get() == {'empty', 'unique'}
        assert await cfg.option('string2').property.get() == {'empty', 'notunique'}
        assert await cfg.option('string3').property.get() == {'unique', 'notempty'}
        assert await cfg.option('string4').property.get() == {'notunique', 'notempty'}
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_property_only_raises():
    s = StrOption("string", "", default=["string"], default_multi="string", multi=True)
    intoption = IntOption('int', 'Test int option', default=0)
    hidden_property = Calculation(calc_value,
                                  Params(ParamValue('hidden'),
                                         kwargs={'condition': ParamOption(intoption),
                                                 'expected': ParamValue(1)}))
    stroption = StrOption('str', 'Test string option', default=["abc"], default_multi="abc", properties=(hidden_property,), multi=True)
    descr = OptionDescription("options", "", [s, intoption, stroption])
    async with await Config(descr) as cfg:
        await cfg.property.read_write()
        assert await cfg.option('str').property.get() == {'empty', 'unique'}
        assert await cfg.option('str').property.get(only_raises=True) == set()
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_default_with_multi():
    "default with multi is a list"
    s = StrOption("string", "", default=[], default_multi="string", multi=True)
    descr = OptionDescription("options", "", [s])
    async with await Config(descr) as cfg:
        assert await cfg.option('string').value.get() == []
        s = StrOption("string", "", default=None, default_multi="string", multi=True)
        descr = OptionDescription("options", "", [s])
    async with await Config(descr) as cfg:
        assert await cfg.option('string').value.get() == []
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_idontexist():
    descr = make_description()
    async with await Config(descr) as cfg:
        await cfg.value.dict()
        with pytest.raises(AttributeError):
            await cfg.option('idontexist').value.get()
    assert not await list_sessions()


# ____________________________________________________________
@pytest.mark.asyncio
async def test_attribute_access_with_multi(config_type):
    s = StrOption("string", "", default=["string"], default_multi="string", multi=True)
    descr = OptionDescription("options", "", [s])
    async with await Config(descr) as cfg:
        cfg = await get_config(cfg, config_type)
        await cfg.option('string').value.set(["foo", "bar"])
        assert await cfg.option('string').value.get() == ["foo", "bar"]
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_item_access_with_multi(config_type):
    s = StrOption("string", "", default=["string"], multi=True)
    descr = OptionDescription("options", "", [s])
    async with await Config(descr) as cfg:
        cfg = await get_config(cfg, config_type)
        await cfg.option('string').value.set(["foo", "bar"])
        assert await cfg.option('string').value.get() == ["foo", "bar"]
        await cfg.option('string').value.set(["changetest", "bar"])
        assert await cfg.option('string').value.get() == ["changetest", "bar"]
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_access_with_multi_default(config_type):
    s = StrOption("string", "", default=["string"], multi=True)
    descr = OptionDescription("options", "", [s])
    async with await Config(descr) as cfg:
        cfg = await get_config(cfg, config_type)
        assert await cfg.option('string').owner.get() =='default'
        await cfg.option('string').value.set(["foo", "bar"])
        assert await cfg.option('string').value.get() == ["foo", "bar"]
        assert await cfg.option('string').owner.get() =='user'
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_multi_with_requires():
    s = StrOption("string", "", default=["string"], default_multi="string", multi=True)
    intoption = IntOption('int', 'Test int option', default=0)
    hidden_property = Calculation(calc_value,
                                  Params(ParamValue('hidden'),
                                         kwargs={'condition': ParamOption(intoption),
                                                 'expected': ParamValue(1)}))
    stroption = StrOption('str', 'Test string option', default=["abc"], default_multi="abc", properties=(hidden_property,), multi=True)
    descr = OptionDescription("options", "", [s, intoption, stroption])
    async with await Config(descr) as cfg:
        await cfg.property.read_write()
        assert not 'hidden' in await cfg.option('str').property.get()
        await cfg.option('int').value.set(1)
        with pytest.raises(PropertiesOptionError):
            await cfg.option('str').value.set(['a', 'b'])
        assert 'hidden' in await cfg.forcepermissive.option('str').property.get()
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_requires_with_inverted():
    s = StrOption("string", "", default=["string"], multi=True)
    intoption = IntOption('int', 'Test int option', default=0)
    hide_property = Calculation(calc_value,
                                Params(ParamValue('hide'),
                                       kwargs={'condition': ParamOption(intoption),
                                               'expected': ParamValue(1),
                                               'reverse_condition': ParamValue(True)}))
    stroption = StrOption('str', 'Test string option', default=["abc"], default_multi="abc", properties=(hide_property,), multi=True)
    descr = OptionDescription("options", "", [s, intoption, stroption])
    async with await Config(descr) as cfg:
        assert not 'hidden' in await cfg.option('str').property.get()
        assert 'hide' in await cfg.option('str').property.get()
        await cfg.option('int').value.set(1)
        assert not 'hidden' in await cfg.option('str').property.get()
        assert not 'hide' in await cfg.option('str').property.get()
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_multi_with_requires_in_another_group():
    s = StrOption("string", "", default=["string"], multi=True)
    intoption = IntOption('int', 'Test int option', default=0)
    hidden_property = Calculation(calc_value,
                                  Params(ParamValue('hidden'),
                                         kwargs={'condition': ParamOption(intoption),
                                                 'expected': ParamValue(1)}))
    stroption = StrOption('str', 'Test string option', default=["abc"], properties=(hidden_property,), multi=True)
    descr = OptionDescription("opt", "", [stroption])
    descr2 = OptionDescription("opt2", "", [intoption, s, descr])
    async with await Config(descr2) as cfg:
        await cfg.property.read_write()
        assert not 'hidden' in await cfg.option('opt.str').property.get()
        await cfg.option('int').value.set(1)
        with pytest.raises(PropertiesOptionError):
            await cfg.option('opt.str').value.set(['a', 'b'])
        assert 'hidden' in await cfg.forcepermissive.option('opt.str').property.get()
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_multi_with_requires_in_another_group_inverse():
    s = StrOption("string", "", default=["string"], multi=True)
    intoption = IntOption('int', 'Test int option', default=0)
    hidden_property = Calculation(calc_value,
                                  Params(ParamValue('hidden'),
                                         kwargs={'condition': ParamOption(intoption),
                                                 'expected': ParamValue(1)}))
#                          requires=[{'option': intoption, 'expected': 1, 'action': 'hidden'}], multi=True)
    stroption = StrOption('str', 'Test string option', default=["abc"], properties=(hidden_property,), multi=True)
    descr = OptionDescription("opt", "", [stroption])
    descr2 = OptionDescription("opt2", "", [intoption, s, descr])
    async with await Config(descr2) as cfg:
        await cfg.property.read_write()
        assert not 'hidden' in await cfg.option('opt.str').property.get()
        await cfg.option('int').value.set(1)
        with pytest.raises(PropertiesOptionError):
            await cfg.option('opt.str').value.set(['a', 'b'])
        assert 'hidden' in await cfg.forcepermissive.option('opt.str').property.get()
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_apply_requires_from_config():
    s = StrOption("string", "", default=["string"], multi=True)
    intoption = IntOption('int', 'Test int option', default=0)
    hidden_property = Calculation(calc_value,
                                  Params(ParamValue('hidden'),
                                         kwargs={'condition': ParamOption(intoption),
                                                 'expected': ParamValue(1)}))
    stroption = StrOption('str', 'Test string option', default=["abc"], properties=(hidden_property,), multi=True)
    descr = OptionDescription("opt", "", [stroption])
    descr2 = OptionDescription("opt2", "", [intoption, s, descr])
    async with await Config(descr2) as cfg:
        await cfg.property.read_write()
        assert not 'hidden' in await cfg.option('opt.str').property.get()
        await cfg.option('int').value.set(1)
        with pytest.raises(PropertiesOptionError):
            await cfg.option('opt.str').value.get()
        assert 'hidden' in await cfg.forcepermissive.option('opt.str').property.get()
        assert 'hidden' not in await cfg.forcepermissive.option('opt.str').option.properties()
        assert 'hidden' not in await cfg.forcepermissive.option('opt.str').option.properties(only_raises=True)
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_apply_requires_with_disabled():
    s = StrOption("string", "", default=["string"], multi=True)
    intoption = IntOption('int', 'Test int option', default=0)
    disabled_property = Calculation(calc_value,
                                    Params(ParamValue('disabled'),
                                           kwargs={'condition': ParamOption(intoption),
                                                   'expected': ParamValue(1)}))
    stroption = StrOption('str', 'Test string option', default=["abc"], properties=(disabled_property,), multi=True)
    descr = OptionDescription("opt", "", [stroption])
    descr2 = OptionDescription("opt2", "", [intoption, s, descr])
    async with await Config(descr2) as cfg:
        await cfg.property.read_write()
        assert not 'disabled' in await cfg.option('opt.str').property.get()
        await cfg.option('int').value.set(1)
        with pytest.raises(PropertiesOptionError):
            await cfg.option('opt.str').value.get()
        assert 'disabled' not in await cfg.unrestraint.option('opt.str').option.properties()
        assert 'disabled' not in await cfg.unrestraint.option('opt.str').option.properties(only_raises=True)
        assert 'disabled' in await cfg.unrestraint.option('opt.str').property.get()
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_multi_with_requires_with_disabled_in_another_group():
    s = StrOption("string", "", default=["string"], multi=True)
    intoption = IntOption('int', 'Test int option', default=0)
    disabled_property = Calculation(calc_value,
                                    Params(ParamValue('disabled'),
                                           kwargs={'condition': ParamOption(intoption),
                                                   'expected': ParamValue(1)}))
    stroption = StrOption('str', 'Test string option', default=["abc"], properties=(disabled_property,), multi=True)
    descr = OptionDescription("opt", "", [stroption])
    descr2 = OptionDescription("opt2", "", [intoption, s, descr])
    async with await Config(descr2) as cfg:
        await cfg.property.read_write()
        assert not 'disabled' in await cfg.option('opt.str').property.get()
        await cfg.option('int').value.set(1)
        with pytest.raises(PropertiesOptionError):
            await cfg.option('opt.str').value.set(['a', 'b'])
        assert 'disabled' in await cfg.unrestraint.option('opt.str').property.get()
    assert not await list_sessions()


# FIXME @pytest.mark.asyncio
# FIXME async def test_multi_with_requires_that_is_multi():
# FIXME     b = IntOption('int', 'Test int option', default=[0], multi=True)
# FIXME     hidden_property = Calculation(calc_value,
# FIXME                                   Params(ParamValue('hidden'),
# FIXME                                          kwargs={'condition': ParamOption(b),
# FIXME                                                  'expected': ParamValue(1)}))
# FIXME     c = StrOption('str', 'Test string option', default=['abc'], properties=(hidden_property,), multi=True)
# FIXME     descr = OptionDescription("opt", "", [b, c])
# FIXME     descr
# FIXME     # FIXME: ValueError: requirement mal formés pour l'option "int" ne doit pas être une valeur multiple pour "str"
# FIXME     with pytest.raises(ValueError):
# FIXME         Config(descr)")
#
#
# FIXME @pytest.mark.asyncio
# FIXME async def test_multi_with_requires_that_is_multi_inverse():
# FIXME     b = IntOption('int', 'Test int option', default=[0], multi=True)
# FIXME     c = StrOption('str', 'Test string option', default=['abc'], requires=[{'option': b, 'expected': 0, 'action': 'hidden', 'inverse': True}], multi=True)
# FIXME     descr = OptionDescription("opt", "", [b, c])
# FIXME     descr
# FIXME     Config(descr)
# FIXME     # FIXME: ValueError: requirement mal formés pour l'option "int" ne doit pas être une valeur multiple pour "str"
# FIXME     with pytest.raises(ValueError):
# FIXME         Config(descr)")
#
#
# FIXME @pytest.mark.asyncio
# FIXME async def test_multi_with_requires_that_is_leadership():
# FIXME     b = IntOption('int', 'Test int option', default=[0], multi=True)
# FIXME     c = StrOption('str', 'Test string option', requires=[{'option': b, 'expected': 1, 'action': 'hidden'}], multi=True)
# FIXME     descr = Leadership("int", "", [b, c])
# FIXME     od = OptionDescription('root', '', [descr])
# FIXME     Config(od)
#
#
# FIXME @pytest.mark.asyncio
# FIXME async def test_multi_with_requires_that_is_leadership_leader():
# FIXME     b = IntOption('int', 'Test int option', multi=True)
# FIXME     c = StrOption('str', 'Test string option', requires=[{'option': b, 'expected': 1, 'action': 'hidden'}], multi=True)
# FIXME     with pytest.raises(ValueError):
# FIXME         Leadership('str', '', [c, b])")


@pytest.mark.asyncio
async def test_multi_with_requires_that_is_leadership_follower():
    b = IntOption('int', 'Test int option', default=[0], multi=True)
    c = StrOption('str', 'Test string option', multi=True)
    hidden_property = Calculation(calc_value,
                                  Params(ParamValue('hidden'),
                                         kwargs={'condition': ParamOption(c),
                                                 'index': ParamIndex(),
                                                 'expected': ParamValue('1')}))
    d = StrOption('str1', 'Test string option', properties=(hidden_property,), multi=True)
    descr = Leadership("int", "", [b, c, d])
    descr2 = OptionDescription('od', '', [descr])
    async with await Config(descr2) as cfg:
        await cfg.property.read_write()
        assert await cfg.option('int.int').value.get() == [0]
        assert await cfg.option('int.str', 0).value.get() == None
        assert await cfg.option('int.str1', 0).value.get() == None
        await cfg.option('int.int').value.set([0, 1])
        assert await cfg.option('int.int').value.get() == [0, 1]
        assert await cfg.option('int.str', 0).value.get() == None
        assert await cfg.option('int.str', 1).value.get() == None
        assert await cfg.option('int.str1', 0).value.get() == None
        assert await cfg.option('int.str1', 1).value.get() == None
        await cfg.option('int.str', 1).value.set('1')
        await cfg.property.read_only()
        assert await cfg.option('int.str1', 0).value.get() == None
        assert await cfg.option('int.str1', 1).value.get() == None
        await cfg.property.read_write()
        assert await cfg.option('int.str1', 0).value.get() == None
        with pytest.raises(PropertiesOptionError):
            await cfg.option('int.str1', 1).value.get()
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_multi_with_requires_that_is_leadership_follower_inverse():
    b = IntOption('int', 'Test int option', default=[0], multi=True)
    c = StrOption('str', 'Test string option', multi=True)
    hidden_property = Calculation(calc_value,
                                  Params(ParamValue('hidden'),
                                         kwargs={'condition': ParamOption(c),
                                                 'index': ParamIndex(),
                                                 'reverse_condition': ParamValue(True),
                                                 'expected': ParamValue(None)}))
    d = StrOption('str1', 'Test string option', properties=(hidden_property,), multi=True)
    descr = Leadership("int", "", [b, c, d])
    descr2 = OptionDescription('od', '', [descr])
    async with await Config(descr2) as cfg:
        await cfg.property.read_write()
        assert await cfg.option('int.int').value.get() == [0]
        assert await cfg.option('int.str', 0).value.get() is None
        assert await cfg.option('int.str1', 0).value.get() is None
        await cfg.option('int.int').value.set([0, 1])
        assert await cfg.option('int.int').value.get() == [0, 1]
        assert await cfg.option('int.str', 0).value.get() is None
        assert await cfg.option('int.str', 1).value.get() is None
        assert await cfg.option('int.str1', 0).value.get() is None
        assert await cfg.option('int.str1', 1).value.get() is None
        await cfg.option('int.str', 1).value.set('1')
        await cfg.property.read_only()
        assert await cfg.option('int.str1', 0).value.get() is None
        assert await cfg.option('int.str1', 1).value.get() is None
        await cfg.property.read_write()
        assert await cfg.option('int.str1', 0).value.get() is None
        with pytest.raises(PropertiesOptionError):
            await cfg.option('int.str1', 1).value.get()
    assert not await list_sessions()


#@pytest.mark.asyncio
#async def test_multi_with_requires_that_is_not_same_leadership():
#    b = IntOption('int', 'Test int option', default=[0], multi=True)
#    hidden_property = Calculation(calc_value,
#                                  Params(ParamValue('hidden'),
#                                         kwargs={'condition': ParamOption(b),
#                                                 'index': ParamIndex(),
#                                                 'expected': ParamValue(1)}))
#    c = StrOption('str', 'Test string option', properties=(hidden_property,), multi=True)
#    #c = StrOption('str', 'Test string option', requires=[{'option': b, 'expected': 1, 'action': 'hidden'}], multi=True)
#    descr1 = Leadership("int", "", [b, c])
#    d = IntOption('int1', 'Test int option', default=[0], multi=True)
#    e = StrOption('str', 'Test string option', requires=[{'option': b, 'expected': 1, 'action': 'hidden'}], multi=True)
#    descr2 = Leadership("int1", "", [d, e])
#    descr3 = OptionDescription('val', '', [descr1, descr2])
#    descr3
#    with pytest.raises(ValueError):
#        Config(descr3)")


@pytest.mark.asyncio
async def test_multi_with_bool():
    s = BoolOption("bool", "", default=[False], multi=True)
    descr = OptionDescription("options", "", [s])
    async with await Config(descr) as cfg:
        await cfg.option('bool').value.set([True, False])
        assert await cfg.option('bool').value.get() == [True, False]
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_choice_access_with_multi():
    ch = ChoiceOption("t1", "", ("a", "b"), default=["a"], multi=True, properties=('notunique',))
    descr = OptionDescription("options", "", [ch])
    async with await Config(descr) as cfg:
        await cfg.option('t1').value.set(["a", "b", "a", "b"])
        assert await cfg.option('t1').value.get() == ["a", "b", "a", "b"]
    assert not await list_sessions()


#____________________________________________________________
@pytest.mark.asyncio
async def test_accepts_multiple_changes_from_option():
    s = StrOption("string", "", default="string")
    descr = OptionDescription("options", "", [s])
    async with await Config(descr) as cfg:
        await cfg.option('string').value.set("egg")
        assert await cfg.option('string').option.default() == "string"
        assert await cfg.option('string').value.get() == "egg"
        await cfg.option('string').value.set('blah')
        assert await cfg.option('string').option.default() == "string"
        assert await cfg.option('string').value.get() == "blah"
        await cfg.option('string').value.set('bol')
        assert await cfg.option('string').value.get() == 'bol'
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_allow_multiple_changes_from_config():
    """
    a `setoption` from the config object is much like the attribute access,
    except the fact that value owner can bet set
    """
    s = StrOption("string", "", default="string")
    s2 = StrOption("string2", "", default="string")
    suboption = OptionDescription("bip", "", [s2])
    descr = OptionDescription("options", "", [s, suboption])
    async with await Config(descr) as cfg:
        await cfg.option('string').value.set("oh")
        assert await cfg.option('string').value.get() == "oh"
        await cfg.option('string').value.set("blah")
        assert await cfg.option('string').value.get() == "blah"
    assert not await list_sessions()


# ____________________________________________________________
# accessing a value by the get method
@pytest.mark.asyncio
async def test_access_by_get():
    descr = make_description()
    async with await Config(descr) as cfg:
        with pytest.raises(AttributeError):
            list(await cfg.option.find('idontexist'))
        ret = await cfg.option.find('wantref', first=True)
        assert await ret.value.get() is False
        ret = await cfg.option.find('dummy', first=True)
        assert await ret.value.get() is False
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_access_by_get_whith_hide():
    b1 = BoolOption("b1", "", properties=(('hidden'),))
    descr = OptionDescription("opt", "",
                              [OptionDescription("sub", "",
                                                 [b1, ChoiceOption("c1", "", ('a', 'b', 'c'), 'a'),
                                                  BoolOption("d1", "")]),
                               BoolOption("b2", ""),
                               BoolOption("d1", "")])
    async with await Config(descr) as cfg:
        await cfg.property.read_write()
        with pytest.raises(AttributeError):
            ret = await cfg.option.find('b1')
            await ret.value.get()
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_append_properties():
    descr = make_description()
    async with await Config(descr) as cfg:
        assert await cfg.option('gc.dummy').property.get() == set()
        await cfg.option('gc.dummy').property.add('test')
        assert await cfg.option('gc.dummy').property.get() == {'test'}
        with pytest.raises(ConfigError):
            await cfg.option('gc.dummy').property.add('force_store_value')
        assert await cfg.option('gc.dummy').property.get() == {'test'}
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_reset_properties():
    descr = make_description()
    async with await Config(descr) as cfg:
        assert await cfg.option('gc.dummy').property.get() == set()
        await cfg.option('gc.dummy').property.add('frozen')
        assert await cfg.option('gc.dummy').property.get() == {'frozen'}
        await cfg.option('gc.dummy').property.reset()
        assert await cfg.option('gc.dummy').property.get() == set()
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_properties_cached():
    b1 = BoolOption("b1", "", properties=('test',))
    descr = OptionDescription("opt", "", [OptionDescription("sub", "", [b1])])
    async with await Config(descr) as cfg:
        await cfg.property.read_write()
        assert await cfg.option('sub.b1').property.get() == {'test'}
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_append_properties_force_store_value():
    gcdummy = BoolOption('dummy', 'dummy', default=False, properties=('force_store_value',))
    gcgroup = OptionDescription('gc', '', [gcdummy])
    descr = OptionDescription('tiramisu', '', [gcgroup])
    async with await Config(descr) as cfg:
        assert await cfg.option('gc.dummy').property.get() == {'force_store_value'}
        await cfg.option('gc.dummy').property.add('test')
        assert await cfg.option('gc.dummy').property.get() == {'force_store_value', 'test'}
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_reset_properties_force_store_value():
    gcdummy = BoolOption('dummy', 'dummy', default=False, properties=('force_store_value',))
    gcgroup = OptionDescription('gc', '', [gcdummy])
    descr = OptionDescription('tiramisu', '', [gcgroup])
    async with await Config(descr) as cfg:
        assert await cfg.property.exportation() == {}
        await cfg.property.add('frozen')
        assert await cfg.property.exportation() == \
                {None: {None: set(('frozen', 'cache', 'validator', 'warnings'))}}
        await cfg.property.reset()
        if environ.get('TIRAMISU_STORAGE') in ['sqlite3', 'postgres']:
            assert await cfg.property.exportation() == {}
        else:
            assert await cfg.property.exportation() == {None: {}}
        await cfg.option('gc.dummy').property.add('test')
        if environ.get('TIRAMISU_STORAGE') in ['sqlite3', 'postgres']:
            assert await cfg.property.exportation() == {'gc.dummy': {None: set(('test', 'force_store_value'))}}
        else:
            assert await cfg.property.exportation() == {None: {}, 'gc.dummy': {None: set(('test', 'force_store_value'))}}
        await cfg.property.reset()
        if environ.get('TIRAMISU_STORAGE') in ['sqlite3', 'postgres']:
            assert await cfg.property.exportation() == {'gc.dummy': {None: set(('test', 'force_store_value'))}}
        else:
            assert await cfg.property.exportation() == {None: {}, 'gc.dummy': {None: set(('test', 'force_store_value'))}}
        await cfg.property.add('frozen')
        assert await cfg.property.exportation() == \
                {None: {None: set(('frozen', 'validator', 'cache', 'warnings'))},
                 'gc.dummy': {None: set(('test', 'force_store_value'))}}
        await cfg.property.add('frozen')
        assert await cfg.property.exportation() == \
                {None: {None: set(('frozen', 'validator', 'cache', 'warnings'))},
                 'gc.dummy': {None: set(('test', 'force_store_value'))}}
        await cfg.option('gc.dummy').property.add('test')
        assert await cfg.property.exportation() == \
                {None: {None: set(('frozen', 'validator', 'cache', 'warnings'))},
                 'gc.dummy': {None: set(('test', 'force_store_value'))}}
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_importation_force_store_value():
    gcdummy = BoolOption('dummy', 'dummy', default=False,
                         properties=('force_store_value',))
    gcgroup = OptionDescription('gc', '', [gcdummy])
    descr = OptionDescription('tiramisu', '', [gcgroup])
    async with await Config(descr) as config1:
        assert await config1.value.exportation() == [[], [], [], []]
        await config1.property.add('frozen')
        assert await config1.value.exportation() == [[], [], [], []]
        await config1.property.add('force_store_value')
        assert await config1.value.exportation() == [['gc.dummy'], [None], [False], ['forced']]
        exportation = await config1.property.exportation()
    async with await Config(descr) as config2:
        assert await config2.value.exportation() == [[], [], [], []]
        await config2.property.importation(exportation)
        assert await config2.value.exportation() == [['gc.dummy'], [None], [False], ['forced']]
        await config2.property.importation(exportation)
        assert await config2.value.exportation() == [['gc.dummy'], [None], [False], ['forced']]
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_set_modified_value():
    gcdummy = BoolOption('dummy', 'dummy', default=False, properties=('force_store_value',))
    gcgroup = OptionDescription('gc', '', [gcdummy])
    descr = OptionDescription('tiramisu', '', [gcgroup])
    async with await Config(descr) as cfg:
        assert await cfg.property.exportation() == {}
        await cfg.property.importation({None: {None: set(('frozen', 'cache', 'validator', 'warnings'))}})
        assert await cfg.property.exportation() == \
                {None: {None: set(('frozen', 'cache', 'validator', 'warnings'))}}
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_none_is_not_modified():
    gcdummy = StrOption('dummy', 'dummy', properties=('force_store_value',))
    gcdummy1 = StrOption('dummy1', 'dummy1', default="str", properties=('force_store_value',))
    gcgroup = OptionDescription('gc', '', [gcdummy, gcdummy1])
    descr = OptionDescription('tiramisu', '', [gcgroup])
    async with await Config(descr) as cfg:
        assert await cfg.value.exportation() == [[], [], [], []]
        await cfg.property.read_write()
        assert await cfg.value.exportation() == [['gc.dummy1'], [None], ['str'], ['forced']]
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_pprint():
    msg_error = _("cannot access to {0} \"{1}\" because has {2} {3}")
    msg_is_not = _('the value of "{0}" is not {1}')
    msg_is = _('the value of "{0}" is {1}')
    properties = _('properties')
    prop = _('property')

    s = StrOption("string", "", default=["string"], default_multi="string", multi=True, properties=('hidden', 'disabled'))
    s2 = StrOption("string2", "", default="string")
    s3 = StrOption("string3", "", default=["string"], default_multi="string", multi=True, properties=('hidden',))
    intoption = IntOption('int', 'Test int option', default=0)
    hidden_property = Calculation(calc_value,
                                  Params(ParamValue('hidden'),
                                         kwargs={'condition': ParamOption(intoption, todict=True),
                                                 'expected_0': ParamValue(2),
                                                 'expected_1': ParamValue(3),
                                                 'expected_2': ParamValue(4),
                                                 'reverse_condition': ParamValue(True)}),
                                  calc_value_property_help)
    disabled_property = Calculation(calc_value,
                                    Params(ParamValue('disabled'),
                                           kwargs={'condition_0': ParamOption(intoption, todict=True),
                                                   'expected_0': ParamValue(1),
                                                   'condition_1': ParamOption(s2, todict=True),
                                                   'expected_1': ParamValue('string')}),
                                    calc_value_property_help)
    stroption = StrOption('str', 'Test string option', default="abc", properties=(hidden_property, disabled_property))
#                          requires=[{'option': intoption, 'expected': 2, 'action': 'hidden', 'inverse': True},
#                                    {'option': intoption, 'expected': 3, 'action': 'hidden', 'inverse': True},
#                                    {'option': intoption, 'expected': 4, 'action': 'hidden', 'inverse': True},
#                                    {'option': intoption, 'expected': 1, 'action': 'disabled'},
#                                    {'option': s2, 'expected': 'string', 'action': 'disabled'}])

    val2 = StrOption('val2', "")
    hidden_property = Calculation(calc_value,
                                  Params(ParamValue('hidden'),
                                         kwargs={'condition': ParamOption(intoption, todict=True),
                                                 'expected': ParamValue(1)}),
                                  calc_value_property_help)
    descr2 = OptionDescription("options", "", [val2], properties=(hidden_property,))
    #descr2 = OptionDescription("options", "", [val2], requires=[{'option': intoption, 'expected': 1, 'action': 'hidden'}])

    hidden_property = Calculation(calc_value,
                                  Params(ParamValue('hidden'),
                                         kwargs={'condition': ParamOption(stroption, todict=True),
                                                 'expected': ParamValue('2'),
                                                 'reverse_condition': ParamValue(True)}),
                                  calc_value_property_help)
    val3 = StrOption('val3', "", properties=(hidden_property,))
    #val3 = StrOption('val3', "", requires=[{'option': stroption, 'expected': '2', 'action': 'hidden', 'inverse': True}])

    descr = OptionDescription("options", "", [s, s2, s3, intoption, stroption, descr2, val3])
    async with await Config(descr) as cfg:
        await cfg.property.read_write()
        await cfg.option('int').value.set(1)
        err = None
        try:
            await cfg.option('str').value.get()
        except PropertiesOptionError as error:
            err = error

        list_disabled = '"disabled" (' + display_list([msg_is.format('Test int option', '"1"'), msg_is.format('string2', '"string"')], add_quote=False) + ')'
        list_hidden = '"hidden" (' + msg_is_not.format('Test int option', display_list([2, 3, 4], 'or', add_quote=True)) + ')'
        assert str(err) == _(msg_error.format('option', 'Test string option', properties, display_list([list_disabled, list_hidden], add_quote=False)))
        del err

        err = None
        try:
            await cfg.option('options.val2').value.get()
        except PropertiesOptionError as error:
            err = error

        assert str(err) == msg_error.format('optiondescription', 'options', prop, '"hidden" (' + msg_is.format('Test int option', '"1"') + ')')

        #err = None
        #try:
        #    await cfg.option('val3').value.get()
        #except PropertiesOptionError as error:
        #    err = error

        #msg_1 = msg_is.format('string2', 'string')
        #msg_2 = msg_is.format('Test int option', 1)
        #msg_3 = msg_is_not.format('Test int option', display_list([2, 3, 4], 'or', add_quote=True))
        #list_hidden = '"hidden" (' + display_list([msg_2, msg_3, msg_1]) + ')'

        #assert str(err) == msg_error.format('option', 'val3', prop, list_hidden)

        err = None
        try:
            await cfg.option('string').value.get()
        except Exception as error:
            err = error

        assert str(err) == msg_error.format('option', 'string', properties, display_list(['disabled', 'hidden'], add_quote=True))
        del err

        err = None
        try:
            await cfg.option('string3').value.get()
        except Exception as error:
            err = error

        assert str(err) == msg_error.format('option', 'string3', prop, '"hidden"')
        del err
    assert not await list_sessions()
