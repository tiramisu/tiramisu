from .autopath import do_autopath
do_autopath()
from tiramisu import BoolOption, StrOption, SymLinkOption, OptionDescription, DynOptionDescription, \
    Calculation, Params, ParamOption, ParamValue, calc_value, Config
from pickle import dumps
import pytest
import sys, warnings
from tiramisu.storage import list_sessions
from .config import event_loop


def test_diff_opt():
    b = BoolOption('b', '')
    disabled_property = Calculation(calc_value,
                                    Params(ParamValue('disabled'),
                                           kwargs={'condition': ParamOption(b),
                                                   'expected': ParamValue(True),
                                                   'reverse_condition': ParamValue(True)}))
    u = StrOption('u', '', properties=(disabled_property,))
    s = SymLinkOption('s', u)
    o = OptionDescription('o', '', [b, u, s])
    o1 = OptionDescription('o1', '', [o])

    with pytest.raises(NotImplementedError):
        dumps(o1)


@pytest.mark.asyncio
async def test_diff_information_config():
    b = BoolOption('b', '')
    b.impl_set_information('info', 'oh')
    b.impl_set_information('info1', 'oh')
    b.impl_set_information('info2', 'oh')
    o = OptionDescription('o', '', [b])
    o1 = OptionDescription('o1', '', [o])
    async with await Config(o1) as cfg:
        c = cfg._config_bag.context
        with pytest.raises(NotImplementedError):
            dumps(c)
    assert not await list_sessions()


def test_only_optiondescription():
    b = BoolOption('b', '')
    b
    with pytest.raises(NotImplementedError):
        dumps(b)
