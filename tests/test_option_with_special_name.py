#this test is much more to test that **it's there** and answers attribute access
from .autopath import do_autopath
do_autopath()

import pytest
from tiramisu import BoolOption, OptionDescription, ChoiceOption,\
    IntOption, FloatOption, StrOption, Config
from tiramisu.storage import list_sessions
from .config import config_type, get_config, event_loop


def make_description():
    gcoption = ChoiceOption('name', 'GC name', ['ref', 'framework'], 'ref')
    gcdummy = BoolOption('dummy', 'dummy', default=False)
    gcdummy2 = BoolOption('hide', 'dummy', default=True)
    objspaceoption = ChoiceOption('objspace', 'Object space',
                                  ['std', 'thunk'], 'std')
    booloption = BoolOption('bool', 'Test boolean option', default=True)
    intoption = IntOption('int', 'Test int option', default=0)
    floatoption = FloatOption('float', 'Test float option', default=2.3)
    stroption = StrOption('str', 'Test string option', default="abc")
    boolop = BoolOption('boolop', 'Test boolean option op', default=True)
    wantref_option = BoolOption('wantref', 'Test requires', default=False)
    wantframework_option = BoolOption('wantframework', 'Test requires',
                                      default=False)

    gcgroup = OptionDescription('gc', '', [gcoption, gcdummy, floatoption, gcdummy2])
    descr = OptionDescription('tiram', '', [gcgroup, booloption, objspaceoption,
                                            wantref_option, stroption,
                                            wantframework_option,
                                            intoption, boolop])
    return descr


@pytest.mark.asyncio
async def test_root_config_answers_ok(config_type):
    "if you hide the root config, the options in this namespace behave normally"
    gcdummy = BoolOption('dummy', 'dummy', default=False)
    boolop = BoolOption('boolop', 'Test boolean option op', default=True)
    descr = OptionDescription('tiramisu', '', [gcdummy, boolop])
    async with await Config(descr) as cfg:
        cfg = await get_config(cfg, config_type)
        #settings = await cfg.cfgimpl_get_settings()
        #settings.append('hidden')

        assert await cfg.option('dummy').value.get() is False
        assert await cfg.option('boolop').value.get() is True
    assert not await list_sessions()


#@pytest.mark.asyncio
# async def test_optname_shall_not_start_with_numbers():
#    raises(ValueError, "gcdummy = BoolOption('123dummy', 'dummy', default=False)")
#    raises(ValueError, "descr = OptionDescription('123tiramisu', '', [])")
#
#
@pytest.mark.asyncio
async def test_option_has_an_api_name(config_type):
    b = BoolOption('impl_has_dependency', 'dummy', default=True)
    descr = OptionDescription('tiramisu', '', [b])
    async with await Config(descr) as cfg:
        cfg = await get_config(cfg, config_type)
        assert await cfg.option('impl_has_dependency').value.get() is True
        assert b.impl_has_dependency() is False
    assert not await list_sessions()
