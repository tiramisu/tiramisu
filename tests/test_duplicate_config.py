# coding: utf-8
from .autopath import do_autopath
do_autopath()
import pytest

from tiramisu.setting import groups
from tiramisu import Config, MetaConfig, ChoiceOption, BoolOption, IntOption, \
    StrOption, OptionDescription, groups
from tiramisu.storage import list_sessions
from .config import event_loop


def make_description():
    numero_etab = StrOption('numero_etab', "identifiant de l'établissement")
    nom_machine = StrOption('nom_machine', "nom de la machine", default="eoleng")
    nombre_interfaces = IntOption('nombre_interfaces', "nombre d'interfaces à activer",
                                  default=1)
    activer_proxy_client = BoolOption('activer_proxy_client', "utiliser un proxy",
                                      default=False)
    mode_conteneur_actif = BoolOption('mode_conteneur_actif', "le serveur est en mode conteneur",
                                      default=False)
    mode_conteneur_actif2 = BoolOption('mode_conteneur_actif2', "le serveur est en mode conteneur2",
                                       default=False, properties=('hidden',))

    adresse_serveur_ntp = StrOption('serveur_ntp', "adresse serveur ntp", multi=True)
    time_zone = ChoiceOption('time_zone', 'fuseau horaire du serveur',
                             ('Paris', 'Londres'), 'Paris')
    wantref_option = BoolOption('wantref', 'Test requires', default=False, properties=('force_store_value',))

    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé")
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau")

    leader = OptionDescription('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    interface1 = OptionDescription('interface1', '', [leader])

    general = OptionDescription('general', '', [numero_etab, nom_machine,
                                nombre_interfaces, activer_proxy_client,
                                mode_conteneur_actif, mode_conteneur_actif2,
                                adresse_serveur_ntp, time_zone, wantref_option])
    new = OptionDescription('new', '', [], properties=('hidden',))
    creole = OptionDescription('creole', 'first tiramisu configuration', [general, interface1, new])
    descr = OptionDescription('baseconfig', 'baseconifgdescr', [creole])
    return descr


def to_tuple(val):
    return tuple([tuple(v) for v in val])


@pytest.mark.asyncio
async def test_copy():
    od = make_description()
    async with await Config(od) as cfg:
        async with await cfg.config.copy() as ncfg:
            assert await cfg.option('creole.general.numero_etab').value.get() == None
            await cfg.option('creole.general.numero_etab').value.set('oui')
            assert await cfg.option('creole.general.numero_etab').value.get() == 'oui'
            assert await ncfg.option('creole.general.numero_etab').value.get() == None
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_copy_information():
    od = make_description()
    async with await Config(od) as cfg:
        await cfg.information.set('key', 'value')
        async with await cfg.config.copy() as ncfg:
            assert await ncfg.information.get('key') == 'value'
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_copy_force_store_value():
    od = make_description()
    async with await Config(od) as conf:
        async with await Config(od) as conf2:
            assert to_tuple(await conf.value.exportation()) == ((), (), (), ())
            assert to_tuple(await conf2.value.exportation()) == ((), (), (), ())
            #
            await conf.property.read_write()
            assert to_tuple(await conf.value.exportation()) == (('creole.general.wantref',), (None,), (False,), ('forced',))
            assert to_tuple(await conf2.value.exportation()) == ((), (), (), ())
            #
            await conf2.property.read_only()
            assert to_tuple(await conf.value.exportation()) == (('creole.general.wantref',), (None,), (False,), ('forced',))
            assert to_tuple(await conf2.value.exportation()) == (('creole.general.wantref',), (None,), (False,), ('forced',))
            #
            await conf.option('creole.general.wantref').value.set(True)
            assert to_tuple(await conf.value.exportation()) == (('creole.general.wantref',), (None,), (True,), ('user',))
            assert to_tuple(await conf2.value.exportation()) == (('creole.general.wantref',), (None,), (False,), ('forced',))
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_copy_force_store_value_metaconfig():
    descr = make_description()
    async with await MetaConfig([], optiondescription=descr) as meta:
        async with await meta.config.new(session_id='conf') as conf:
            assert await meta.property.get() == await conf.property.get()
            assert await meta.permissive.get() == await conf.permissive.get()
            await conf.property.read_write()
            assert to_tuple(await conf.value.exportation()) == (('creole.general.wantref',), (None,), (False,), ('forced',))
            assert to_tuple(await meta.value.exportation()) == ((), (), (), ())
    assert not await list_sessions()
