# coding: utf-8
"frozen and hidden values"
from .autopath import do_autopath
do_autopath()

import pytest

from tiramisu import ChoiceOption, BoolOption, IntOption, FloatOption, \
    PasswordOption, StrOption, DateOption, OptionDescription, Config, \
    Calculation, Params, ParamOption, ParamValue, calc_value
from tiramisu.error import PropertiesOptionError
from tiramisu.storage import list_sessions
from .config import config_type, get_config, event_loop


def make_description():
    gcoption = ChoiceOption('name', 'GC name', ('ref', 'framework'), 'ref')
    gcdummy = BoolOption('dummy', 'dummy', default=False, properties=(('hidden'),))
    objspaceoption = ChoiceOption('objspace', 'Object space',
                                  ('std', 'thunk'), ['std'], multi=True)
    booloption = BoolOption('bool', 'Test boolean option', default=True)
    intoption = IntOption('int', 'Test int option', default=0)
    floatoption = FloatOption('float', 'Test float option', default=2.3)
    stroption = StrOption('str', 'Test string option', default="abc")

    hidden_property = Calculation(calc_value,
                                  Params(ParamValue('hidden'),
                                         kwargs={'condition': ParamOption(gcoption),
                                                 'expected': ParamValue('ref')}))
    wantref_option = BoolOption('wantref', 'Test requires', default=False, properties=(hidden_property,))
    hidden_property = Calculation(calc_value,
                                  Params(ParamValue('hidden'),
                                         kwargs={'condition': ParamOption(gcoption),
                                                 'expected': ParamValue('framework')}))
    wantframework_option = BoolOption('wantframework', 'Test requires',
                                      default=False, properties=(hidden_property,))

    # ____________________________________________________________
    booloptiontwo = BoolOption('booltwo', 'Test boolean option two', default=False)
    subgroup = OptionDescription('subgroup', '', [booloptiontwo])
    # ____________________________________________________________

    gcgroup = OptionDescription('gc', '', [subgroup, gcoption, gcdummy, floatoption])
    descr = OptionDescription('trs', '', [gcgroup, booloption, objspaceoption,
                                          wantref_option, stroption,
                                          wantframework_option,
                                          intoption])
    return descr


# ____________________________________________________________
@pytest.mark.asyncio
async def test_is_hidden(config_type):
    descr = make_description()
    async with await Config(descr) as cfg:
        await cfg.property.read_write()
        assert not 'frozen' in await cfg.forcepermissive.option('gc.dummy').property.get()
        cfg = await get_config(cfg, config_type)
        # setattr
        with pytest.raises(PropertiesOptionError):
            await cfg.option('gc.dummy').value.get() == False
        # getattr
        with pytest.raises(PropertiesOptionError):
            await cfg.option('gc.dummy').value.get()
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_group_is_hidden(config_type):
    descr = make_description()
    async with await Config(descr) as cfg_ori:
        await cfg_ori.property.read_write()
        await cfg_ori.option('gc').property.add('hidden')
        cfg = await get_config(cfg_ori, config_type)
        with pytest.raises(PropertiesOptionError):
            await cfg.option('gc.dummy').value.get()
        if config_type == 'tiramisu-api':
            await cfg.send()
        assert 'hidden' in await cfg_ori.forcepermissive.option('gc').property.get()
        cfg = await get_config(cfg_ori, config_type)
        with pytest.raises(PropertiesOptionError):
            await cfg.option('gc.float').value.get()
        # manually set the subconfigs to "show"
        if config_type == 'tiramisu-api':
            await cfg.send()
        await cfg_ori.forcepermissive.option('gc').property.pop('hidden')
        cfg = await get_config(cfg_ori, config_type)
        assert not 'hidden' in await cfg.option('gc').property.get()
        assert await cfg.option('gc.float').value.get() == 2.3
        #dummy est en hide
        prop = []
        try:
            await cfg.option('gc.dummy').value.set(False)
        except PropertiesOptionError as err:
            prop = err.proptype
        if config_type == 'tiramisu-api':
            assert 'disabled' in prop
        else:
            assert 'hidden' in prop
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_group_is_hidden_multi(config_type):
    descr = make_description()
    async with await Config(descr) as cfg_ori:
        await cfg_ori.property.read_write()
        await cfg_ori.option('objspace').property.add('hidden')
        cfg = await get_config(cfg_ori, config_type)
        with pytest.raises(PropertiesOptionError):
            await cfg.option('objspace').value.get()
        if config_type == 'tiramisu-api':
            await cfg.send()
        assert 'hidden' in await cfg_ori.forcepermissive.option('objspace').property.get()
        cfg = await get_config(cfg_ori, config_type)
        prop = []
        try:
            await cfg.option('objspace').value.set(['std'])
        except PropertiesOptionError as err:
            prop = err.proptype
        if config_type == 'tiramisu-api':
            assert 'disabled' in prop
        else:
            assert 'hidden' in prop
        if config_type == 'tiramisu-api':
            await cfg.send()
        await cfg_ori.forcepermissive.option('objspace').property.pop('hidden')
        cfg = await get_config(cfg_ori, config_type)
        assert not 'hidden' in await cfg.option('objspace').property.get()
        await cfg.option('objspace').value.set(['std', 'thunk'])
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_global_show(config_type):
    descr = make_description()
    async with await Config(descr) as cfg:
        await cfg.property.read_write()
        await cfg.forcepermissive.option('gc.dummy').property.add('hidden')
        assert 'hidden' in await cfg.forcepermissive.option('gc.dummy').property.get()
        cfg = await get_config(cfg, config_type)
        with pytest.raises(PropertiesOptionError):
            await cfg.option('gc.dummy').value.get() == False
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_with_many_subgroups(config_type):
    descr = make_description()
    async with await Config(descr) as cfg_ori:
        #booltwo = config.unwrap_from_path('gc.subgroup.booltwo')
        #setting = config.cfgimpl_get_settings()
        cfg = await get_config(cfg_ori, config_type)
        assert not 'hidden' in await cfg.option('gc.subgroup.booltwo').property.get()
        assert await cfg.option('gc.subgroup.booltwo').value.get() is False
        if config_type == 'tiramisu-api':
            await cfg.send()
        await cfg_ori.option('gc.subgroup.booltwo').property.add('hidden')
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_password_option(config_type):
    o = PasswordOption('o', '')
    d = OptionDescription('d', '', [o])
    async with await Config(d) as cfg:
        cfg = await get_config(cfg, config_type)

        await cfg.option('o').value.set('a_valid_password')
        with pytest.raises(ValueError):
            await cfg.option('o').value.set(1)
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_date_option(config_type):
    o = DateOption('o', '')
    d = OptionDescription('d', '', [o])
    async with await Config(d) as cfg:
        cfg = await get_config(cfg, config_type)

        await cfg.option('o').value.set('2017-02-04')
        await cfg.option('o').value.set('2017-2-4')
        with pytest.raises(ValueError):
            await cfg.option('o').value.set(1)
        with pytest.raises(ValueError):
            await cfg.option('o').value.set('2017-13-20')
        with pytest.raises(ValueError):
            await cfg.option('o').value.set('2017-11-31')
        with pytest.raises(ValueError):
            await cfg.option('o').value.set('2017-12-32')
        with pytest.raises(ValueError):
            await cfg.option('o').value.set('2017-2-29')
        with pytest.raises(ValueError):
            await cfg.option('o').value.set('2-2-2017')
        with pytest.raises(ValueError):
            await cfg.option('o').value.set('2017/2/2')
    assert not await list_sessions()
