from .autopath import do_autopath
do_autopath()

import pytest

from tiramisu.setting import owners, groups
from tiramisu import ChoiceOption, BoolOption, IntOption, FloatOption, \
    StrOption, OptionDescription, SymLinkOption, Leadership, Config
from tiramisu.error import ConfigError, ConstError, PropertiesOptionError, APIError
from tiramisu.storage import list_sessions
from .config import config_type, get_config, event_loop


owners.addowner("readonly2")
owners.addowner("new2")


def make_description():
    gcoption = ChoiceOption('name', 'GC name', ['ref', 'framework'], 'ref')
    gcdummy = BoolOption('dummy', 'dummy', default=False)
    objspaceoption = ChoiceOption('objspace', 'Object space',
                                  ['std', 'thunk'], 'std')
    booloption = BoolOption('bool', 'Test boolean option', default=True)
    intoption = IntOption('int', 'Test int option', default=0)
    floatoption = FloatOption('float', 'Test float option', default=2.3)
    stroption = StrOption('str', 'Test string option', default="abc")
    boolop = BoolOption('boolop', 'Test boolean option op', default=True)
    wantref_option = BoolOption('wantref', 'Test requires', default=False)
    wantframework_option = BoolOption('wantframework', 'Test requires',
                                      default=False)

    gcgroup = OptionDescription('gc', '', [gcoption, gcdummy, floatoption])
    descr = OptionDescription('tiram', '', [gcgroup, booloption, objspaceoption,
                                            wantref_option, stroption,
                                            wantframework_option,
                                            intoption, boolop])
    return descr


@pytest.mark.asyncio
async def test_default_owner(config_type):
    gcdummy = BoolOption('dummy', 'dummy', default=False)
    descr = OptionDescription('tiramisu', '', [gcdummy])
    async with await Config(descr) as cfg:
        cfg = await get_config(cfg, config_type)
        assert await cfg.option('dummy').value.get() is False
        assert await cfg.option('dummy').owner.get() == 'default'
        await cfg.option('dummy').value.set(True)
        owner = await cfg.owner.get()
        assert await cfg.option('dummy').owner.get() == owner
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_hidden_owner():
    gcdummy = BoolOption('dummy', 'dummy', default=False, properties=('hidden',))
    descr = OptionDescription('tiramisu', '', [gcdummy])
    async with await Config(descr) as cfg:
        await cfg.property.read_write()
        #with pytest.raises(PropertiesOptionError):
        #    await cfg.forcepermissive.option('dummy').owner.get()
        #with pytest.raises(PropertiesOptionError):
        #    await cfg.option('dummy').owner.isdefault()
        #with pytest.raises(PropertiesOptionError):
        #    await cfg.forcepermissive.option('dummy').owner.isdefault()
        await cfg.permissive.add('hidden')
        await cfg.forcepermissive.option('dummy').value.get()
        await cfg.forcepermissive.option('dummy').owner.isdefault()
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_addowner(config_type):
    gcdummy = BoolOption('dummy', 'dummy', default=False)
    descr = OptionDescription('tiramisu', '', [gcdummy])
    async with await Config(descr) as cfg_ori:
        cfg = await get_config(cfg_ori, config_type)
        assert await cfg.option('dummy').value.get() is False
        assert await cfg.option('dummy').owner.get() == 'default'
        assert await cfg.option('dummy').owner.isdefault()
        if config_type == 'tiramisu-api':
            await cfg.send()
        await cfg_ori.owner.set('gen_config')
        cfg = await get_config(cfg_ori, config_type)
        await cfg.option('dummy').value.set(True)
        assert await cfg.option('dummy').owner.get() == owners.gen_config
        assert not await cfg.option('dummy').owner.isdefault()
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_addowner_multiple_time():
    owners.addowner("testowner2")
    with pytest.raises(ConstError):
        owners.addowner("testowner2")


@pytest.mark.asyncio
async def test_delete_owner():
    owners.addowner('deleted2')
    with pytest.raises(ConstError):
        del(owners.deleted2)


@pytest.mark.asyncio
async def test_owner_is_not_a_string(config_type):
    gcdummy = BoolOption('dummy', 'dummy', default=False)
    descr = OptionDescription('tiramisu', '', [gcdummy])
    async with await Config(descr) as cfg:
        cfg = await get_config(cfg, config_type)
        assert await cfg.option('dummy').value.get() is False
        assert await cfg.option('dummy').owner.get() == owners.default
        assert await cfg.option('dummy').owner.get() == 'default'
        if config_type == 'tiramisu':
            assert isinstance(await cfg.option('dummy').owner.get(), owners.Owner)
        await cfg.option('dummy').value.set(True)
        assert await cfg.option('dummy').owner.get() == 'user'
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_setowner_without_valid_owner(config_type):
    gcdummy = BoolOption('dummy', 'dummy', default=False)
    descr = OptionDescription('tiramisu', '', [gcdummy])
    async with await Config(descr) as cfg:
        cfg = await get_config(cfg, config_type)
        assert await cfg.option('dummy').value.get() is False
        assert await cfg.option('dummy').owner.get() == 'default'
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_setowner_for_value(config_type):
    gcdummy = BoolOption('dummy', 'dummy', default=False)
    descr = OptionDescription('tiramisu', '', [gcdummy])
    async with await Config(descr) as cfg_ori:
        cfg = await get_config(cfg_ori, config_type)
        assert await cfg.option('dummy').value.get() is False
        assert await cfg.option('dummy').owner.get() == 'default'
        if config_type == 'tiramisu-api':
            await cfg.send()
        with pytest.raises(ConfigError):
            await cfg_ori.option('dummy').owner.set('new2')
        cfg = await get_config(cfg_ori, config_type)
        await cfg.option('dummy').value.set(False)
        assert await cfg.option('dummy').owner.get() == owners.user
        if config_type == 'tiramisu-api':
            await cfg.send()
        await cfg_ori.option('dummy').owner.set('new2')
        cfg = await get_config(cfg_ori, config_type)
        assert await cfg.option('dummy').owner.get() == owners.new2
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_setowner_forbidden(config_type):
    gcdummy = BoolOption('dummy', 'dummy', default=False)
    descr = OptionDescription('tiramisu', '', [gcdummy])
    async with await Config(descr) as cfg_ori:
        cfg = await get_config(cfg_ori, config_type)
        assert await cfg.option('dummy').value.get() is False
        assert await cfg.option('dummy').owner.get() == 'default'
        if config_type == 'tiramisu-api':
            await cfg.send()
        with pytest.raises(ValueError):
            await cfg_ori.owner.set('default')
        cfg = await get_config(cfg_ori, config_type)
        await cfg.option('dummy').value.set(False)
        if config_type == 'tiramisu-api':
            await cfg.send()
        with pytest.raises(ValueError):
            await cfg_ori.option('dummy').owner.set('default')
        cfg = await get_config(cfg_ori, config_type)
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_setowner_read_only(config_type):
    gcdummy = BoolOption('dummy', 'dummy', default=False)
    descr = OptionDescription('tiramisu', '', [gcdummy])
    async with await Config(descr) as cfg_ori:
        await cfg_ori.property.read_write()
        cfg = await get_config(cfg_ori, config_type)
        assert await cfg.option('dummy').value.get() is False
        assert await cfg.option('dummy').owner.get() == 'default'
        await cfg.option('dummy').value.set(False)
        assert await cfg.option('dummy').owner.get() == owners.user
        if config_type == 'tiramisu-api':
            await cfg.send()
        await cfg_ori.property.read_only()
        with pytest.raises(PropertiesOptionError):
            await cfg_ori.option('dummy').owner.set('readonly2')
        cfg = await get_config(cfg_ori, config_type)
        assert await cfg.option('dummy').owner.get() == owners.user
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_setowner_optiondescription(config_type):
    gcdummy = BoolOption('dummy', 'dummy', default=False)
    descr1 = OptionDescription('tiramisu', '', [gcdummy])
    descr = OptionDescription('tiramisu', '', [descr1])
    async with await Config(descr) as cfg:
        cfg = await get_config(cfg, config_type)
        with pytest.raises(APIError):
            await cfg.option('tiramisu').owner.get()
        with pytest.raises(APIError):
            await cfg.option('tiramisu').owner.set('user')
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_setowner_symlinkoption(config_type):
    gcdummy = BoolOption('dummy', 'dummy', default=False)
    s = SymLinkOption('symdummy', gcdummy)
    descr1 = OptionDescription('tiramisu', '', [gcdummy, s])
    descr = OptionDescription('tiramisu', '', [descr1])
    async with await Config(descr) as cfg_ori:
        cfg = await get_config(cfg_ori, config_type)
        assert await cfg.option('tiramisu.symdummy').owner.isdefault()
        await cfg.option('tiramisu.dummy').value.set(True)
        assert not await cfg.option('tiramisu.symdummy').owner.isdefault()
        if config_type == 'tiramisu-api':
            await cfg.send()
        with pytest.raises(ConfigError):
            await cfg_ori.option('tiramisu.symdummy').owner.set('user')
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_owner_leadership(config_type):
    b = IntOption('int', 'Test int option', default=[0], multi=True)
    c = StrOption('str', 'Test string option', multi=True)
    descr = Leadership("int", "", [b, c])
    od = OptionDescription('od', '', [descr])
    async with await Config(od) as cfg_ori:
        with pytest.raises(ConfigError):
            await cfg_ori.option('int.str', 0).owner.set('user')
        cfg = await get_config(cfg_ori, config_type)

        await cfg.option('int.int').value.set([0, 1])
        await cfg.option('int.str', 0).value.set('yes')
        assert not await cfg.option('int.str', 0).owner.isdefault()
        assert await cfg.option('int.str', 1).owner.isdefault()
        if config_type == 'tiramisu-api':
            await cfg.send()
        await cfg_ori.option('int.str', 0).owner.set('user')
        cfg = await get_config(cfg_ori, config_type)
        assert await cfg.option('int.str', 0).owner.get() == owners.user
        assert await cfg.option('int.str', 1).owner.isdefault()
        assert await cfg.option('int.str', 0).value.get() == 'yes'
        assert await cfg.option('int.str', 1).value.get() == None
    assert not await list_sessions()
