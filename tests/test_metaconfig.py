import pytest
from .autopath import do_autopath
do_autopath()

from tiramisu.setting import groups, owners
from tiramisu import IntOption, StrOption, NetworkOption, NetmaskOption, BoolOption, ChoiceOption, \
                     IPOption, OptionDescription, Leadership, Config, GroupConfig, MetaConfig, \
                     Calculation, Params, ParamOption, ParamValue, calc_value, ParamSelfOption, \
                     valid_network_netmask, valid_not_equal
from tiramisu.error import ConfigError, ConflictError, PropertiesOptionError, LeadershipError, APIError
from tiramisu.storage import list_sessions
from .config import config_type, get_config, delete_sessions, event_loop


owners.addowner('config')
owners.addowner('meta1')
owners.addowner('meta2')


def return_value(value=None):
    return value


def return_condition(val, condition, expected):
    if condition == expected:
        return val
    return None


def raise_exception():
    raise Exception('test')


def make_description():
    i1 = IntOption('i1', '')
    i2 = IntOption('i2', '', default=1)
    i3 = IntOption('i3', '')
    i4 = IntOption('i4', '', default=2)
    i5 = IntOption('i5', '', default=[2], multi=True)
    i6 = IntOption('i6', '', properties=('disabled',))
    od1 = OptionDescription('od1', '', [i1, i2, i3, i4, i5, i6])
    od2 = OptionDescription('od2', '', [od1])
    return od2


async def make_metaconfig(double=False):
    od2 = make_description()
    conf1 = await Config(od2, session_id='conf1', delete_old_session=True)
    await conf1.property.read_write()
    conf2 = await Config(od2, session_id='conf2', delete_old_session=True)
    await conf2.property.read_write()
    meta = await MetaConfig([conf1, conf2], session_id='meta', delete_old_session=True)
    if double:
        await meta.owner.set(owners.meta2)
        meta = await MetaConfig([meta], session_id='doublemeta', delete_old_session=True)
    await meta.property.read_write()
    await meta.owner.set(owners.meta1)
    return meta


@pytest.mark.asyncio
async def test_unknown_config():
    meta = await make_metaconfig()
    with pytest.raises(ConfigError):
        await meta.config('unknown')
    await delete_sessions(meta)


@pytest.mark.asyncio
async def test_error_metaconfig():
    od2 = make_description()
    conf1 = await Config(od2, session_id='conf1')
    with pytest.raises(TypeError):
        await MetaConfig([await GroupConfig([conf1])], session_id='meta')
    await delete_sessions(conf1)


@pytest.mark.asyncio
async def test_path():
    meta = await make_metaconfig()
    assert await meta.config.path() == 'meta'
    ret = await meta.config('conf1')
    assert await ret.config.path() == 'meta.conf1'
    ret = await meta.config('conf2')
    assert await ret.config.path() == 'meta.conf2'
    await delete_sessions(meta)


@pytest.mark.asyncio
async def test_none():
    meta = await make_metaconfig()
    conf1 = await meta.config('conf1')
    conf2 = await meta.config('conf2')
    assert await meta.option('od1.i3').value.get() is await conf1.option('od1.i3').value.get() is await conf2.option('od1.i3').value.get() is None
    assert await meta.option('od1.i3').owner.get() is await conf1.option('od1.i3').owner.get() is await conf2.option('od1.i3').owner.get() is owners.default
    #
    await meta.option('od1.i3').value.set(3)
    assert await meta.option('od1.i3').value.get() == await conf1.option('od1.i3').value.get() == await conf2.option('od1.i3').value.get() == 3
    assert await meta.option('od1.i3').owner.get() is await conf1.option('od1.i3').owner.get() is await conf2.option('od1.i3').owner.get() is owners.meta1
    #
    await conf1.option('od1.i3').value.set(2)
    assert await meta.option('od1.i3').value.get() == await conf2.option('od1.i3').value.get() == 3
    assert await conf1.option('od1.i3').value.get() == 2
    assert await meta.option('od1.i3').owner.get() is await conf2.option('od1.i3').owner.get() is owners.meta1
    assert await conf1.option('od1.i3').owner.get() is owners.user
    #
    await meta.option('od1.i3').value.set(4)
    assert await meta.option('od1.i3').value.get() == await conf2.option('od1.i3').value.get() == 4
    assert await conf1.option('od1.i3').value.get() == 2
    assert await meta.option('od1.i3').owner.get() is await conf2.option('od1.i3').owner.get() is owners.meta1
    assert await conf1.option('od1.i3').owner.get() is owners.user
    #
    await meta.option('od1.i3').value.reset()
    assert await meta.option('od1.i3').value.get() is await conf2.option('od1.i3').value.get() is None
    assert await conf1.option('od1.i3').value.get() == 2
    assert await meta.option('od1.i3').owner.get() is await conf2.option('od1.i3').owner.get() is owners.default
    assert await conf1.option('od1.i3').owner.get() is owners.user
    #
    await conf1.option('od1.i3').value.reset()
    assert await meta.option('od1.i3').value.get() is await conf1.option('od1.i3').value.get() is await conf2.option('od1.i3').value.get() is None
    assert await meta.option('od1.i3').owner.get() is await conf1.option('od1.i3').owner.get() is await conf2.option('od1.i3').owner.get() is owners.default
    await delete_sessions(meta)


@pytest.mark.asyncio
async def test_metaconfig_reset(config_type):
    meta = await make_metaconfig()
    meta_api = await get_config(meta, config_type)
    assert await meta_api.option('od1.i2').value.get() == 1
    await meta_api.option('od1.i2').value.set(2)
    if config_type == 'tiramisu-api':
        meta_api.send()
    conf1 = await meta.config('conf1')
    conf2 = await meta.config('conf2')
    await conf1.option('od1.i2').value.set(3)
    assert await meta.option('od1.i2').value.get() == 2
    assert await conf1.option('od1.i2').value.get() == 3
    assert await conf2.option('od1.i2').value.get() == 2
    await meta.config.reset()
    assert await meta.option('od1.i2').value.get() == 1
    assert await conf1.option('od1.i2').value.get() == 3
    assert await conf2.option('od1.i2').value.get() == 1
    await delete_sessions(meta)


@pytest.mark.asyncio
async def test_default():
    meta = await make_metaconfig()
    conf1 = await meta.config('conf1')
    conf2 = await meta.config('conf2')
    assert await meta.option('od1.i2').value.get() == await conf1.option('od1.i2').value.get() == await conf2.option('od1.i2').value.get() == 1
    assert await meta.option('od1.i2').owner.get() is await conf1.option('od1.i2').owner.get() is await conf2.option('od1.i2').owner.get() is owners.default
    #
    await meta.option('od1.i2').value.set(3)
    assert await meta.option('od1.i2').value.get() == await conf1.option('od1.i2').value.get() == await conf2.option('od1.i2').value.get() == 3
    assert await meta.option('od1.i2').owner.get() is await conf1.option('od1.i2').owner.get() is await conf2.option('od1.i2').owner.get() is owners.meta1
    #
    await conf1.option('od1.i2').value.set(2)
    assert await meta.option('od1.i2').value.get() == await conf2.option('od1.i2').value.get() == 3
    assert await conf1.option('od1.i2').value.get() == 2
    assert await meta.option('od1.i2').owner.get() is await conf2.option('od1.i2').owner.get() is owners.meta1
    assert await conf1.option('od1.i2').owner.get() is owners.user
    #
    await meta.option('od1.i2').value.set(4)
    assert await meta.option('od1.i2').value.get() == await conf2.option('od1.i2').value.get() == 4
    assert await conf1.option('od1.i2').value.get() == 2
    assert await meta.option('od1.i2').owner.get() is await conf2.option('od1.i2').owner.get() is owners.meta1
    assert await conf1.option('od1.i2').owner.get() is owners.user
    #
    await meta.option('od1.i2').value.reset()
    assert await meta.option('od1.i2').value.get() == await conf2.option('od1.i2').value.get() == 1
    assert await conf1.option('od1.i2').value.get() == 2
    assert await meta.option('od1.i2').owner.get() is await conf2.option('od1.i2').owner.get() is owners.default
    assert await conf1.option('od1.i2').owner.get() is owners.user
    #
    await conf1.option('od1.i2').value.reset()
    assert await meta.option('od1.i2').value.get() == await conf1.option('od1.i2').value.get() == await conf2.option('od1.i2').value.get() == 1
    assert await meta.option('od1.i2').owner.get() is await conf1.option('od1.i2').owner.get() is await conf2.option('od1.i2').owner.get() is owners.default
    await delete_sessions(meta)


@pytest.mark.asyncio
async def test_contexts():
    meta = await make_metaconfig()
    errors = await meta.value.set('od1.i2', 6, only_config=True)
    assert len(errors) == 0
    conf1 = await meta.config('conf1')
    assert await meta.option('od1.i2').value.get() == 1
    assert await meta.option('od1.i2').owner.get() == owners.default
    assert await conf1.option('od1.i2').value.get() == await conf1.option('od1.i2').value.get() == 6
    assert await conf1.option('od1.i2').owner.get() == await conf1.option('od1.i2').owner.get() is owners.user
    await delete_sessions(meta)


@pytest.mark.asyncio
async def test_find():
    meta = await make_metaconfig()
    ret = list(await meta.option.find('i2'))
    assert len(ret) == 1
    assert 1 == await ret[0].value.get()
    ret = await meta.option.find('i2', first=True)
    assert 1 == await ret.value.get()
    assert await meta.value.dict() == {'od1.i4': 2, 'od1.i1': None, 'od1.i3': None,
                                      'od1.i2': 1, 'od1.i5': [2]}
    await delete_sessions(meta)


@pytest.mark.asyncio
async def test_meta_meta():
    meta = await make_metaconfig(double=True)
    meta1 = await meta.config('meta')
    conf1 = await meta.config('meta.conf1')
    conf2 = await meta.config('meta.conf2')
    assert await meta.option('od1.i2').value.get() == await meta1.option('od1.i2').value.get() == await conf1.option('od1.i2').value.get() == await conf2.option('od1.i2').value.get() == 1
    assert await meta.option('od1.i2').owner.get() is await meta1.option('od1.i2').owner.get() is await conf1.option('od1.i2').owner.get() is await conf2.option('od1.i2').owner.get() is owners.default
    #
    await meta.option('od1.i2').value.set(3)
    assert await meta.option('od1.i2').value.get() == await meta1.option('od1.i2').value.get() == await conf1.option('od1.i2').value.get() == await conf2.option('od1.i2').value.get() == 3
    assert await meta.option('od1.i2').owner.get() is await meta1.option('od1.i2').owner.get() is await conf1.option('od1.i2').owner.get() is await conf2.option('od1.i2').owner.get() is owners.meta1
    #
    await conf1.option('od1.i2').value.set(2)
    assert await meta.option('od1.i2').value.get() == await meta1.option('od1.i2').value.get() == await conf2.option('od1.i2').value.get() == 3
    assert await conf1.option('od1.i2').value.get() == 2
    assert await meta.option('od1.i2').owner.get() is await meta1.option('od1.i2').owner.get() is await conf2.option('od1.i2').owner.get() is owners.meta1
    assert await conf1.option('od1.i2').owner.get() is owners.user
    #
    await meta1.option('od1.i2').value.set(4)
    assert await meta.option('od1.i2').value.get() == 3
    assert await meta1.option('od1.i2').value.get() == await conf2.option('od1.i2').value.get() == 4
    assert await conf1.option('od1.i2').value.get() == 2
    assert await meta.option('od1.i2').owner.get() is owners.meta1
    assert await meta1.option('od1.i2').owner.get() is await conf2.option('od1.i2').owner.get() is owners.meta2
    assert await conf1.option('od1.i2').owner.get() is owners.user
    #
    await meta1.option('od1.i2').value.reset()
    assert await meta.option('od1.i2').value.get() == await meta1.option('od1.i2').value.get() == await conf2.option('od1.i2').value.get() == 3
    assert await conf1.option('od1.i2').value.get() == 2
    assert await meta.option('od1.i2').owner.get() is await meta1.option('od1.i2').owner.get() is await conf2.option('od1.i2').owner.get() is owners.meta1
    assert await conf1.option('od1.i2').owner.get() is owners.user
    #
    await meta.option('od1.i2').value.reset()
    assert await meta.option('od1.i2').value.get() == await meta1.option('od1.i2').value.get() == await conf2.option('od1.i2').value.get() == 1
    assert await conf1.option('od1.i2').value.get() == 2
    assert await meta.option('od1.i2').owner.get() is await meta1.option('od1.i2').owner.get() is await conf2.option('od1.i2').owner.get() is owners.default
    assert await conf1.option('od1.i2').owner.get() is owners.user
    #
    await conf1.option('od1.i2').value.reset()
    assert await meta.option('od1.i2').value.get() == await meta1.option('od1.i2').value.get() == await conf1.option('od1.i2').value.get() == await conf2.option('od1.i2').value.get() == 1
    assert await meta.option('od1.i2').owner.get() is await meta1.option('od1.i2').owner.get() is await conf1.option('od1.i2').owner.get() is await conf2.option('od1.i2').owner.get() is owners.default
    await delete_sessions(meta)


@pytest.mark.asyncio
async def test_meta_new_config():
    od = make_description()
    meta = await MetaConfig(['name1', 'name2'], optiondescription=od)
    assert len(list(await meta.config.list())) == 2
    await meta.config.new('newconf1')
    assert len(list(await meta.config.list())) == 3
    await delete_sessions(meta)


@pytest.mark.asyncio
async def test_meta_new_config_owner():
    od = make_description()
    meta = await MetaConfig(['name1', 'name2'], optiondescription=od)
    await meta.owner.set('meta')
    await meta.config.new('newconf1')
    assert await meta.owner.get() == 'meta'
    await delete_sessions(meta)


@pytest.mark.asyncio
async def test_meta_new_metaconfig():
    od = make_description()
    meta = await MetaConfig(['name1', 'name2'], optiondescription=od)
    await meta.config.new('newconf1', type='metaconfig')
    newconf1 = await meta.config('newconf1')
    await newconf1.config.new('newconf2', type='metaconfig')
    newconf2 = await newconf1.config('newconf2')
    await newconf2.config.new('newconf3')
    newconf3 = await newconf2.config('newconf3')
    assert await newconf3.session.id() == 'newconf3'
    await delete_sessions(meta)


@pytest.mark.asyncio
async def test_meta_pop_config():
    od = make_description()
    meta = await MetaConfig(['name1', 'name2'], optiondescription=od)
    await meta.option('od1.i1').value.set(2)
    #
    assert len(list(await meta.config.list())) == 2
    await meta.config.new('newconf1')
    newconf1 = await meta.config('newconf1')
    assert await newconf1.value.dict() == {'od1.i1': 2, 'od1.i2': 1, 'od1.i3': None, 'od1.i4': 2, 'od1.i5': [2], 'od1.i6': None}
    #
    assert len(list(await meta.config.list())) == 3
    newconf1 = await meta.config.pop('newconf1')
    try:
        await meta.config('newconf1')
    except ConfigError:
        pass
    else:
        raise Exception('must raise')
    assert await newconf1.value.dict() == {'od1.i1': None, 'od1.i2': 1, 'od1.i3': None, 'od1.i4': 2, 'od1.i5': [2], 'od1.i6': None}
    #
    assert len(list(await meta.config.list())) == 2
    with pytest.raises(ConfigError):
        await meta.config.pop('newconf1')
    await delete_sessions([meta, newconf1])


@pytest.mark.asyncio
async def test_meta_add_config():
    od = make_description()
    od2 = make_description()
    meta = await MetaConfig(['name1', 'name2'], optiondescription=od, delete_old_session=True)
    await meta.option('od1.i1').value.set(2)
    #
    assert len(list(await meta.config.list())) == 2
    config = await Config(od, session_id='new', delete_old_session=True)
    assert await config.value.dict() == {'od1.i1': None, 'od1.i2': 1, 'od1.i3': None, 'od1.i4': 2, 'od1.i5': [2], 'od1.i6': None}
    await meta.config.add(config)
    #
    assert len(list(await meta.config.list())) == 3
    assert await config.value.dict() == {'od1.i1': 2, 'od1.i2': 1, 'od1.i3': None, 'od1.i4': 2, 'od1.i5': [2], 'od1.i6': None}
    #
    with pytest.raises(ConflictError):
        await meta.config.add(config)
    newconfig = await Config(od2)
    with pytest.raises(ValueError):
        await meta.config.add(newconfig)
    await delete_sessions([meta, newconfig])


@pytest.mark.asyncio
async def test_meta_add_config_readd():
    od = make_description()
    meta = await MetaConfig([], optiondescription=od)
    meta2 = await MetaConfig([], optiondescription=od)
    config = await Config(od, session_id='new')
    #
    await meta.config.add(config)
    await meta2.config.add(config)
    assert len(list(await config.config.parents())) == 2
    await delete_sessions([meta, meta2])


@pytest.mark.asyncio
async def test_meta_new_config_wrong_name():
    od = make_description()
    meta = await MetaConfig(['name1', 'name2'], optiondescription=od)
    assert len(list(await meta.config.list())) == 2
    with pytest.raises(ConflictError):
        await meta.config.new('name1')
    assert len(list(await meta.config.list())) == 2
    await delete_sessions(meta)


@pytest.mark.asyncio
async def test_meta_load_config():
    od = make_description()
    meta = await MetaConfig(['name1', 'name2'], optiondescription=od)
    assert len(list(await meta.config.list())) == 2
    await meta.config.load('name1')
    assert len(list(await meta.config.list())) == 3
    await delete_sessions(meta)


@pytest.mark.asyncio
async def test_meta_load_config_wrong_name():
    od = make_description()
    meta = await MetaConfig(['name1', 'name2'], optiondescription=od)
    assert len(list(await meta.config.list())) == 2
    with pytest.raises(ConfigError):
        await meta.config.load('name3')
    assert len(list(await meta.config.list())) == 2
    await delete_sessions(meta)


@pytest.mark.asyncio
async def test_meta_meta_set():
    meta = await make_metaconfig(double=True)
    errors1 = await meta.value.set('od1.i1', 7, only_config=True)
    errors2 = await meta.value.set('od1.i6', 7, only_config=True)
    assert len(errors1) == 0
    assert len(errors2) == 2
    conf1 = await meta.config('meta.conf1')
    conf2 = await meta.config('meta.conf2')
    assert await conf1.option('od1.i1').value.get() == await conf2.option('od1.i1').value.get() == 7
    #
    dconfigs = []
    ret = await meta.config.find('i1', value=7)
    for conf in await ret.config.list():
        dconfigs.append(conf._config_bag.context)
    assert [conf1._config_bag.context, conf2._config_bag.context] == dconfigs
    await conf1.option('od1.i1').value.set(8)
    #
    dconfigs = []
    ret = await meta.config.find('i1')
    for conf in await ret.config.list():
        dconfigs.append(conf._config_bag.context)
    assert [conf1._config_bag.context, conf2._config_bag.context] == dconfigs
    ret = await meta.config.find('i1', value=7)
    assert len(await ret.config.list()) == 1
    assert conf2._config_bag.context == list(await ret.config.list())[0]._config_bag.context

    ret = await meta.config.find('i1', value=8)
    assert len(await ret.config.list()) == 1
    assert conf1._config_bag.context == list(await ret.config.list())[0]._config_bag.context
    #
    dconfigs = []
    ret = await meta.config.find('i5', value=2)
    for conf in await ret.config.list():
        dconfigs.append(conf._config_bag.context)
    assert [conf1._config_bag.context, conf2._config_bag.context] == dconfigs
    #
    with pytest.raises(AttributeError):
        await meta.config.find('i1', value=10)
    with pytest.raises(AttributeError):
        await meta.config.find('not', value=10)
    with pytest.raises(AttributeError):
        await meta.config.find('i6')
    with pytest.raises(ValueError):
        await meta.value.set('od1.i6', 7, only_config=True, force_default=True)
    with pytest.raises(ValueError):
        await meta.value.set('od1.i6', 7, only_config=True, force_default_if_same=True)
    with pytest.raises(ValueError):
        await meta.value.set('od1.i6', 7, only_config=True, force_dont_change_value=True)
    await delete_sessions(meta)


@pytest.mark.asyncio
async def test_not_meta():
    i1 = IntOption('i1', '')
    od1 = OptionDescription('od1', '', [i1])
    od2 = OptionDescription('od2', '', [od1])
    conf1 = await Config(od2, session_id='conf1')
    conf2 = await Config(od2, session_id='conf2')
    conf3 = await Config(od2)
    conf4 = await Config(od2, session_id='conf4')
    with pytest.raises(TypeError):
        await GroupConfig(conf1)
    #same name
    #with pytest.raises(ConflictError):
    #    GroupConfig([conf2, conf4], session_id='conf2')")
    with pytest.raises(ConflictError):
        await GroupConfig([conf2, conf2], session_id='conf8')
    grp = await GroupConfig([conf1, conf2])
    with pytest.raises(APIError):
        await grp.option('od1.i1').value.get()
    conf1, conf2 = await grp.config.list()
    errors = await grp.value.set('od1.i1', 7)
    assert len(errors) == 0
    conf1 = await grp.config('conf1')
    conf2 = await grp.config('conf2')
    assert await conf1.option('od1.i1').value.get() == await conf2.option('od1.i1').value.get() == 7
    assert await conf1.option('od1.i1').owner.get() is await conf2.option('od1.i1').owner.get() is owners.user
    await grp.option('od1.i1').value.reset()
    assert await conf1.option('od1.i1').owner.get() is await conf2.option('od1.i1').owner.get() is owners.default
    await delete_sessions([conf1, conf2, conf3, conf4])


@pytest.mark.asyncio
async def test_group_find_firsts():
    i1 = IntOption('i1', '')
    od1 = OptionDescription('od1', '', [i1])
    od2 = OptionDescription('od2', '', [od1])
    conf1 = await Config(od2, session_id='conf1')
    conf2 = await Config(od2, session_id='conf2')
    grp = await GroupConfig([conf1, conf2])
    ret = await grp.config.find('i1')
    newconf1, newconf2 = await grp.config.list()
    conf1._config_bag.context == newconf1._config_bag.context
    conf2._config_bag.context == newconf2._config_bag.context
    await delete_sessions([conf1, conf2])


@pytest.mark.asyncio
async def test_group_group():
    i1 = IntOption('i1', '')
    od1 = OptionDescription('od1', '', [i1])
    od2 = OptionDescription('od2', '', [od1])
    conf1 = await Config(od2, session_id='conf9')
    conf2 = await Config(od2, session_id='conf10')
    grp = await GroupConfig([conf1, conf2], 'grp')
    grp2 = await GroupConfig([grp])
    errors = await grp2.value.set('od1.i1', 2)
    assert len(errors) == 0
    conf9 = await grp2.config('grp.conf9')
    assert await conf9.option('od1.i1').value.get() == 2
    assert await conf9.option('od1.i1').owner.get() is owners.user
    await delete_sessions([conf1, conf2, conf9])


@pytest.mark.asyncio
async def test_group_group_path():
    i1 = IntOption('i1', '')
    od1 = OptionDescription('od1', '', [i1])
    od2 = OptionDescription('od2', '', [od1])
    conf1 = await Config(od2, session_id='conf9')
    conf2 = await Config(od2, session_id='conf10')
    grp = await GroupConfig([conf1, conf2], 'grp')
    grp2 = await GroupConfig([grp], 'grp2')
    assert await grp2.config.path() == 'grp2'
    newgrp = await grp2.config('grp')
    assert await newgrp.config.path() == 'grp'
    newgrp = await grp2.config('grp.conf9')
    assert await newgrp.config.path() == 'conf9'
    newgrp = await grp2.config('grp.conf10')
    assert await newgrp.config.path() == 'conf10'
    await delete_sessions([conf1, conf2])


@pytest.mark.asyncio
async def test_meta_unconsistent():
    i1 = IntOption('i1', '')
    i2 = IntOption('i2', '', default=1)
    i3 = IntOption('i3', '')
    i4 = IntOption('i4', '', default=2)
    od1 = OptionDescription('od1', '', [i1, i2, i3, i4])
    od2 = OptionDescription('od2', '', [od1])
    i5 = IntOption('i5', '')
    od3 = OptionDescription('od3', '', [i5])
    conf1 = await Config(od2, session_id='conf1')
    conf2 = await Config(od2, session_id='conf2')
    conf3 = await Config(od2, session_id='conf3')
    conf4 = await Config(od3, session_id='conf4')
    meta = await MetaConfig([conf1, conf2])
    await meta.owner.set(owners.meta1)
    with pytest.raises(TypeError):
        await MetaConfig("string")
    #same descr but conf1 already in meta
    assert len(list(await conf1.config.parents())) == 1
    assert len(list(await conf3.config.parents())) == 0
    new_meta = await MetaConfig([conf1, conf3])
    assert len(list(await conf1.config.parents())) == 2
    assert len(list(await conf3.config.parents())) == 1
    #not same descr
    with pytest.raises(ValueError):
        await MetaConfig([conf3, conf4])
    await delete_sessions([meta, new_meta, conf4])


@pytest.mark.asyncio
async def test_meta_leadership():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "mask", multi=True, properties=('hidden',))
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    conf1 = await Config(od, session_id='conf1')
    conf2 = await Config(od, session_id='conf2')
    meta = await MetaConfig([conf1, conf2])
    await meta.property.read_only()
    ret = await meta.config.find('ip_admin_eth0')
    configs = await ret.config.list()
    assert len(configs) == 2
    assert conf1._config_bag.context == configs[0]._config_bag.context
    assert conf2._config_bag.context == configs[1]._config_bag.context
    ret = await meta.config.find('netmask_admin_eth0')
    configs = await ret.config.list()
    assert len(configs) == 2
    assert conf1._config_bag.context == configs[0]._config_bag.context
    assert conf2._config_bag.context == configs[1]._config_bag.context
    await meta.property.read_write()
    with pytest.raises(AttributeError):
        await meta.config.find('netmask_admin_eth0')
    ret = await meta.unrestraint.config.find('netmask_admin_eth0')
    configs = await ret.config.list()
    assert conf1._config_bag.context == configs[0]._config_bag.context
    assert conf2._config_bag.context == configs[1]._config_bag.context
    await meta.property.read_only()
    ret = await meta.config.find('netmask_admin_eth0')
    configs = await ret.config.list()
    assert conf1._config_bag.context == configs[0]._config_bag.context
    assert conf2._config_bag.context == configs[1]._config_bag.context
    await delete_sessions(meta)


@pytest.mark.asyncio
async def test_meta_leadership_value():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "mask", multi=True, properties=('hidden',))
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    conf1 = await Config(od, session_id='conf1')
    conf2 = await Config(od, session_id='conf2')
    meta = await MetaConfig([conf1, conf2], session_id="meta")
    await conf1.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.8'])
    assert await conf1.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == None
    with pytest.raises(APIError):
        await conf1.option('ip_admin_eth0.ip_admin_eth0', 0).value.get()
    #
    await conf1.option('ip_admin_eth0.ip_admin_eth0').value.reset()
    #
    await meta.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.1'])
    assert await conf1.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == None
    await meta.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set('255.255.255.0')
    assert await conf1.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == '255.255.255.0'
    await meta.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set('255.255.0.0')
    assert await conf1.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == '255.255.0.0'
    #
    await conf1.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.1'])
    assert await conf1.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == '255.255.0.0'
    #
    await meta.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.2.1', '192.168.3.1'])
    await meta.option('ip_admin_eth0.netmask_admin_eth0', 1).value.set('255.255.255.0')
    #
    assert await conf1.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.1']
    assert await conf1.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == '255.255.0.0'
    await delete_sessions(meta)


@pytest.mark.asyncio
async def test_meta_leadership_value_default():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip", multi=True, default=['192.168.1.1'])
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "mask", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    conf1 = await Config(od, session_id='conf1')
    conf2 = await Config(od, session_id='conf2')
    meta = await MetaConfig([conf1, conf2])
    newconf1 = await meta.config('conf1')
    assert await newconf1.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == None
    #
    await meta.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.1'])
    assert await newconf1.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == None
    #
    await meta.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set('255.255.255.0')
    assert await newconf1.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == '255.255.255.0'
    #
    await meta.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set('255.255.0.0')
    assert await newconf1.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == '255.255.0.0'
    #
    await newconf1.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.1'])
    assert await newconf1.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == '255.255.0.0'
    await delete_sessions(meta)


@pytest.mark.asyncio
async def test_meta_leadership_owners():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "mask", multi=True, properties=('hidden',))
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    conf1 = await Config(od, session_id='conf1')
    conf2 = await Config(od, session_id='conf2')
    meta = await MetaConfig([conf1, conf2])
    await meta.owner.set(owners.meta1)
    newconf1 = await meta.config('conf1')
    assert await newconf1.option('ip_admin_eth0.ip_admin_eth0').owner.isdefault()
    with pytest.raises(LeadershipError):
        await newconf1.option('ip_admin_eth0.netmask_admin_eth0', 0).owner.isdefault()
    #
    await newconf1.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.1'])
    assert await newconf1.option('ip_admin_eth0.ip_admin_eth0').owner.get() == owners.user
    assert await newconf1.option('ip_admin_eth0.netmask_admin_eth0', 0).owner.isdefault()
    #
    await newconf1.option('ip_admin_eth0.ip_admin_eth0').value.reset()
    assert await newconf1.option('ip_admin_eth0.ip_admin_eth0').owner.isdefault()
    #
    await meta.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.1'])
    assert await newconf1.option('ip_admin_eth0.ip_admin_eth0').owner.get() == owners.meta1
    assert await newconf1.option('ip_admin_eth0.netmask_admin_eth0', 0).owner.isdefault()
    #
    await meta.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set('255.255.255.0')
    assert await newconf1.option('ip_admin_eth0.ip_admin_eth0').owner.get() == owners.meta1
    assert await newconf1.option('ip_admin_eth0.netmask_admin_eth0', 0).owner.get() == owners.meta1
    #
    await meta.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set('255.255.0.0')
    assert await newconf1.option('ip_admin_eth0.ip_admin_eth0').owner.get() == owners.meta1
    assert await newconf1.option('ip_admin_eth0.netmask_admin_eth0', 0).owner.get() == owners.meta1
    #
    await newconf1.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.1'])
    assert await newconf1.option('ip_admin_eth0.ip_admin_eth0').owner.get() == owners.user
    assert await newconf1.option('ip_admin_eth0.netmask_admin_eth0', 0).owner.get() == owners.meta1
    await delete_sessions(meta)


@pytest.mark.asyncio
async def test_meta_force_default():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "mask", multi=True, properties=('hidden',))
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    conf1 = await Config(od, session_id='conf1')
    conf2 = await Config(od, session_id='conf2')
    meta = await MetaConfig([conf1, conf2])
    await meta.property.read_write()
    await meta.owner.set('meta1')
    newconf1 = await meta.config('conf1')
    newconf2 = await meta.config('conf2')
    assert await meta.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert await newconf1.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert await newconf2.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    #
    errors = await meta.value.set('ip_admin_eth0.ip_admin_eth0', ['192.168.1.1'])
    assert len(errors) == 0
    assert await meta.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.1']
    assert await newconf1.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.1']
    assert await newconf2.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.1']
    #
    await newconf1.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.2'])
    assert await meta.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.1']
    assert await newconf1.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.2']
    assert await newconf2.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.1']
    #
    errors = await meta.value.set('ip_admin_eth0.ip_admin_eth0', ['192.168.1.3'])
    assert len(errors) == 0
    assert await meta.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.3']
    assert await newconf1.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.2']
    assert await newconf2.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.3']
    #
    errors = await meta.value.set('ip_admin_eth0.ip_admin_eth0', ['192.168.1.4'], force_default=True)
    assert len(errors) == 0
    assert await meta.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.4']
    assert await newconf1.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.4']
    assert await newconf2.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.4']
    await delete_sessions(meta)


@pytest.mark.asyncio
async def test_meta_force_dont_change_value():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "mask", multi=True, properties=('hidden',))
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    conf1 = await Config(od, session_id='conf1')
    conf2 = await Config(od, session_id='conf2')
    meta = await MetaConfig([conf1, conf2])
    await meta.property.read_write()
    await meta.owner.set('meta1')
    assert await meta.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    newconf1 = await meta.config('conf1')
    newconf2 = await meta.config('conf2')
    assert await newconf1.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert await newconf2.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    await newconf1.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.4'])
    assert await newconf1.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.4']
    assert await newconf2.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert await newconf1.option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.user
    assert await newconf2.option('ip_admin_eth0.ip_admin_eth0').owner.isdefault()
    errors = await meta.value.set('ip_admin_eth0.ip_admin_eth0', ['192.168.1.4'], force_dont_change_value=True)
    assert len(errors) == 0
    assert await meta.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.4']
    assert await newconf1.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.4']
    assert await newconf2.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert await newconf1.option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.user
    assert await newconf2.option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.user
    await delete_sessions(meta)


@pytest.mark.asyncio
async def test_meta_force_default_if_same():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "mask", multi=True, properties=('hidden',))
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    conf1 = await Config(od, session_id='conf1')
    conf2 = await Config(od, session_id='conf2')
    meta = await MetaConfig([conf1, conf2])
    await meta.property.read_write()
    await meta.owner.set('meta1')
    #
    newconf1 = await meta.config('conf1')
    newconf2 = await meta.config('conf2')
    assert await meta.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert await newconf1.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert await newconf2.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    #
    await newconf1.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.4'])
    assert await newconf1.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.4']
    assert await newconf2.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert await newconf1.option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.user
    assert await newconf2.option('ip_admin_eth0.ip_admin_eth0').owner.isdefault()
    errors = await meta.value.set('ip_admin_eth0.ip_admin_eth0', ['192.168.1.4'], force_default_if_same=True)
    assert len(errors) == 0
    assert await meta.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.4']
    assert await newconf1.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.4']
    assert await newconf2.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.4']
    assert await newconf1.option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.meta1
    assert await newconf2.option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.meta1
    #
    await newconf1.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.3'])
    assert await newconf1.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.3']
    assert await newconf2.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.4']
    assert await newconf1.option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.user
    assert await newconf2.option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.meta1
    errors = await meta.value.set('ip_admin_eth0.ip_admin_eth0', ['192.168.1.5'], force_default_if_same=True)
    assert len(errors) == 0
    assert await meta.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.5']
    assert await newconf1.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.3']
    assert await newconf2.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.5']
    assert await newconf1.option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.user
    assert await newconf2.option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.meta1
    await delete_sessions(meta)


@pytest.mark.asyncio
async def test_meta_force_default_if_same_and_dont_change():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "mask", multi=True, properties=('hidden',))
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    conf1 = await Config(od, session_id='conf1')
    conf2 = await Config(od, session_id='conf2')
    meta = await MetaConfig([conf1, conf2])
    await meta.property.read_write()
    await meta.owner.set('meta1')
    #
    newconf1 = await meta.config('conf1')
    newconf2 = await meta.config('conf2')
    assert await meta.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert await newconf1.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert await newconf2.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    #
    await newconf1.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.4'])
    assert await newconf1.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.4']
    assert await newconf2.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert await newconf1.option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.user
    assert await newconf2.option('ip_admin_eth0.ip_admin_eth0').owner.isdefault()
    errors = await meta.value.set('ip_admin_eth0.ip_admin_eth0', ['192.168.1.4'], force_default_if_same=True, force_dont_change_value=True)
    assert len(errors) == 0
    assert await meta.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.4']
    assert await newconf1.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.4']
    assert await newconf2.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert await newconf1.option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.meta1
    assert await newconf2.option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.user
    #
    await newconf1.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.3'])
    assert await newconf1.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.3']
    assert await newconf2.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert await newconf1.option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.user
    assert await newconf2.option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.user
    errors = await meta.value.set('ip_admin_eth0.ip_admin_eth0', ['192.168.1.5'], force_default_if_same=True, force_dont_change_value=True)
    assert len(errors) == 0
    assert await meta.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.5']
    assert await newconf1.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.3']
    assert await newconf2.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert await newconf1.option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.user
    assert await newconf2.option('ip_admin_eth0.ip_admin_eth0').owner.get() is owners.user
    await delete_sessions(meta)


@pytest.mark.asyncio
async def test_meta_force_default_and_dont_change():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "mask", multi=True, properties=('hidden',))
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    conf1 = await Config(od, session_id='rconf1')
    conf2 = await Config(od, session_id='rconf2')
    meta = await MetaConfig([conf1, conf2])
    await meta.property.read_write()
    await meta.owner.set('meta1')
    with pytest.raises(ValueError):
        await meta.value.set('ip_admin_eth0.ip_admin_eth0', ['192.168.1.4'], force_default=True, force_dont_change_value=True)
    await delete_sessions(meta)


@pytest.mark.asyncio
async def test_meta_properties_meta():
    ip_admin_eth0 = NetworkOption('ip_admin_eth0', "ip", multi=True, default=['192.168.1.1'])
    netmask_admin_eth0 = NetmaskOption('netmask_admin_eth0', "mask", multi=True, validators=[Calculation(valid_network_netmask, Params((ParamOption(ip_admin_eth0), ParamSelfOption())))])
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0], properties=('disabled',))
    od = OptionDescription('root', '', [interface1])
    conf1 = await Config(od, session_id='conf1')
    conf2 = await Config(od, session_id='conf2')
    await conf1.property.read_write()
    await conf2.property.read_write()
    meta = await MetaConfig([conf1, conf2])
    await meta.property.read_write()
    ret = await meta.config('conf1')
    assert await ret.value.dict() == {}
    await delete_sessions(meta)


@pytest.mark.asyncio
async def test_meta_exception_meta():
    ip_admin_eth0 = NetworkOption('ip_admin_eth0', "ip", multi=True, default=['192.168.1.1'])
    netmask_admin_eth0 = NetmaskOption('netmask_admin_eth0', "mask", Calculation(raise_exception), multi=True, validators=[Calculation(valid_network_netmask, Params((ParamOption(ip_admin_eth0), ParamSelfOption())))])
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    conf1 = await Config(od, session_id='conf1')
    conf2 = await Config(od, session_id='conf2')
    meta = await MetaConfig([conf1, conf2])
    await meta.property.read_write()
    with pytest.raises(Exception):
        await conf1.make_dict()
    await delete_sessions(meta)


@pytest.mark.asyncio
async def test_meta_properties_requires1():
    opt1 = BoolOption('opt1', 'opt1', False)
    opt2 = BoolOption('opt2', "")
    disabled_property = Calculation(calc_value,
                                    Params(ParamValue('disabled'),
                                           kwargs={'condition': ParamOption(opt1, todict=True),
                                                   'expected': ParamValue(False)}))
    od2 = OptionDescription('od2', "", [opt2], properties=(disabled_property,))
    opt3 = BoolOption('opt3', '', validators=[Calculation(valid_not_equal, Params((ParamOption(opt2), ParamSelfOption())))])
    od = OptionDescription('root', '', [opt1, od2, opt3])
    conf1 = await Config(od, session_id='conf1')
    await conf1.property.read_write()
    meta = await MetaConfig([conf1], 'meta')
    await meta.property.read_write()
    await meta.option('opt1').value.set(True)
    #
    await conf1.option('od2.opt2').value.set(False)
    await delete_sessions(meta)


@pytest.mark.asyncio
async def test_meta_properties_requires_mandatory():
    probes = BoolOption('probes', 'probes available', False)
    eth0_method = ChoiceOption('eth0_method', '', ('static', 'dhcp'), 'static')
    ip_address = IPOption('ip_address', '')
    mandatory_property = Calculation(calc_value,
                                     Params(ParamValue('mandatory'),
                                            kwargs={'condition': ParamOption(probes),
                                                    'expected': ParamValue('yes'),
                                                    'default': ParamValue(None)}))
    ip_eth0 = IPOption('ip_eth0', "ip", Calculation(return_condition, Params(kwargs={'val': ParamOption(ip_address), 'condition': ParamOption(eth0_method), 'expected': ParamValue('dhcp')})), properties=(mandatory_property,))
    ip_gw = IPOption('ip_gw', 'gw', validators=[Calculation(valid_not_equal, Params((ParamOption(ip_eth0), ParamSelfOption())))])
    od = OptionDescription('root', '', [ip_gw, probes, eth0_method, ip_address, ip_eth0])
    conf1 = await Config(od, session_id='conf1')
    await conf1.property.read_write()
    meta = await MetaConfig([conf1], 'meta')
    #
    await meta.option('probes').value.set(True)
    await meta.option('ip_address').value.set('1.1.1.1')
    await meta.option('ip_gw').value.set('1.1.1.2')
    await conf1.option('eth0_method').value.set('dhcp')
    await conf1.property.read_only()
    assert await conf1.value.dict(fullpath=True) == {'probes': True, 'eth0_method': 'dhcp', 'ip_address': '1.1.1.1', 'ip_eth0': '1.1.1.1', 'ip_gw': '1.1.1.2'}
    await delete_sessions(meta)


@pytest.mark.asyncio
async def test_meta_callback():
    val1 = StrOption('val1', "", 'val')
    val2 = StrOption('val2', "", Calculation(return_value, Params(ParamOption(val1))))
    val3 = StrOption('val3', "", Calculation(return_value, Params(ParamValue('yes'))))
    val4 = StrOption('val4', "", Calculation(return_value, Params(kwargs={'value': ParamOption(val1)})))
    val5 = StrOption('val5', "", Calculation(return_value, Params(kwargs={'value': ParamValue('yes')})))
    maconfig = OptionDescription('rootconfig', '', [val1, val2, val3, val4, val5])
    cfg = await Config(maconfig, session_id='cfg')
    meta = await MetaConfig([cfg])
    await meta.property.read_write()
    newcfg = await meta.config('cfg')
    assert await newcfg.value.dict() == {'val3': 'yes', 'val2': 'val', 'val1': 'val', 'val5': 'yes', 'val4': 'val'}
    await newcfg.option('val1').value.set('new')
    assert await newcfg.value.dict() == {'val3': 'yes', 'val2': 'new', 'val1': 'new', 'val5': 'yes', 'val4': 'new'}
    await newcfg.option('val1').value.reset()
    await meta.option('val1').value.set('new')
    assert await newcfg.value.dict() == {'val3': 'yes', 'val2': 'new', 'val1': 'new', 'val5': 'yes', 'val4': 'new'}
    await newcfg.option('val4').value.set('new1')
    assert await newcfg.value.dict() == {'val3': 'yes', 'val2': 'new', 'val1': 'new', 'val5': 'yes', 'val4': 'new1'}
    await newcfg.option('val4').value.reset()
    await meta.option('val4').value.set('new1')
    assert await newcfg.value.dict() == {'val3': 'yes', 'val2': 'new', 'val1': 'new', 'val5': 'yes', 'val4': 'new1'}
    await meta.option('val4').value.reset()
    await delete_sessions(meta)


@pytest.mark.asyncio
async def test_meta_callback_follower():
    val = StrOption('val', "", default='val')
    val1 = StrOption('val1', "", [Calculation(return_value, Params(ParamOption(val)))], multi=True)
    val3 = StrOption('val2', "", Calculation(return_value, Params(ParamOption(val1))), multi=True)
    val4 = StrOption('val3', "", Calculation(return_value, Params(ParamOption(val1))), multi=True)
    interface1 = Leadership('val1', '', [val1, val3, val4])
    od = OptionDescription('root', '', [interface1])
    maconfig = OptionDescription('rootconfig', '', [val, interface1])
    conf1 = await Config(maconfig, session_id='conf1')
    meta = await MetaConfig([conf1])
    await meta.property.read_write()
    assert await conf1.value.dict() == {'val1.val2': ['val'], 'val1.val1': ['val'], 'val1.val3': ['val'], 'val': 'val'}
    #
    await conf1.option('val').value.set('val1')
    assert await conf1.value.dict() == {'val1.val2': ['val1'], 'val1.val1': ['val1'], 'val1.val3': ['val1'], 'val': 'val1'}
    #
    await conf1.option('val').value.reset()
    await meta.option('val').value.set('val1')
    assert await conf1.value.dict() == {'val1.val2': ['val1'], 'val1.val1': ['val1'], 'val1.val3': ['val1'], 'val': 'val1'}
    #
    await meta.option('val').value.reset()
    await conf1.option('val1.val2', 0).value.set('val2')
    assert await conf1.value.dict() == {'val1.val2': ['val2'], 'val1.val1': ['val'], 'val1.val3': ['val'], 'val': 'val'}
    #
    await conf1.option('val1.val2', 0).value.reset()
    assert await conf1.value.dict() == {'val1.val2': ['val'], 'val1.val1': ['val'], 'val1.val3': ['val'], 'val': 'val'}
    #
    await meta.option('val1.val2', 0).value.set('val2')
    assert await conf1.value.dict() == {'val1.val2': ['val2'], 'val1.val1': ['val'], 'val1.val3': ['val'], 'val': 'val'}
    #
    await meta.option('val1.val1').value.set(['val'])
    assert await conf1.value.dict() == {'val1.val2': ['val2'], 'val1.val1': ['val'], 'val1.val3': ['val'], 'val': 'val'}
    #
    await conf1.option('val1.val3', 0).value.set('val6')
    assert await conf1.value.dict() == {'val1.val2': ['val2'], 'val1.val1': ['val'], 'val1.val3': ['val6'], 'val': 'val'}
    #
    await meta.option('val1.val2', 0).value.reset()
    await conf1.option('val1.val3', 0).value.reset()
    await conf1.option('val1.val1').value.set(['val3'])
    assert await conf1.value.dict() == {'val1.val2': ['val3'], 'val1.val1': ['val3'], 'val1.val3': ['val3'], 'val': 'val'}
    #
    await conf1.option('val1.val1').value.reset()
    assert await conf1.value.dict() == {'val1.val2': ['val'], 'val1.val1': ['val'], 'val1.val3': ['val'], 'val': 'val'}
    #
    await meta.option('val1.val1').value.set(['val3'])
    assert await conf1.value.dict() == {'val1.val2': ['val3'], 'val1.val1': ['val3'], 'val1.val3': ['val3'], 'val': 'val'}
    #
    await conf1.option('val1.val2', 0).value.set('val2')
    assert await conf1.value.dict() == {'val1.val2': ['val2'], 'val1.val1': ['val3'], 'val1.val3': ['val3'], 'val': 'val'}
    #
    await meta.option('val1.val1').value.set(['val3', 'rah'])
    assert await conf1.value.dict() == {'val1.val2': ['val2', 'rah'], 'val1.val1': ['val3', 'rah'], 'val1.val3': ['val3', 'rah'], 'val': 'val'}
    #
    await meta.option('val1.val1').value.pop(1)
    await meta.option('val1.val1').value.set(['val4'])
    assert await conf1.value.dict() == {'val1.val2': ['val2'], 'val1.val1': ['val4'], 'val1.val3': ['val4'], 'val': 'val'}
    await delete_sessions(meta)


@pytest.mark.asyncio
async def test_meta_reset():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "mask", multi=True, properties=('hidden',))
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    conf1 = await Config(od, session_id='conf1')
    conf2 = await Config(od, session_id='conf2')
    meta = await MetaConfig([conf1, conf2])
    await meta.property.read_write()
    await meta.owner.set('meta1')
    newconf1 = await meta.config('conf1')
    newconf2 = await meta.config('conf2')
    assert await meta.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert await newconf1.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert await newconf2.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    errors = await meta.value.set('ip_admin_eth0.ip_admin_eth0', ['192.168.1.1'])
    assert len(errors) == 0
    assert await meta.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.1']
    assert await newconf1.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.1']
    assert await newconf2.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.1']
    await newconf1.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.2'])
    assert await meta.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.1']
    assert await newconf1.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.2']
    assert await newconf2.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.1']
    await meta.value.reset('ip_admin_eth0.ip_admin_eth0')
    assert await meta.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert await newconf1.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert await newconf2.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    await delete_sessions(meta)


@pytest.mark.asyncio
async def test_meta_properties_meta_copy():
    ip_admin_eth0 = NetworkOption('ip_admin_eth0', "ip", multi=True, default=['192.168.1.1'])
    netmask_admin_eth0 = NetmaskOption('netmask_admin_eth0', "mask", multi=True, properties=('disabled',))
    interface1 = OptionDescription('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    conf1 = await Config(interface1, session_id='conf1')
    conf2 = await Config(interface1, session_id='conf2')
    await conf1.property.read_write()
    await conf2.property.read_write()
    meta = await MetaConfig([conf1, conf2], session_id='meta1')
    await meta.property.read_write()

    newconf1 = await meta.config('conf1')
    newconf2 = await meta.config('conf2')
    conf3 = await newconf1.config.copy(session_id='conf3')
    # old fashion
    meta2 = await conf3.config.metaconfig()
    assert await meta.session.id() == await meta2.session.id()
    # new method
    meta2 = list(await conf3.config.parents())
    assert len(meta2) == 1
    assert await meta.session.id() == await meta2[0].session.id()

    assert await newconf1.value.dict() == {'ip_admin_eth0': ['192.168.1.1']}
    assert await newconf2.value.dict() == {'ip_admin_eth0': ['192.168.1.1']}
    newconf3 = await meta.config('conf3')
    assert await newconf3.value.dict() == {'ip_admin_eth0': ['192.168.1.1']}
    await meta.option('ip_admin_eth0').value.set(['192.168.1.2'])
    assert await newconf1.value.dict() == {'ip_admin_eth0': ['192.168.1.2']}
    assert await newconf2.value.dict() == {'ip_admin_eth0': ['192.168.1.2']}
    assert await newconf3.value.dict() == {'ip_admin_eth0': ['192.168.1.2']}
    ret = await meta.value.set('ip_admin_eth0', ['192.168.1.3'], force_default_if_same=True)
    assert await newconf1.value.dict() == {'ip_admin_eth0': ['192.168.1.3']}
    assert await newconf2.value.dict() == {'ip_admin_eth0': ['192.168.1.3']}
    assert await newconf3.value.dict() == {'ip_admin_eth0': ['192.168.1.3']}
    await delete_sessions(meta)


@pytest.mark.asyncio
async def test_meta_properties_meta_deepcopy():
    ip_admin_eth0 = NetworkOption('ip_admin_eth0', "ip", multi=True, default=['192.168.1.1'])
    netmask_admin_eth0 = NetmaskOption('netmask_admin_eth0', "mask", multi=True,
                                       properties=('disabled',))
    interface1 = OptionDescription('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    conf1 = await Config(interface1, session_id='conf1')
    conf2 = await Config(interface1, session_id='conf2')
    await conf1.property.read_write()
    await conf2.property.read_write()
    meta = await MetaConfig([conf1, conf2])
    await meta.permissive.add('hidden')
    await meta.property.read_write()

    newconf1 = await meta.config('conf1')
    newconf2 = await meta.config('conf2')
    meta2 = await newconf1.config.deepcopy(session_id='conf3')
    newconf3 = await meta2.config('conf3')
    assert meta != meta2
    assert await meta.permissive.get() == await meta2.permissive.get()

    assert await newconf1.value.dict() == {'ip_admin_eth0': ['192.168.1.1']}
    assert await newconf2.value.dict() == {'ip_admin_eth0': ['192.168.1.1']}
    assert await newconf3.value.dict() == {'ip_admin_eth0': ['192.168.1.1']}
    await meta.option('ip_admin_eth0').value.set(['192.168.1.2'])
    assert await newconf1.value.dict() == {'ip_admin_eth0': ['192.168.1.2']}
    assert await newconf2.value.dict() == {'ip_admin_eth0': ['192.168.1.2']}
    assert await newconf3.value.dict() == {'ip_admin_eth0': ['192.168.1.1']}
    await meta.value.set('ip_admin_eth0', ['192.168.1.3'], force_default_if_same=True)
    assert await newconf1.value.dict() == {'ip_admin_eth0': ['192.168.1.3']}
    assert await newconf2.value.dict() == {'ip_admin_eth0': ['192.168.1.3']}
    assert await newconf3.value.dict() == {'ip_admin_eth0': ['192.168.1.1']}
    await delete_sessions([meta, meta2])


@pytest.mark.asyncio
async def test_meta_properties_meta_deepcopy_multi_parent():
    ip_admin_eth0 = NetworkOption('ip_admin_eth0', "ip")
    netmask_admin_eth0 = NetmaskOption('netmask_admin_eth0', "mask")
    interface1 = OptionDescription('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    conf1 = await Config(interface1, session_id='conf1')
    conf2 = await Config(interface1, session_id='conf2')
    await conf1.property.read_write()
    await conf2.property.read_write()
    meta1 = await MetaConfig([conf1, conf2], session_id='meta1')
    await meta1.permissive.add('hidden')
    await meta1.property.read_write()

    meta2 = await MetaConfig(['name1', 'name2'], optiondescription=interface1, session_id='meta2')
    await meta2.config.add(conf1)

    await meta1.option('ip_admin_eth0').value.set('192.168.1.1')
    await meta2.option('netmask_admin_eth0').value.set('255.255.255.0')

    assert await meta1.value.dict() == {'ip_admin_eth0': '192.168.1.1', 'netmask_admin_eth0': None}
    assert await meta2.value.dict() == {'ip_admin_eth0': None, 'netmask_admin_eth0': '255.255.255.0'}
    assert await conf1.value.dict() == {'ip_admin_eth0': '192.168.1.1', 'netmask_admin_eth0': '255.255.255.0'}
    assert await conf2.value.dict() == {'ip_admin_eth0': '192.168.1.1', 'netmask_admin_eth0': None}

    copy_meta2 = await conf1.config.deepcopy(session_id='copy_conf1', metaconfig_prefix='copy_')
    assert await copy_meta2.config.path() == 'copy_meta2'
    copy_meta1 = await copy_meta2.config('copy_meta1')
    copy_conf1 = await copy_meta1.config('copy_conf1')
    assert await copy_meta2.value.dict() == {'ip_admin_eth0': None, 'netmask_admin_eth0': '255.255.255.0'}
    assert await copy_conf1.value.dict() == {'ip_admin_eth0': '192.168.1.1', 'netmask_admin_eth0': '255.255.255.0'}
    await delete_sessions([conf1, conf2, meta1, meta2, copy_conf1, copy_meta1, copy_meta2])


@pytest.mark.asyncio
async def test_meta_properties_submeta_deepcopy():
    ip_admin_eth0 = NetworkOption('ip_admin_eth0', "ip", multi=True, default=['192.168.1.1'])
    netmask_admin_eth0 = NetmaskOption('netmask_admin_eth0', "mask", multi=True,
                                       properties=('disabled',))
    interface1 = OptionDescription('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    conf1 = await Config(interface1, session_id='conf1')
    await conf1.property.read_write()
    meta1 = await MetaConfig([conf1], session_id='meta1')
    meta2 = await MetaConfig([meta1], session_id='meta2')
    meta_copy = await conf1.config.deepcopy(session_id='conf2',
                                      metaconfig_prefix='copy_')
    assert await meta_copy.session.id() == 'copy_meta2'
    newcopy = await meta_copy.config('copy_meta1')
    assert await newcopy.session.id() == 'copy_meta1'
    newcopy = await newcopy.config('conf2')
    assert await newcopy.session.id() == 'conf2'
    await delete_sessions([meta2, meta_copy])


@pytest.mark.asyncio
async def test_meta_properties_deepcopy_meta():
    ip_admin_eth0 = NetworkOption('ip_admin_eth0', "ip", multi=True, default=['192.168.1.1'])
    netmask_admin_eth0 = NetmaskOption('netmask_admin_eth0', "mask", multi=True,
                                       properties=('disabled',))
    interface1 = OptionDescription('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    conf1 = await Config(interface1, session_id='conf1')
    await conf1.property.read_write()
    meta1 = await MetaConfig([conf1], session_id='meta1')
    meta2 = await MetaConfig([meta1], session_id='meta2')
    meta_copy = await meta1.config.deepcopy(session_id='meta3',
                                            metaconfig_prefix='copy_')
    assert await meta_copy.session.id() == 'copy_meta2'
    newcopy = await meta_copy.config('meta3')
    assert await newcopy.session.id() == 'meta3'
    assert list(await newcopy.config.list()) == []
    await delete_sessions([meta2, meta_copy])


@pytest.mark.asyncio
async def test_meta_properties_submeta_deepcopy_owner():
    ip_admin_eth0 = NetworkOption('ip_admin_eth0', "ip")
    netmask_admin_eth0 = NetmaskOption('netmask_admin_eth0', "mask")
    interface1 = OptionDescription('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    conf1 = await Config(interface1, session_id='conf1')
    await conf1.owner.set('conf1_user')
    await conf1.property.read_write()
    meta1 = await MetaConfig([conf1], session_id='meta1')
    await meta1.owner.set('meta1_user')
    meta2 = await MetaConfig([meta1], session_id='meta2')
    await meta2.owner.set('meta2_user')
    #
    await conf1.option('ip_admin_eth0').value.set('192.168.0.1')
    assert await conf1.option('ip_admin_eth0').owner.get() == 'conf1_user'
    await meta1.option('ip_admin_eth0').value.set('192.168.0.2')
    assert await meta1.option('ip_admin_eth0').owner.get() == 'meta1_user'
    await meta2.option('ip_admin_eth0').value.set('192.168.0.3')
    assert await meta2.option('ip_admin_eth0').owner.get() == 'meta2_user'
    #
    meta2_copy = await conf1.config.deepcopy(session_id='conf2',
                                       metaconfig_prefix='copy_')
    await meta2_copy.option('netmask_admin_eth0').value.set('255.255.255.255')
    assert await meta2_copy.option('ip_admin_eth0').value.get() == '192.168.0.3'
    assert await meta2_copy.option('ip_admin_eth0').owner.get() == 'meta2_user'
    assert await meta2_copy.option('netmask_admin_eth0').owner.get() == 'meta2_user'
    #
    meta1_copy = await meta2_copy.config('copy_meta1')
    await meta1_copy.option('netmask_admin_eth0').value.set('255.255.255.255')
    assert await meta1_copy.option('ip_admin_eth0').value.get() == '192.168.0.2'
    assert await meta1_copy.option('ip_admin_eth0').owner.get() == 'meta1_user'
    assert await meta1_copy.option('netmask_admin_eth0').owner.get() == 'meta1_user'
    #
    conf2 = await meta1_copy.config('conf2')
    await conf2.owner.set('conf2_user')
    await conf2.option('netmask_admin_eth0').value.set('255.255.255.255')
    assert await conf2.option('netmask_admin_eth0').owner.get() == 'conf2_user'
    assert await conf2.option('ip_admin_eth0').value.get() == '192.168.0.1'
    assert await conf2.option('ip_admin_eth0').owner.get() == 'conf1_user'
    await delete_sessions([meta1, meta2, conf2, meta2_copy])


@pytest.mark.asyncio
async def test_meta_properties_meta_set_value():
    ip_admin_eth0 = NetworkOption('ip_admin_eth0', "ip", multi=True, default=['192.168.1.1'])
    netmask_admin_eth0 = NetmaskOption('netmask_admin_eth0', "mask", multi=True, properties=('disabled',))
    interface1 = OptionDescription('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    conf1 = await Config(interface1, session_id='conf1')
    conf2 = await Config(interface1, session_id='conf2')
    await conf1.property.read_write()
    await conf2.property.read_write()
    meta = await MetaConfig([conf1, conf2])
    await meta.property.read_write()
    ret = await meta.config('conf1')
    assert await ret.value.dict() == {'ip_admin_eth0': ['192.168.1.1']}
    ret = await meta.value.set('netmask_admin_eth0', ['255.255.255.255'], only_config=True)
    assert len(ret) == 2
    assert isinstance(ret[0], PropertiesOptionError)
    assert isinstance(ret[1], PropertiesOptionError)
    del ret[1]
    del ret[0]
    del ret
    ret = await meta.value.set('netmask_admin_eth0', ['255.255.255.255'], force_default=True)
    assert len(ret) == 1
    assert isinstance(ret[0], PropertiesOptionError)
    del ret[0]
    del ret
    ret = await meta.value.set('netmask_admin_eth0', ['255.255.255.255'], force_dont_change_value=True)
    assert len(ret) == 3
    assert isinstance(ret[0], PropertiesOptionError)
    assert isinstance(ret[1], PropertiesOptionError)
    assert isinstance(ret[2], PropertiesOptionError)
    del ret[2]
    del ret[1]
    del ret[0]
    del ret
    ret = await meta.value.set('netmask_admin_eth0', ['255.255.255.255'], force_default_if_same=True)
    assert len(ret) == 1
    assert isinstance(ret[0], PropertiesOptionError)
    del ret[0]
    del ret
    ret = await meta.value.set('ip_admin_eth0', '255.255.255.255', only_config=True)
    assert len(ret) == 2
    assert isinstance(ret[0], ValueError)
    assert isinstance(ret[1], ValueError)
    del ret[1]
    del ret[0]
    del ret
    ret = await meta.value.set('ip_admin_eth0', '255.255.255.255', force_default=True)
    assert len(ret) == 1
    assert isinstance(ret[0], ValueError)
    del ret[0]
    del ret
    ret = await meta.value.set('ip_admin_eth0', '255.255.255.255', force_dont_change_value=True)
    assert len(ret) == 1
    assert isinstance(ret[0], ValueError)
    del ret[0]
    del ret
    ret = await meta.value.set('ip_admin_eth0', '255.255.255.255', force_default_if_same=True)
    assert len(ret) == 1
    assert isinstance(ret[0], ValueError)
    del ret[0]
    del ret
    await delete_sessions(meta)


@pytest.mark.asyncio
async def test_metaconfig_force_metaconfig_on_freeze():
    dummy1 = StrOption('dummy1', 'doc dummy', default='default', properties=('force_metaconfig_on_freeze',))
    group = OptionDescription('group', '', [dummy1])
    cfg = await Config(group, session_id='cfg')
    await cfg.owner.set(owners.config)
    meta1 = await MetaConfig([cfg], session_id='meta1')
    await meta1.owner.set(owners.meta1)
    meta2 = await MetaConfig([meta1], session_id='meta2')
    await meta2.owner.set(owners.meta2)
    await cfg.property.read_write()

    await cfg.option('dummy1').property.add('frozen')
    #
    assert await cfg.option('dummy1').value.get() == 'default'
    assert await cfg.option('dummy1').owner.get() == 'default'
    #
    await meta2.option('dummy1').value.set('meta2')
    #
    assert await cfg.option('dummy1').value.get() == 'meta2'
    assert await cfg.option('dummy1').owner.get() == 'meta2'
    #
    await cfg.option('dummy1').property.pop('frozen')
    await cfg.option('dummy1').value.set('cfg')
    await cfg.option('dummy1').property.add('frozen')
    #
    assert await cfg.option('dummy1').value.get() == 'meta2'
    assert await cfg.option('dummy1').owner.get() == 'meta2'
    #
    await meta1.option('dummy1').value.set('meta1')
    #
    assert await cfg.option('dummy1').value.get() == 'meta1'
    assert await cfg.option('dummy1').owner.get() == 'meta1'
    #
    await cfg.option('dummy1').property.pop('frozen')
    assert await cfg.option('dummy1').value.get() == 'cfg'
    assert await cfg.option('dummy1').owner.get() == 'config'
    await delete_sessions([meta1, meta2])


@pytest.mark.asyncio
async def test_metaconfig_force_metaconfig_on_freeze_option():
    dummy1 = StrOption('dummy1', 'doc dummy', default='default')
    dummy2 = StrOption('dummy2', 'doc dummy', default='default', properties=('force_default_on_freeze',))
    group = OptionDescription('group', '', [dummy1, dummy2])
    cfg = await Config(group, session_id='cfg')
    await cfg.owner.set(owners.config)
    meta1 = await MetaConfig([cfg], session_id='meta1')
    await meta1.owner.set(owners.meta1)
    meta2 = await MetaConfig([meta1], session_id='meta2')
    await meta2.owner.set(owners.meta2)
    await cfg.property.read_write()

    await cfg.option('dummy1').property.add('frozen')
    await cfg.option('dummy1').property.add('force_metaconfig_on_freeze')
    await cfg.option('dummy2').property.add('frozen')
    #
    assert await cfg.option('dummy1').value.get() == 'default'
    assert await cfg.option('dummy1').owner.get() == 'default'
    assert await cfg.option('dummy2').value.get() == 'default'
    assert await cfg.option('dummy2').owner.get() == 'default'
    #
    await meta2.option('dummy1').value.set('meta2')
    await meta2.option('dummy2').value.set('meta2')
    #
    assert await cfg.option('dummy1').value.get() == 'meta2'
    assert await cfg.option('dummy1').owner.get() == 'meta2'
    assert await cfg.option('dummy2').value.get() == 'default'
    assert await cfg.option('dummy2').owner.get() == 'default'
    #
    await cfg.option('dummy1').property.pop('frozen')
    await cfg.option('dummy2').property.pop('frozen')
    await cfg.option('dummy1').value.set('cfg')
    await cfg.option('dummy2').value.set('cfg')
    await cfg.option('dummy1').property.add('frozen')
    await cfg.option('dummy2').property.add('frozen')
    #
    assert await cfg.option('dummy1').value.get() == 'meta2'
    assert await cfg.option('dummy1').owner.get() == 'meta2'
    assert await cfg.option('dummy2').value.get() == 'default'
    assert await cfg.option('dummy2').owner.get() == 'default'
    #
    await meta1.option('dummy1').value.set('meta1')
    await meta1.option('dummy2').value.set('meta1')
    #
    assert await cfg.option('dummy1').value.get() == 'meta1'
    assert await cfg.option('dummy1').owner.get() == 'meta1'
    assert await cfg.option('dummy2').value.get() == 'default'
    assert await cfg.option('dummy2').owner.get() == 'default'
    #
    await meta1.option('dummy1').property.add('force_metaconfig_on_freeze')
    assert await cfg.option('dummy1').value.get() == 'meta2'
    assert await cfg.option('dummy1').owner.get() == 'meta2'
    #
    await meta2.option('dummy1').property.add('force_metaconfig_on_freeze')
    assert await cfg.option('dummy1').value.get() == 'default'
    assert await cfg.option('dummy1').owner.get() == 'default'
    #
    await meta1.option('dummy1').property.pop('force_metaconfig_on_freeze')
    assert await cfg.option('dummy1').value.get() == 'meta1'
    assert await cfg.option('dummy1').owner.get() == 'meta1'
    #
    await cfg.option('dummy1').property.pop('frozen')
    assert await cfg.option('dummy1').value.get() == 'cfg'
    assert await cfg.option('dummy1').owner.get() == 'config'
    await delete_sessions([meta1, meta2])


@pytest.mark.asyncio
async def test_meta_get_config():
    od = make_description()
    meta = await MetaConfig(['name1', 'name2'], optiondescription=od)
    await meta.config.new('meta1', type='metaconfig')
    assert isinstance(await meta.config.get('meta1'), MetaConfig)
    assert isinstance(await meta.config.get('name1'), Config)
    with pytest.raises(ConfigError):
        await meta.config.get('unknown')
    await delete_sessions(meta)
