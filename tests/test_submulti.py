# coding: utf-8
from .autopath import do_autopath
do_autopath()
import pytest
import warnings


from tiramisu.setting import groups, owners
from tiramisu import StrOption, IntOption, OptionDescription, submulti, Leadership, Config, \
                     MetaConfig, undefined, Params, ParamOption, Calculation
from tiramisu.error import LeadershipError
from tiramisu.storage import list_sessions
from .config import event_loop


def return_val(val=None):
    if val is None:
        return 'val'
    else:
        return val


def return_list(value=None):
    return ['val', 'val']


def return_list2(value=None):
    return [['val', 'val']]


@pytest.mark.asyncio
async def test_unknown_multi():
    with pytest.raises(ValueError):
        StrOption('multi', '', multi='unknown')


@pytest.mark.asyncio
async def test_submulti():
    multi = StrOption('multi', '', multi=submulti)
    multi2 = StrOption('multi2', '', default_multi=['yes'], multi=submulti)
    multi3 = StrOption('multi3', '', default=[['yes']], multi=submulti)
    od = OptionDescription('od', '', [multi, multi2, multi3])
    async with await Config(od) as cfg:
        assert await cfg.option('multi').option.ismulti()
        assert await cfg.option('multi').option.issubmulti()
        assert await cfg.option('multi').owner.get() == owners.default
        assert await cfg.option('multi').value.get() == []
        assert await cfg.option('multi').owner.get() == owners.default
        assert await cfg.option('multi').owner.get() == owners.default
        assert await cfg.option('multi3').value.get() == [['yes']]
        assert await cfg.option('multi').owner.get() == owners.default
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_submulti_default_multi_not_list():
    with pytest.raises(ValueError):
        StrOption('multi2', '', default_multi='yes', multi=submulti)


@pytest.mark.asyncio
async def test_append_submulti():
    multi = StrOption('multi', '', multi=submulti)
    multi2 = StrOption('multi2', '', default_multi=['yes'], multi=submulti)
    multi3 = StrOption('multi3', '', default=[['yes']], multi=submulti)
    od = OptionDescription('od', '', [multi, multi2, multi3])
    async with await Config(od) as cfg:
        owner = await cfg.owner.get()
        assert await cfg.option('multi').value.get() == []
        assert await cfg.option('multi').owner.get() == owners.default
        await cfg.option('multi').value.set([undefined])
        assert await cfg.option('multi').owner.get() == owner
        assert await cfg.option('multi').value.get() == [[]]
        await cfg.option('multi').value.set([undefined, ['no']])
        assert await cfg.option('multi').value.get() == [[], ['no']]
        #
        assert await cfg.option('multi2').value.get() == []
        assert await cfg.option('multi2').owner.get() == owners.default
        await cfg.option('multi2').value.set([undefined])
        assert await cfg.option('multi2').owner.get() == owner
        assert await cfg.option('multi2').value.get() == [['yes']]
        await cfg.option('multi2').value.set([undefined, ['no']])
        assert await cfg.option('multi2').value.get() == [['yes'], ['no']]
        #
        assert await cfg.option('multi3').value.get() == [['yes']]
        assert await cfg.option('multi3').owner.get() == owners.default
        await cfg.option('multi3').value.set([undefined, undefined])
        assert await cfg.option('multi3').owner.get() == owner
        assert await cfg.option('multi3').value.get() == [['yes'], []]
        await cfg.option('multi3').value.set([undefined, undefined, ['no']])
        assert await cfg.option('multi3').value.get() == [['yes'], [], ['no']]
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_append_unvalide_submulti():
    multi = StrOption('multi', '', multi=submulti)
    multi2 = StrOption('multi2', '', default_multi=['yes'], multi=submulti)
    multi3 = StrOption('multi3', '', default=[['yes']], multi=submulti)
    od = OptionDescription('od', '', [multi, multi2, multi3])
    async with await Config(od) as cfg:
        assert await cfg.option('multi').value.get() == []
        assert await cfg.option('multi').owner.get() == owners.default
        with pytest.raises(ValueError):
            await cfg.option('multi').value.set([[1]])
        assert await cfg.option('multi').value.get() == []
        assert await cfg.option('multi').owner.get() == owners.default
        #
        assert await cfg.option('multi2').value.get() == []
        with pytest.raises(ValueError):
            await cfg.option('multi2').value.set(['no'])
        assert await cfg.option('multi').owner.get() == owners.default
        assert await cfg.option('multi2').value.get() == []
        #
        assert await cfg.option('multi3').value.get() == [['yes']]
        assert await cfg.option('multi3').owner.get() == owners.default
        with pytest.raises(ValueError):
            await cfg.option('multi3').value.set([[1]])
        assert await cfg.option('multi3').value.get() == [['yes']]
        assert await cfg.option('multi3').owner.get() == owners.default
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_pop_submulti():
    multi = StrOption('multi', '', multi=submulti)
    multi2 = StrOption('multi2', '', default_multi=['yes'], multi=submulti)
    multi3 = StrOption('multi3', '', default=[['yes']], multi=submulti)
    od = OptionDescription('od', '', [multi, multi2, multi3])
    async with await Config(od) as cfg:
        owner = await cfg.owner.get()
        assert await cfg.option('multi').value.get() == []
        assert await cfg.option('multi3').owner.get() == owners.default
        await cfg.option('multi').value.set([['no', 'yes'], ['peharps']])
        assert await cfg.option('multi').owner.get() == owner
        assert await cfg.option('multi').value.get() == [['no', 'yes'], ['peharps']]
        #
        assert await cfg.option('multi3').value.get() == [['yes']]
        assert await cfg.option('multi3').owner.get() == owners.default
        await cfg.option('multi3').value.set([])
        assert await cfg.option('multi').owner.get() == owner
        assert await cfg.option('multi3').value.get() == []
        await cfg.option('multi3').value.reset()
        assert await cfg.option('multi3').owner.get() == owners.default
        await cfg.option('multi3').value.set([[]])
        assert await cfg.option('multi3').owner.get() == owner
        assert await cfg.option('multi3').value.get() == [[]]
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_callback_submulti_str():
    multi = StrOption('multi', '', [[Calculation(return_val)]], multi=submulti, default_multi=[Calculation(return_val)])
    od = OptionDescription('od', '', [multi])
    async with await Config(od) as cfg:
        await cfg.property.read_write()
        owner = await cfg.owner.get()
        assert await cfg.option('multi').owner.get() == owners.default
        assert await cfg.option('multi').value.get() == [['val']]
        await cfg.option('multi').value.set([['val'], undefined])
        assert await cfg.option('multi').owner.get() == owner
        assert await cfg.option('multi').value.get() == [['val'], ['val']]
        await cfg.option('multi').value.reset()
        assert await cfg.option('multi').owner.get() == owners.default
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_callback_submulti_list():
    multi = StrOption('multi', '', [Calculation(return_list)], multi=submulti, default_multi=Calculation(return_list), properties=('notunique',))
    od = OptionDescription('od', '', [multi])
    async with await Config(od) as cfg:
        await cfg.property.read_write()
        owner = await cfg.owner.get()
        assert await cfg.option('multi').value.get() == [['val', 'val']]
        assert await cfg.option('multi').owner.get() == owners.default
        await cfg.option('multi').value.set([['val', 'val'], undefined])
        #assert await cfg.option('multi').owner.get() == owner
        #assert await cfg.option('multi').value.get() == [['val', 'val'], ['val', 'val']]
        #await cfg.option('multi').value.set([['val', 'val'], undefined, undefined])
        #assert await cfg.option('multi').value.get() == [['val', 'val'], ['val', 'val'], ['val', 'val']]
        #await cfg.option('multi').value.reset()
        #assert await cfg.option('multi').owner.get() == owners.default
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_callback_submulti_list_list():
    multi = StrOption('multi', '', Calculation(return_list2), multi=submulti, properties=('notunique',))
    od = OptionDescription('od', '', [multi])
    async with await Config(od) as cfg:
        await cfg.property.read_write()
        owner = await cfg.owner.get()
        assert await cfg.option('multi').value.get() == [['val', 'val']]
        assert await cfg.option('multi').owner.get() == owners.default
        await cfg.option('multi').value.set([['val', 'val'], undefined])
        assert await cfg.option('multi').owner.get() == owner
        assert await cfg.option('multi').value.get() == [['val', 'val'], []]
        await cfg.option('multi').value.reset()
        assert await cfg.option('multi').owner.get() == owners.default
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_groups_with_leader_submulti():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=submulti)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    assert interface1.impl_get_group_type() == groups.leadership


@pytest.mark.asyncio
async def test_groups_with_leader_in_config_submulti():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=submulti)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    async with await Config(od) as cfg:
        pass
    assert interface1.impl_get_group_type() == groups.leadership
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_values_with_leader_and_followers_submulti():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=submulti)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    maconfig = OptionDescription('toto', '', [interface1])
    async with await Config(maconfig) as cfg:
        await cfg.property.read_write()
        owner = await cfg.owner.get()
        assert interface1.impl_get_group_type() == groups.leadership
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').owner.get() == owners.default
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(["192.168.230.145"])
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').value.get() == ["192.168.230.145"]
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == []
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').owner.get() == owner
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).owner.get() == owners.default
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(["192.168.230.145", "192.168.230.147"])
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == []
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 1).value.get() == []
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set(['255.255.255.0'])
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == ['255.255.255.0']
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 1).value.get() == []
        with pytest.raises(ValueError):
            await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set('255.255.255.0')
        with pytest.raises(ValueError):
            await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set([['255.255.255.0']])
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_values_with_leader_and_followers_submulti_default_multi():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=submulti, default_multi=['255.255.0.0', '0.0.0.0'])
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    maconfig = OptionDescription('toto', '', [interface1])
    async with await Config(maconfig) as cfg:
        await cfg.property.read_write()
        owner = await cfg.owner.get()
        assert interface1.impl_get_group_type() == groups.leadership
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').owner.get() == owners.default
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(["192.168.230.145"])
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').value.get() == ["192.168.230.145"]
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == ['255.255.0.0', '0.0.0.0']
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(["192.168.230.145", "192.168.230.147"])
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set(['255.255.255.0'])
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == ['255.255.255.0']
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 1).value.get() == ['255.255.0.0', '0.0.0.0']
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_reset_values_with_leader_and_followers_submulti():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=submulti)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    maconfig = OptionDescription('toto', '', [interface1])
    async with await Config(maconfig) as cfg:
        await cfg.property.read_write()
        owner = await cfg.owner.get()
        assert interface1.impl_get_group_type() == groups.leadership
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').owner.get() == owners.default
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.230.145'])
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').owner.get() == owner
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).owner.get() == owners.default
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.reset()
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').owner.get() == owners.default
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
        #
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.230.145'])
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set(['255.255.255.0'])
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').owner.get() == owner
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).owner.get() == owner
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.reset()
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').owner.get() == owners.default
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_values_with_leader_and_followers_follower_submulti():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True, properties=('notunique',))
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=submulti)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    maconfig = OptionDescription('toto', '', [interface1])
    async with await Config(maconfig) as cfg:
        await cfg.property.read_write()
        with pytest.raises(LeadershipError):
            await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set(['255.255.255.0'])
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.230.145'])
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set(['255.255.255.0'])
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set(['255.255.255.0', '255.255.255.0'])
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.reset()
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set(['255.255.255.0'])
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.230.145', '192.168.230.145'])
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == ['255.255.255.0']
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 1).value.get() == []
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set(['255.255.255.0'])
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_values_with_leader_and_leadership_submulti():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True, properties=('notunique',))
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=submulti)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    maconfig = OptionDescription('toto', '', [interface1])
    async with await Config(maconfig) as cfg:
        await cfg.property.read_write()
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(["192.168.230.145"])
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(["192.168.230.145", "192.168.230.145"])
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set(['255.255.255.0'])
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 1).value.set(['255.255.255.0'])
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == ['255.255.255.0']
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 1).value.get() == ['255.255.255.0']
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.pop(1)
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').value.get() == ["192.168.230.145"]
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == ['255.255.255.0']
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.reset()
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_values_with_leader_owner_submulti():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=submulti)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    maconfig = OptionDescription('toto', '', [interface1])
    async with await Config(maconfig) as cfg:
        await cfg.property.read_write()
        owner = await cfg.owner.get()
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').owner.get() == owners.default
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.230.145'])
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').owner.get() == owner
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).owner.get() == owners.default
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.reset()
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').owner.get() == owners.default
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_values_with_leader_disabled_submulti():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True, properties=('notunique',))
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=submulti)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    maconfig = OptionDescription('toto', '', [interface1])
    async with await Config(maconfig) as cfg:
        await cfg.property.read_write()
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.230.145'])
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.pop(0)
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.230.145'])
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set(['192.168.230.145'])
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.reset()
        await cfg.option('ip_admin_eth0.netmask_admin_eth0').property.add('disabled')
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.230.145', '192.168.230.145'])
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.pop(1)
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.pop(0)

        #delete with value in disabled var
        await cfg.unrestraint.option('ip_admin_eth0.netmask_admin_eth0').property.pop('disabled')
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.230.145'])
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set(['192.168.230.145'])
        await cfg.unrestraint.option('ip_admin_eth0.netmask_admin_eth0').property.add('disabled')
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.pop(0)
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_leader_is_submulti():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=submulti)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    maconfig = OptionDescription('toto', '', [interface1])
    async with await Config(maconfig) as cfg:
        await cfg.property.read_write()
        owner = await cfg.owner.get()
        assert interface1.impl_get_group_type() == groups.leadership
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').owner.isdefault()
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set([["192.168.230.145"]])
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').value.get() == [["192.168.230.145"]]
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == None
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').owner.get() == owner
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).owner.isdefault()
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set([["192.168.230.145"], ["192.168.230.147"]])
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == None
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 1).value.get() == None
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set([["192.168.230.145", '192.168.1.1'], ["192.168.230.147"]])
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').value.get() == [["192.168.230.145", '192.168.1.1'], ["192.168.230.147"]]
        with pytest.raises(ValueError):
            await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.1', '192.168.1.1'])
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_callback_submulti():
    multi = StrOption('multi', '', multi=submulti)
    multi2 = StrOption('multi2', '', Calculation(return_val, Params(ParamOption(multi))), multi=submulti)
    od = OptionDescription('multi', '', [multi, multi2])
    async with await Config(od) as cfg:
        await cfg.property.read_write()
        owner = await cfg.owner.get()
        assert await cfg.option('multi').owner.get() == owners.default
        assert await cfg.option('multi').value.get() == []
        assert await cfg.option('multi2').value.get() == []
        await cfg.option('multi').value.set([['val']])
        assert await cfg.option('multi').owner.get() == owner
        assert await cfg.option('multi2').owner.get() == owners.default
        assert await cfg.option('multi').value.get() == [['val']]
        assert await cfg.option('multi2').value.get() == [['val']]
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_callback_submulti_follower():
    multi = StrOption('multi', '', multi=True)
    multi2 = StrOption('multi2', '', Calculation(return_list), multi=submulti)
    od = Leadership('multi', '', [multi, multi2])
    od = OptionDescription('multi', '', [od])
    async with await Config(od) as cfg:
        await cfg.property.read_write()
        assert await cfg.option('multi.multi').value.get() == []
        await cfg.option('multi.multi').value.set(['val'])
        assert await cfg.option('multi.multi2', 0).value.get() == ['val', 'val']
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_submulti_unique():
    i = IntOption('int', '', multi=submulti, properties=('unique',))
    o = OptionDescription('od', '', [i])
    async with await Config(o) as cfg:
        assert await cfg.option('int').value.get() == []
        await cfg.option('int').value.set([[0]])
        assert await cfg.option('int').value.get() == [[0]]
        with pytest.raises(ValueError):
            await cfg.option('int').value.set([[0, 0]])
        await cfg.option('int').value.set([[0], [0]])
        with pytest.raises(ValueError):
            await cfg.option('int').value.set([[1, 0, 2, 3, 4, 5, 6, 0, 7], [0]])
        await cfg.option('int').value.set([[0, 4, 5, 6], [0]])
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_multi_submulti_meta():
    multi = StrOption('multi', '', multi=submulti)
    od = OptionDescription('od', '', [multi])
    async with await Config(od, session_id='cfg') as cfg:
        await cfg.property.read_write()
        async with await Config(od, session_id='cfg2') as cfg2:
            await cfg2.property.read_write()
            async with await MetaConfig([cfg, cfg2]) as meta:
                await meta.property.read_write()
                await meta.option('multi').value.set([['val']])
                assert await meta.option('multi').value.get() == [['val']]
                newcfg = await meta.config('cfg')
                await newcfg.option('multi').value.set([['val', None]])
                assert await cfg.option('multi').value.get() == [['val', None]]
                newcfg = await meta.config('cfg')
                assert await newcfg.option('multi').value.get() == [['val', None]]
                assert await meta.option('multi').value.get() == [['val']]
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_multi_submulti_meta_no_cache():
    multi = StrOption('multi', '', multi=submulti)
    multi = StrOption('multi', '', multi=submulti)
    od = OptionDescription('od', '', [multi])
    async with await Config(od, session_id='cfg') as cfg:
        await cfg.property.read_write()
        async with await Config(od, session_id='cfg2') as cfg2:
            await cfg.property.read_write()
            async with await MetaConfig([cfg, cfg2]) as meta:
                await meta.property.read_write()
                await meta.property.pop('cache')
                await meta.option('multi').value.set([['val']])
                assert await meta.option('multi').value.get() == [['val']]
                newcfg = await meta.config('cfg')
                await newcfg.option('multi').value.set([['val', None]])
                assert await cfg.option('multi').value.get() == [['val', None]]
                newcfg = await meta.config('cfg')
                assert await newcfg.option('multi').value.get() == [['val', None]]
                assert await meta.option('multi').value.get() == [['val']]
    assert not await list_sessions()
