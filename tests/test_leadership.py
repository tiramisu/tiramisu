# coding: utf-8
from .autopath import do_autopath
do_autopath()
from .config import config_type, get_config, value_list, global_owner, event_loop
import pytest

from tiramisu.setting import groups, owners
from tiramisu import ChoiceOption, BoolOption, IntOption, IPOption, NetworkOption, NetmaskOption, \
    StrOption, OptionDescription, Leadership, Config, delete_session
from tiramisu.error import LeadershipError, PropertiesOptionError, APIError, ConfigError
from tiramisu.storage import list_sessions


groups.family = groups.GroupType('family')


def compare(calculated, expected):
    def convert_list(val):
        if isinstance(val, list):
            val = tuple(val)
        return val
    # convert to tuple
    for idx in range(len(calculated[0])):
        right_idx = expected[0].index(calculated[0][idx])
        for typ in range(4):
            assert convert_list(calculated[typ][idx]) == expected[typ][right_idx]


def make_description():
    numero_etab = StrOption('numero_etab', "identifiant de l'établissement")
    nom_machine = StrOption('nom_machine', "nom de la machine", default="eoleng")
    nombre_interfaces = IntOption('nombre_interfaces', "nombre d'interfaces à activer",
                                  default=1)
    activer_proxy_client = BoolOption('activer_proxy_client', "utiliser un proxy",
                                      default=False)
    mode_conteneur_actif = BoolOption('mode_conteneur_actif', "le serveur est en mode conteneur",
                                      default=False)
    mode_conteneur_actif2 = BoolOption('mode_conteneur_actif2', "le serveur est en mode conteneur2",
                                       default=False, properties=('hidden',))

    adresse_serveur_ntp = StrOption('serveur_ntp', "adresse serveur ntp", multi=True)
    time_zone = ChoiceOption('time_zone', 'fuseau horaire du serveur',
                             ('Paris', 'Londres'), 'Paris')

    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé")
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau")

    leader = OptionDescription('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    interface1 = OptionDescription('interface1', '', [leader])
    interface1.impl_set_group_type(groups.family)

    general = OptionDescription('general', '', [numero_etab, nom_machine,
                                nombre_interfaces, activer_proxy_client,
                                mode_conteneur_actif, mode_conteneur_actif2,
                                adresse_serveur_ntp, time_zone])
    general.impl_set_group_type(groups.family)
    new = OptionDescription('new', '', [], properties=('hidden',))
    new.impl_set_group_type(groups.family)
    creole = OptionDescription('creole', 'first tiramisu configuration', [general, interface1, new])
    descr = OptionDescription('baseconfig', 'baseconifgdescr', [creole])
    return descr


@pytest.mark.asyncio
async def test_base_config(config_type):
    descr = make_description()
    async with await Config(descr) as cfg:
        await cfg.property.read_write()
        cfg = await get_config(cfg, config_type)
        assert await cfg.option('creole.general.activer_proxy_client').value.get() is False
        assert await cfg.option('creole.general.nom_machine').value.get() == "eoleng"
        if config_type != 'tiramisu-api':
            ret = await cfg.option.find('nom_machine', first=True)
            assert await ret.value.get() == "eoleng"
        result = {'general.numero_etab': None, 'general.nombre_interfaces': 1,
                  'general.serveur_ntp': [], 'interface1.ip_admin_eth0.ip_admin_eth0': None,
                  'general.mode_conteneur_actif': False, 'general.time_zone': 'Paris',
                  'interface1.ip_admin_eth0.netmask_admin_eth0': None, 'general.nom_machine':
                  'eoleng', 'general.activer_proxy_client': False}
        assert await cfg.option('creole').value.dict() == result
        result = {'serveur_ntp': [], 'mode_conteneur_actif': False,
                  'ip_admin_eth0': None, 'time_zone': 'Paris', 'numero_etab': None,
                  'netmask_admin_eth0': None, 'nom_machine': 'eoleng', 'activer_proxy_client':
                  False, 'nombre_interfaces': 1}
        assert await cfg.option('creole').value.dict(flatten=True) == result
        if config_type == 'tiramisu-api':
            await cfg.send()
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_get_group_type():
    descr = make_description()
    async with await Config(descr) as cfg:
        await cfg.property.read_write()
        grp = cfg.option('creole.general')
        assert await grp.group_type() == groups.family
        assert await grp.group_type() == 'family'
        assert isinstance(await grp.group_type(), groups.GroupType)
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_iter_on_groups():
    descr = make_description()
    async with await Config(descr) as cfg:
        await cfg.property.read_write()
        result = await cfg.option('creole').list('optiondescription',
                                                 group_type=groups.family)
        group_names = [await res.option.name() for res in result]
        assert group_names == ['general', 'interface1']
        for i in await cfg.option('creole').list('optiondescription',
                                           group_type=groups.family):
            #test StopIteration
            break
        result = await cfg.option('creole').list('option',
                                           group_type=groups.family)
        assert list(result) == []
        result = await cfg.option('creole.general').list('optiondescription',
                                                    group_type=groups.family)
        assert list(result) == []
    assert not await list_sessions()

@pytest.mark.asyncio
async def test_list_recursive():
    descr = make_description()
    async with await Config(descr) as cfg:
        await cfg.property.read_write()
        result = await cfg.option('creole').list('all')
        group_names = [await res.option.name() for res in result]
        assert group_names == ['general', 'interface1']
        #
        result = await cfg.option.list(recursive=True)
        group_names = [await res.option.name() for res in result]
        assert group_names == ['numero_etab', 'nom_machine', 'nombre_interfaces',
                               'activer_proxy_client', 'mode_conteneur_actif',
                               'serveur_ntp', 'time_zone', 'ip_admin_eth0',
                               'netmask_admin_eth0']
        result = list(await cfg.option.list(recursive=True, type='optiondescription'))
        group_names = [await res.option.name() for res in result]
        assert group_names == ['general', 'ip_admin_eth0', 'interface1', 'creole']
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_iter_on_groups_force_permissive():
    descr = make_description()
    async with await Config(descr) as cfg:
        await cfg.property.read_write()
        await cfg.permissive.add('hidden')
        result = await cfg.forcepermissive.option('creole.general').list()
        group_names = [await res.option.name() for res in result]
        ass = ['numero_etab', 'nom_machine', 'nombre_interfaces',
               'activer_proxy_client', 'mode_conteneur_actif',
               'mode_conteneur_actif2', 'serveur_ntp', 'time_zone']
        assert group_names == ass
        # mode_conteneur_actif2 is not visible is not forcepermissive
        result = await cfg.option('creole.general').list()
        group_names = [await res.option.name() for res in result]
        ass.remove('mode_conteneur_actif2')
        assert group_names == ass
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_iter_group_on_groups_force_permissive():
    descr = make_description()
    async with await Config(descr) as cfg:
        await cfg.property.read_write()
        await cfg.permissive.add('hidden')
        result = await cfg.forcepermissive.option('creole').list(type='optiondescription',
                                                           group_type=groups.family)
        group_names = [await res.option.name() for res in result]
        assert group_names == ['general', 'interface1', 'new']
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_iter_on_groups_props():
    descr = make_description()
    async with await Config(descr) as cfg:
        await cfg.property.read_write()
        await cfg.option('creole.interface1').property.add('disabled')
        result = await cfg.option('creole').list(type='optiondescription',
                                           group_type=groups.family)
        group_names = [await res.option.name() for res in result]
        assert group_names == ['general']
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_iter_on_empty_group():
    async with await Config(OptionDescription("name", "descr", [])) as cfg:
        await cfg.property.read_write()
        result = list(await cfg.option.list(type='optiondescription'))
        assert result == []
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_iter_not_group():
    async with await Config(OptionDescription("name", "descr", [])) as cfg:
        await cfg.property.read_write()
        try:
            list(await cfg.option.list(type='optiondescription', group_type='family'))
        except AssertionError:
            pass
        else:
            raise Exception('must raise')
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_groups_with_leader():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    assert interface1.impl_get_group_type() == groups.leadership
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_groups_is_leader(config_type):
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True, default_multi='value')
    interface1 = Leadership('leadership', '', [ip_admin_eth0, netmask_admin_eth0])
    var = StrOption('var', "ip réseau autorisé", multi=True)
    od2 = OptionDescription('od2', '', [var])
    od1 = OptionDescription('od', '', [interface1, od2])
    async with await Config(od1) as cfg:
        cfg = await get_config(cfg, config_type)
        assert not await cfg.option('od2').option.isleadership()
        assert await cfg.option('leadership').option.isleadership()
        assert not await cfg.option('od2.var').option.isleader()
        assert not await cfg.option('od2.var').option.isfollower()
        assert await cfg.option('leadership.ip_admin_eth0').option.ismulti()
        assert await cfg.option('leadership.netmask_admin_eth0').option.ismulti()
        assert not await cfg.option('leadership.ip_admin_eth0').option.issubmulti()
        assert not await cfg.option('leadership.netmask_admin_eth0').option.issubmulti()
        assert await cfg.option('leadership.ip_admin_eth0').option.isleader()
        assert not await cfg.option('leadership.ip_admin_eth0').option.isfollower()
        assert not await cfg.option('leadership.netmask_admin_eth0').option.isleader()
        assert await cfg.option('leadership.netmask_admin_eth0').option.isfollower()
        assert await cfg.option('leadership.netmask_admin_eth0').option.path() == 'leadership.netmask_admin_eth0'
        assert await cfg.option('leadership.netmask_admin_eth0').option.defaultmulti() == 'value'
        if config_type == 'tiramisu-api':
            await cfg.send()
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_groups_with_leader_in_root():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    interface1
    with pytest.raises(ConfigError):
        await Config(interface1, session_id='error')
    await delete_session('error')
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_groups_with_leader_in_config():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    async with await Config(od) as cfg:
        pass
    assert interface1.impl_get_group_type() == groups.leadership
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_groups_with_leader_make_dict(config_type):
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    async with await Config(od) as cfg:
        cfg = await get_config(cfg, config_type)
        assert await cfg.value.dict() == {'ip_admin_eth0.ip_admin_eth0': [], 'ip_admin_eth0.netmask_admin_eth0': []}
        assert await cfg.value.dict(leader_to_list=True) == {'ip_admin_eth0.ip_admin_eth0': []}
        if config_type != 'tiramisu-api':
            # FIXME useful? already in leadership
            assert await cfg.option('ip_admin_eth0.ip_admin_eth0').value.len() == 0
            assert await cfg.option('ip_admin_eth0.netmask_admin_eth0').value.len() == 0
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(['ip1', 'ip2'])
        if config_type != 'tiramisu-api':
            # FIXME
            assert await cfg.option('ip_admin_eth0.ip_admin_eth0').value.len() == 2
            assert await cfg.option('ip_admin_eth0.netmask_admin_eth0').value.len() == 2
        assert await cfg.value.dict() == {'ip_admin_eth0.ip_admin_eth0': ['ip1', 'ip2'], 'ip_admin_eth0.netmask_admin_eth0': [None, None]}
        assert await cfg.value.dict(leader_to_list=True) == {'ip_admin_eth0.ip_admin_eth0': [{'ip_admin_eth0.ip_admin_eth0': 'ip1', 'ip_admin_eth0.netmask_admin_eth0': None}, {'ip_admin_eth0.ip_admin_eth0': 'ip2', 'ip_admin_eth0.netmask_admin_eth0': None}]}
        if config_type == 'tiramisu-api':
            await cfg.send()
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_groups_with_leader_make_dict2(config_type):
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface1 = Leadership('other', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    async with await Config(od) as cfg:
        cfg = await get_config(cfg, config_type)
        assert await cfg.value.dict() == {'other.ip_admin_eth0': [], 'other.netmask_admin_eth0': []}
        assert await cfg.value.dict(leader_to_list=True) == {'other.ip_admin_eth0': []}
        if config_type != 'tiramisu-api':
            # FIXME useful? already in leadership
            assert await cfg.option('other.ip_admin_eth0').value.len() == 0
            assert await cfg.option('other.netmask_admin_eth0').value.len() == 0
        await cfg.option('other.ip_admin_eth0').value.set(['ip1', 'ip2'])
        if config_type != 'tiramisu-api':
            # FIXME
            assert await cfg.option('other.ip_admin_eth0').value.len() == 2
            assert await cfg.option('other.netmask_admin_eth0').value.len() == 2
        assert await cfg.value.dict() == {'other.ip_admin_eth0': ['ip1', 'ip2'], 'other.netmask_admin_eth0': [None, None]}
        assert await cfg.value.dict(leader_to_list=True) == {'other.ip_admin_eth0': [{'other.ip_admin_eth0': 'ip1', 'other.netmask_admin_eth0': None}, {'other.ip_admin_eth0': 'ip2', 'other.netmask_admin_eth0': None}]}
        if config_type == 'tiramisu-api':
            await cfg.send()
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_groups_with_leader_default_value(config_type):
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    async with await Config(od) as cfg:
        cfg = await get_config(cfg, config_type)
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').value.default() == []
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(['ip1', 'ip2'])
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['ip1', 'ip2']
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').value.default() == []
        if config_type == 'tiramisu-api':
            await cfg.send()
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_groups_with_leader_default_value_2(config_type):
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", ['ip1', 'ip2'], multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", default_multi='netmask1', multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    async with await Config(od) as cfg:
        cfg = await get_config(cfg, config_type)
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['ip1', 'ip2']
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').value.default() == ['ip1', 'ip2']
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(['ip3', 'ip4'])
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['ip3', 'ip4']
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').value.default() == ['ip1', 'ip2']
        #
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == 'netmask1'
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 1).value.get() == 'netmask1'
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.default() == 'netmask1'
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 1).value.default() == 'netmask1'
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0').value.default() == ['netmask1', 'netmask1']
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 1).value.set('netmask2')
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == 'netmask1'
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 1).value.get() == 'netmask2'
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.default() == 'netmask1'
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 1).value.default() == 'netmask1'
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0').value.default() == ['netmask1', 'netmask1']
        if config_type == 'tiramisu-api':
            await cfg.send()
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_groups_with_leader_hidden_in_config():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True, properties=('hidden',))
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0], properties=('hidden',))
    od = OptionDescription('root', '', [interface1])
    async with await Config(od) as cfg:
        await cfg.property.read_write()
        await cfg.permissive.add('hidden')
        assert await cfg.forcepermissive.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
        await cfg.forcepermissive.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.1'])
        assert await cfg.forcepermissive.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() is None
        with pytest.raises(PropertiesOptionError):
            await cfg.option('ip_admin_eth0.ip_admin_eth0').value.get()
        with pytest.raises(PropertiesOptionError):
            await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get()
        assert await cfg.value.dict() == {}
        assert await cfg.value.dict(leader_to_list=True) == {}
    assert not await list_sessions()


async def test_groups_with_leader_hidden_in_config2():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True, properties=('hidden',))
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    async with await Config(od) as cfg:
        await cfg.property.read_write()
        await cfg.permissive.add('hidden')
        assert await cfg.forcepermissive.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
        await cfg.forcepermissive.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.1'])
        assert await cfg.forcepermissive.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() is None
        with pytest.raises(PropertiesOptionError):
            await cfg.option('ip_admin_eth0.ip_admin_eth0').value.get()
        with pytest.raises(PropertiesOptionError):
            await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get()
        assert await cfg.value.dict() == {'ip_admin_eth0.ip_admin_eth0': ['192.168.1.1']}
        assert await cfg.value.dict(leader_to_list=True) == {'ip_admin_eth0.ip_admin_eth0': [{'ip_admin_eth0.ip_admin_eth0': '192.168.1.1'}]}
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_groups_with_leader_hidden_in_config2():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True, properties=('hidden',))
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od = OptionDescription('root', '', [interface1])
    async with await Config(od) as cfg:
        await cfg.property.read_write()
        await cfg.permissive.add('hidden')
        assert await cfg.forcepermissive.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
        await cfg.forcepermissive.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.1'])
        with pytest.raises(PropertiesOptionError):
            await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get()
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.reset()
        assert await cfg.forcepermissive.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
        #del
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.1'])
        await cfg.property.pop('hidden')
        assert await cfg.forcepermissive.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() is None
        await cfg.forcepermissive.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set('255.255.255.0')
        assert await cfg.forcepermissive.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == '255.255.255.0'
        await cfg.property.add('hidden')
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.reset()
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.1'])
        await cfg.property.pop('hidden')
        assert await cfg.forcepermissive.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() is None
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_groups_with_leader_reset_empty(config_type):
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od_ = OptionDescription('root', '', [interface1])
    async with await Config(od_) as cfg:
        await cfg.property.read_write()
        cfg = await get_config(cfg, config_type)
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.reset()
        if config_type != 'tiramisu-api':
            with pytest.raises(LeadershipError):
                await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.reset()
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.1'])
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == None
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.reset()
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == None
        if config_type == 'tiramisu-api':
            await cfg.send()
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_groups_with_leader_reset_out_of_range(config_type):
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od_ = OptionDescription('root', '', [interface1])
    async with await Config(od_) as cfg_ori:
        await cfg_ori.property.read_write()
        cfg = await get_config(cfg_ori, config_type)
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.1'])
        if config_type == 'tiramisu-api':
            await cfg.send()
        await cfg_ori.forcepermissive.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set('255.255.255.0')
        cfg = await get_config(cfg_ori, config_type)
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.reset()
        if config_type != 'tiramisu-api':
            # FIXME
            with pytest.raises(LeadershipError):
                await cfg.option('ip_admin_eth0.netmask_admin_eth0', 1).value.reset()
            with pytest.raises(IndexError):
                await cfg.option('ip_admin_eth0.ip_admin_eth0').value.pop(1)
        if config_type == 'tiramisu-api':
            await cfg.send()
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_allowed_groups():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface1 = OptionDescription('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    interface1
    with pytest.raises(ValueError):
        interface1.impl_set_group_type('toto')
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_values_with_leader_disabled_leader(config_type):
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    maconfig = OptionDescription('toto', '', [interface1])
    async with await Config(maconfig) as cfg_ori:
        await cfg_ori.property.read_write()
        cfg = await get_config(cfg_ori, config_type)
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.230.145'])
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.pop(0)
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.230.145'])
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set("192.168.230.145")
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.reset()
        with pytest.raises(LeadershipError):
            await cfg_ori.option('ip_admin_eth0.ip_admin_eth0').property.add('disabled')
        await cfg_ori.option('ip_admin_eth0').property.add('disabled')
        cfg = await get_config(cfg_ori, config_type)
        if config_type != 'tiramisu-api':
            # FIXME
            with pytest.raises(PropertiesOptionError):
                await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set('192.168.230.145')
        if config_type == 'tiramisu-api':
            await cfg.send()
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_sub_group_in_leader_group():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    subgroup = OptionDescription("subgroup", '', [])
    with pytest.raises(ValueError):
        Leadership('ip_admin_eth0', '', [subgroup, ip_admin_eth0, netmask_admin_eth0])
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_group_always_has_multis():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau")
    with pytest.raises(ValueError):
        Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    assert not await list_sessions()


#____________________________________________________________
@pytest.mark.asyncio
async def test_values_with_leader_and_followers1(config_type):
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    maconfig = OptionDescription('toto', '', [interface1])
    async with await Config(maconfig) as cfg:
        await cfg.property.read_write()
        owner = await global_owner(cfg, config_type)
        cfg = await get_config(cfg, config_type)
        assert interface1.impl_get_group_type() == groups.leadership
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').owner.isdefault()
        #
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(["192.168.230.145"])
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').value.get() == ["192.168.230.145"]
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() is None
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').owner.get() == owner
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).owner.isdefault()
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(["192.168.230.145", "192.168.230.147"])
        if config_type != 'tiramisu-api':
            # FIXME
            with pytest.raises(APIError):
                await cfg.option('ip_admin_eth0.netmask_admin_eth0').value.set([None])
            with pytest.raises(APIError):
                await cfg.option('ip_admin_eth0.netmask_admin_eth0').value.pop(0)
        if config_type == 'tiramisu-api':
            await cfg.send()
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_reset_values_with_leader_and_followers(config_type):
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    maconfig = OptionDescription('toto', '', [interface1])
    async with await Config(maconfig) as cfg:
        await cfg.property.read_write()
        owner = await global_owner(cfg, config_type)
    async with await Config(maconfig) as cfg:
        assert interface1.impl_get_group_type() == groups.leadership
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').owner.isdefault()
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(["192.168.230.145"])
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').owner.get() == owner
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).owner.isdefault()
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.reset()
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').owner.isdefault()
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
        #reset
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(["192.168.230.145"])
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.reset()
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').owner.isdefault()
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_reset_values_with_leader_and_followers_default_value():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True, default=['192.168.230.145'])
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True, default=['255.255.255.0'])
    with pytest.raises(ValueError):
        Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_reset_values_with_leader_and_followers_default(config_type):
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True, default=['192.168.230.145'])
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    maconfig = OptionDescription('toto', '', [interface1])
    async with await Config(maconfig) as cfg:
        await cfg.property.read_write()
        owner = await global_owner(cfg, config_type)
        cfg = await get_config(cfg, config_type)
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').owner.isdefault()
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).owner.isdefault()

        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.230.146'])
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').owner.get() == owner
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).owner.isdefault()
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.reset()
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').owner.isdefault()
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).owner.isdefault()
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.230.145']
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set(None)

        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.230.146'])
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set('255.255.255.0')
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').owner.get() == owner
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).owner.get() == owner
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.reset()
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').owner.isdefault()
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).owner.get() == owners.default
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.230.145']
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set(None)

        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set('255.255.255.0')
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').owner.isdefault()
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).owner.get() == owner
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.reset()
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').owner.isdefault()
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).owner.isdefault()
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.230.145']
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set(None)
        if config_type == 'tiramisu-api':
            await cfg.send()
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_values_with_leader_and_followers_follower(config_type):
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True, properties=('notunique',))
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    maconfig = OptionDescription('toto', '', [interface1])
    async with await Config(maconfig) as cfg:
        await cfg.property.read_write()
        cfg = await get_config(cfg, config_type)
        if config_type != 'tiramisu-api':
            with pytest.raises(LeadershipError):
               await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set('255.255.255.0')
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.230.145'])
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set('255.255.255.0')
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.reset()
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set('255.255.255.0')
        #
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.230.145', '192.168.230.145'])
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == '255.255.255.0'
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 1).value.get() is None
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set('255.255.255.0')
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 1).value.set('255.255.255.0')
        if config_type != 'tiramisu-api':
            # FIXME
            with pytest.raises(APIError):
                await cfg.option('ip_admin_eth0.netmask_admin_eth0', 1).value.pop(1)
        #reset
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.230.145',
                                                             '192.168.230.145',
                                                             '192.168.230.145'])
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.reset()
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
        if config_type == 'tiramisu-api':
            await cfg.send()
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_values_with_leader_and_followers_pop(config_type):
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    maconfig = OptionDescription('toto', '', [interface1])
    async with await Config(maconfig) as cfg:
        await cfg.property.read_write()
        cfg = await get_config(cfg, config_type)
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.230.145'])
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set('255.255.255.0')
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.230.145', '192.168.230.146'])
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 1).value.set('255.255.0.0')
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.230.145', '192.168.230.146']
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == '255.255.255.0'
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 1).value.get() == '255.255.0.0'
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.pop(0)
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.230.146']
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == '255.255.0.0'
        if config_type == 'tiramisu-api':
            await cfg.send()
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_values_with_leader_and_followers_leader(config_type):
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True, properties=('notunique',))
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    maconfig = OptionDescription('toto', '', [interface1])
    async with await Config(maconfig) as cfg:
        await cfg.property.read_write()
        cfg = await get_config(cfg, config_type)
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(["192.168.230.145"])
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(["192.168.230.145"])
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(["192.168.230.145", "192.168.230.145"])
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set('255.255.255.0')
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 1).value.set('255.255.255.0')
        if config_type != 'tiramisu-api':
            with pytest.raises(LeadershipError):
                await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.230.145'])
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == '255.255.255.0'
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 1).value.get() == '255.255.255.0'
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.pop(1)
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').value.get() == ["192.168.230.145"]
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == '255.255.255.0'
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.reset()
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
        if config_type == 'tiramisu-api':
            await cfg.send()
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_values_with_leader_and_followers_leader_pop():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True, properties=('notunique',))
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    maconfig = OptionDescription('toto', '', [interface1])
    async with await Config(maconfig) as cfg:
        await cfg.property.read_write()
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(["192.168.230.145", "192.168.230.146"])
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 1).value.set('255.255.0.0')
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').value.get() == ["192.168.230.145", "192.168.230.146"]
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == None
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 1).value.get() == '255.255.0.0'
        compare(await cfg.value.exportation(), (('ip_admin_eth0.ip_admin_eth0', 'ip_admin_eth0.netmask_admin_eth0'), (None, (1,)), (('192.168.230.145', '192.168.230.146'), ('255.255.0.0',)), ('user', ('user',))))
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.pop(0)
        compare(await cfg.value.exportation(), (('ip_admin_eth0.ip_admin_eth0', 'ip_admin_eth0.netmask_admin_eth0'), (None, (0,)), (('192.168.230.146',), ('255.255.0.0',)), ('user', ('user',))))
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').value.get() == ["192.168.230.146"]
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == '255.255.0.0'
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.230.146', "192.168.230.145", "192.168.230.146", "192.168.230.147", "192.168.230.148", "192.168.230.149"])
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 3).value.set('255.255.0.0')
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 4).value.set('255.255.0.0')
        compare(await cfg.value.exportation(), (('ip_admin_eth0.ip_admin_eth0', 'ip_admin_eth0.netmask_admin_eth0'), (None, (0, 3, 4)), (('192.168.230.146', "192.168.230.145", "192.168.230.146", "192.168.230.147", "192.168.230.148", "192.168.230.149"), ('255.255.0.0', '255.255.0.0', '255.255.0.0')), ('user', ('user', 'user', 'user'))))
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.pop(5)
        compare(await cfg.value.exportation(), (('ip_admin_eth0.ip_admin_eth0', 'ip_admin_eth0.netmask_admin_eth0'), (None, (0, 3, 4)), (('192.168.230.146', "192.168.230.145", "192.168.230.146", "192.168.230.147", "192.168.230.148"), ('255.255.0.0', '255.255.0.0', '255.255.0.0')), ('user', ('user', 'user', 'user'))))
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.pop(2)
        compare(await cfg.value.exportation(), (('ip_admin_eth0.ip_admin_eth0', 'ip_admin_eth0.netmask_admin_eth0'), (None, (0, 2, 3)), (('192.168.230.146', "192.168.230.145", "192.168.230.147", "192.168.230.148"), ('255.255.0.0', '255.255.0.0', '255.255.0.0')), ('user', ('user', 'user', 'user'))))
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.pop(2)
        compare(await cfg.value.exportation(), (('ip_admin_eth0.ip_admin_eth0', 'ip_admin_eth0.netmask_admin_eth0'), (None, (0, 2)), (('192.168.230.146', "192.168.230.145", "192.168.230.148"), ('255.255.0.0', '255.255.0.0')), ('user', ('user', 'user'))))
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.pop(2)
        compare(await cfg.value.exportation(), (('ip_admin_eth0.ip_admin_eth0', 'ip_admin_eth0.netmask_admin_eth0'), (None, (0,)), (('192.168.230.146', "192.168.230.145"), ('255.255.0.0',)), ('user', ('user',))))
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_follower_unique():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True, properties=('unique',))
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    maconfig = OptionDescription('toto', '', [interface1])
    async with await Config(maconfig) as cfg:
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(["192.168.230.145", "192.168.230.146"])
        # unique property is removed for a follower
        assert not await cfg.option('ip_admin_eth0.netmask_admin_eth0', 1).property.get()
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_values_with_leader_owner(config_type):
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    maconfig = OptionDescription('toto', '', [interface1])
    async with await Config(maconfig) as cfg:
        await cfg.property.read_write()
        owner = await cfg.owner.get()
        cfg = await get_config(cfg, config_type)
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').owner.isdefault()
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(["192.168.230.145"])
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').owner.get() == owner
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).owner.isdefault()
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.pop(0)
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').owner.get() == owner
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_values_with_leader_disabled(config_type):
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    maconfig = OptionDescription('toto', '', [interface1])
    async with await Config(maconfig) as cfg_ori:
        await cfg_ori.property.read_write()
        cfg = await get_config(cfg_ori, config_type)
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(["192.168.230.145"])
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.pop(0)
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(["192.168.230.145"])
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set("192.168.230.145")
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.pop(0)
        if config_type == 'tiramisu-api':
            await cfg.send()
        await cfg_ori.option('ip_admin_eth0.netmask_admin_eth0').property.add('disabled')
        cfg = await get_config(cfg_ori, config_type)
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(["192.168.230.145"])
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.pop(0)

        if config_type == 'tiramisu-api':
            await cfg.send()
        #delete with value in disabled var
        await cfg_ori.unrestraint.option('ip_admin_eth0.netmask_admin_eth0').property.pop('disabled')
        cfg = await get_config(cfg_ori, config_type)
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(["192.168.230.145"])
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set("192.168.230.145")
        if config_type == 'tiramisu-api':
            await cfg.send()
        await cfg_ori.unrestraint.option('ip_admin_eth0.netmask_admin_eth0').property.add('disabled')
        cfg = await get_config(cfg_ori, config_type)
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.pop(0)

        ##append with value in disabled var
        if config_type == 'tiramisu-api':
            await cfg.send()
        await cfg_ori.unrestraint.option('ip_admin_eth0.netmask_admin_eth0').property.pop('disabled')
        cfg = await get_config(cfg_ori, config_type)
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(["192.168.230.145"])
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set("192.168.230.145")
        if config_type == 'tiramisu-api':
            await cfg.send()
        await cfg_ori.unrestraint.option('ip_admin_eth0.netmask_admin_eth0').property.add('disabled')
        cfg = await get_config(cfg_ori, config_type)
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(["192.168.230.145", '192.168.230.43'])
        if config_type == 'tiramisu-api':
            await cfg.send()
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_multi_non_valid_value(config_type):
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    maconfig = OptionDescription('toto', '', [ip_admin_eth0])
    async with await Config(maconfig) as cfg:
        await cfg.property.read_write()
        cfg = await get_config(cfg, config_type)
        await cfg.option('ip_admin_eth0').value.set(['a'])
        with pytest.raises(ValueError):
            await cfg.option('ip_admin_eth0').value.set([1])
        if config_type == 'tiramisu-api':
            await cfg.send()
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_multi_leader_default_follower(config_type):
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", default_multi="255.255.255.0", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    maconfig = OptionDescription('toto', '', [interface1])
    async with await Config(maconfig) as cfg:
        await cfg.property.read_write()
        cfg = await get_config(cfg, config_type)
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.1'])
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.1']
        if config_type == 'tiramisu-api':
            await cfg.send()
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_groups_with_leader_get_modified_value():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True, properties=('notunique',))
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    maconfig = OptionDescription('toto', '', [interface1])
    async with await Config(maconfig) as cfg:
        await cfg.property.read_write()
        compare(await cfg.value.exportation(), ((), (), (), ()))
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.1'])
        compare(await cfg.value.exportation(), (('ip_admin_eth0.ip_admin_eth0',), (None,), (('192.168.1.1',),), ('user',)))
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set('255.255.255.255')
        compare(await cfg.value.exportation(), (('ip_admin_eth0.ip_admin_eth0', 'ip_admin_eth0.netmask_admin_eth0',), (None, (0,)), (('192.168.1.1',), ('255.255.255.255',)), ('user', ('user',))))
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(['192.168.1.1', '192.168.1.1'])
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 1).value.set('255.255.255.255')
        compare(await cfg.value.exportation(), (('ip_admin_eth0.ip_admin_eth0', 'ip_admin_eth0.netmask_admin_eth0',), (None, (0, 1)), (('192.168.1.1', '192.168.1.1'), ('255.255.255.255', '255.255.255.255')), ('user', ('user', 'user'))))
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_groups_with_leader_importation(config_type):
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    maconfig = OptionDescription('toto', '', [interface1])
    async with await Config(maconfig) as cfg:
        await cfg.property.read_write()
        await cfg.value.importation([['ip_admin_eth0.ip_admin_eth0', 'ip_admin_eth0.netmask_admin_eth0'], [None, [0, 1]], [['192.168.1.1', '192.168.1.0'], ['255.255.255.255', '255.255.255.0']], ['user', ['user', 'user']]])
        cfg = await get_config(cfg, config_type)
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['192.168.1.1', '192.168.1.0']
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == '255.255.255.255'
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 1).value.get() == '255.255.255.0'
        await cfg.option('ip_admin_eth0.ip_admin_eth0').owner.get() == 'user'
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).owner.get() == 'user'
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 1).owner.get() == 'user'
        if config_type == 'tiramisu-api':
            await cfg.send()
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_wrong_index():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True, default=['1.1.1.1'])
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    od1 = OptionDescription('od', '', [interface1])
    maconfig = OptionDescription('toto', '', [od1])
    async with await Config(maconfig) as cfg:
        await cfg.property.read_write()
        assert await cfg.option('od.ip_admin_eth0.ip_admin_eth0').option.get()
        with pytest.raises(APIError):
            await cfg.option('od.ip_admin_eth0.ip_admin_eth0', 0).option.get()
        assert await cfg.option('od.ip_admin_eth0.netmask_admin_eth0', 0).option.get()
        assert await cfg.option('od.ip_admin_eth0').option.get()
        with pytest.raises(APIError):
            await cfg.option('od.ip_admin_eth0', 0).option.get()
        assert await cfg.option('od').option.get()
        with pytest.raises(APIError):
            await cfg.option('od', 0).option.get()
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_without_leader_or_follower():
    with pytest.raises(ValueError):
        Leadership('ip_admin_eth0', '', [])
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True, default=['1.1.1.1'])
    with pytest.raises(ValueError):
        Leadership('ip_admin_eth0', '', [ip_admin_eth0])
    #empty optiondescription is allowed
    OptionDescription('ip_admin_eth0', '', [])
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_leader_not_multi():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé")
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    with pytest.raises(ValueError):
        Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_follower_not_multi():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau")
    with pytest.raises(ValueError):
        Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_follower_force_store_value_none():
    ip_admin_eth0 = IPOption('ip_admin_eth0', "ip réseau autorisé", multi=True, default=['1.1.1.1'])
    netmask_admin_eth0 = NetmaskOption('netmask_admin_eth0', "masque du sous-réseau",  multi=True, properties=('force_store_value',))
    interface0 = Leadership('interface0', '', [ip_admin_eth0, netmask_admin_eth0])
    od1 = OptionDescription('od', '', [interface0])
    od2 = OptionDescription('toto', '', [od1])
    async with await Config(od2) as cfg:
        await cfg.property.read_write()
        assert await cfg.option('od.interface0.netmask_admin_eth0', 0).owner.isdefault()
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_follower_force_store_value():
    ip_admin_eth0 = IPOption('ip_admin_eth0', "ip réseau autorisé", multi=True, default=['1.1.1.1'])
    netmask_admin_eth0 = NetmaskOption('netmask_admin_eth0', "masque du sous-réseau", default_multi='255.255.255.0', multi=True, properties=('force_store_value',))
    interface0 = Leadership('interface0', '', [ip_admin_eth0, netmask_admin_eth0])
    od1 = OptionDescription('od', '', [interface0])
    od2 = OptionDescription('toto', '', [od1])
    async with await Config(od2) as cfg:
        await cfg.property.read_write()
        assert not await cfg.option('od.interface0.netmask_admin_eth0', 0).owner.isdefault()
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_follower_force_store_value_read_only():
    ip_admin_eth0 = IPOption('ip_admin_eth0', "ip réseau autorisé", multi=True, default=['1.1.1.1'])
    netmask_admin_eth0 = NetmaskOption('netmask_admin_eth0', "masque du sous-réseau", default_multi='255.255.255.0', multi=True, properties=('force_store_value',))
    interface0 = Leadership('interface0', '', [ip_admin_eth0, netmask_admin_eth0])
    od1 = OptionDescription('od', '', [interface0])
    od2 = OptionDescription('toto', '', [od1])
    async with await Config(od2) as cfg:
        await cfg.property.read_only()
        assert not await cfg.option('od.interface0.netmask_admin_eth0', 0).owner.isdefault()
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_follower_force_store_value_reset():
    ip_admin_eth0 = IPOption('ip_admin_eth0', "ip réseau autorisé", multi=True, default=['1.1.1.1'])
    netmask_admin_eth0 = NetmaskOption('netmask_admin_eth0', "masque du sous-réseau", default_multi='255.255.255.0', multi=True, properties=('force_store_value',))
    interface0 = Leadership('interface0', '', [ip_admin_eth0, netmask_admin_eth0])
    od1 = OptionDescription('od', '', [interface0])
    od2 = OptionDescription('toto', '', [od1])
    async with await Config(od2) as cfg:
        await cfg.property.read_write()
        await cfg.option('od.interface0.ip_admin_eth0').value.set(['1.1.1.1', '192.168.0.0'])
        assert not await cfg.option('od.interface0.netmask_admin_eth0', 0).owner.isdefault()
        assert not await cfg.option('od.interface0.netmask_admin_eth0', 1).owner.isdefault()
        #
        await cfg.option('od.interface0.netmask_admin_eth0', 1).value.reset()
        assert not await cfg.option('od.interface0.netmask_admin_eth0', 1).owner.isdefault()
        #
        await cfg.option('od.interface0.ip_admin_eth0').value.pop(0)
        await cfg.option('od.interface0.ip_admin_eth0').value.pop(0)
        assert await cfg.option('od.interface0.ip_admin_eth0').value.get() == []
        await cfg.option('od.interface0.ip_admin_eth0').value.reset()
        assert not await cfg.option('od.interface0.netmask_admin_eth0', 0).owner.isdefault()
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_follower_properties():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True, properties=('aproperty',))
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    maconfig = OptionDescription('toto', '', [interface1])
    async with await Config(maconfig) as cfg:
        await cfg.property.read_write()
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(['1.1.1.1', '192.168.0.0'])
        await cfg.option('ip_admin_eth0.netmask_admin_eth0').property.get() == ('aproperty',)
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).property.get() == ('aproperty',)
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 1).property.get() == ('aproperty',)
        #
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).property.add('newproperty')
        await cfg.option('ip_admin_eth0.netmask_admin_eth0').property.get() == ('aproperty',)
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).property.get() == ('aproperty', 'newproperty')
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 1).property.get() == ('aproperty',)
        #
        await cfg.option('ip_admin_eth0.netmask_admin_eth0').property.add('newproperty1')
        await cfg.option('ip_admin_eth0.netmask_admin_eth0').property.get() == ('aproperty', 'newproperty1')
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).property.get() == ('aproperty', 'newproperty', 'newproperty1')
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 1).property.get() == ('aproperty', 'newproperty1')
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_api_get_leader(config_type):
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau", multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    maconfig = OptionDescription('conf', '', [interface1])
    async with await Config(maconfig) as cfg:
        option = await cfg.option('ip_admin_eth0.netmask_admin_eth0').option.leader()
        assert await option.option.get() == ip_admin_eth0

    assert not await list_sessions()
