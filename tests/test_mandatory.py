# coding: utf-8
from .autopath import do_autopath
do_autopath()
# FIXME from .config import config_type, get_config

import pytest
from tiramisu import Config
from tiramisu import IntOption, StrOption, OptionDescription, \
                     SymLinkOption, Leadership, undefined, Calculation, Params, \
                     ParamOption, ParamValue, calc_value
from tiramisu.error import PropertiesOptionError, ConfigError
from tiramisu.setting import groups
from tiramisu.storage import list_sessions
from .config import event_loop


#def teardown_function(function):
#    assert list_sessions() == [], 'session list is not empty when leaving "{}"'.format(function.__name__)


def make_description():
    stro = StrOption('str', '')
    subdescr = OptionDescription('sub', '', [stro], properties=('disabled',))
    stroption = StrOption('str', 'Test string option', default="abc",
                          properties=('mandatory', ))
    stroption1 = StrOption('str1', 'Test string option',
                           properties=('mandatory', ))
    stroption2 = StrOption('unicode2', 'Test string option',
                               properties=('mandatory', ))
    stroption3 = StrOption('str3', 'Test string option', multi=True,
                           properties=('mandatory', ))
    descr = OptionDescription('tiram', '', [subdescr, stroption, stroption1, stroption2, stroption3])
    return descr


def return_value(value):
    return value


def make_description2():
    stroption = StrOption('str', 'Test string option', default="abc",
                          properties=('mandatory', ))
    stroption1 = StrOption('str1', 'Test string option',
                           properties=('mandatory', ))
    stroption2 = SymLinkOption('unicode2', stroption1)
    stroption3 = StrOption('str3', 'Test string option', multi=True,
                           properties=('mandatory', ))
    unicode1 = StrOption('unicode1', 'Test string option', Calculation(return_value, Params(ParamOption(stroption))), properties=('mandatory',))
    descr = OptionDescription('tiram', '', [stroption, stroption1, stroption2, stroption3, unicode1])
    return descr


def make_description_sym():
    stroption = StrOption('str', 'Test string option', default="abc",
                          properties=('mandatory', ))
    stroption1 = StrOption('str1', 'Test string option',
                           properties=('mandatory', ))
    stroption2 = SymLinkOption('unicode2', stroption1)
    stroption3 = StrOption('str3', 'Test string option', multi=True,
                           properties=('mandatory', ))
    descr = OptionDescription('tiram', '', [stroption, stroption1, stroption2, stroption3])
    return descr


def make_description3():
    stroption = StrOption('str', 'Test string option', default="abc",
                          properties=('mandatory', ))
    stroption1 = StrOption('str1', 'Test string option',
                           properties=('mandatory', ))
    stroption2 = SymLinkOption('unicode2', stroption1)
    stroption3 = StrOption('str3', 'Test string option', multi=True,
                           properties=('mandatory', ))
    unicode1 = StrOption('unicode1', 'Test string option', callback=return_value, callback_params=Params(ParamOption(stroption)),  properties=('mandatory', ))
    int1 = IntOption('int1', '', callback=return_value, callback_params=Params(ParamOption(stroption)),  properties=('mandatory', ))
    descr = OptionDescription('tiram', '', [stroption, stroption1, stroption2, stroption3, unicode1, int1])
    return descr


@pytest.mark.asyncio
async def test_mandatory_ro():
    descr = make_description()
    async with await Config(descr) as cfg:
        await cfg.property.read_only()
        prop = []
        try:
            await cfg.option('str1').value.get()
        except PropertiesOptionError as err:
            prop = err.proptype
        assert 'mandatory' in prop
        await cfg.property.read_write()
        await cfg.option('str1').value.set('yes')
        await cfg.property.read_only()
        assert await cfg.option('str1').value.get() == 'yes'
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_mandatory_ro_dict():
    descr = make_description()
    async with await Config(descr) as cfg:
        await cfg.property.read_only()
        prop = []
        try:
            await cfg.value.dict()
        except PropertiesOptionError as err:
            prop = err.proptype
        assert 'mandatory' in prop
        await cfg.property.read_write()
        await cfg.option('str1').value.set('yes')
        await cfg.option('unicode2').value.set('yes')
        await cfg.property.read_only()
        try:
            await cfg.value.dict()
        except PropertiesOptionError as err:
            prop = err.proptype
        assert 'mandatory' in prop
        await cfg.property.read_write()
        await cfg.option('str3').value.set(['yes'])
        await cfg.property.read_only()
        assert await cfg.value.dict() == {'str': 'abc', 'str1': 'yes', 'str3': ['yes'], 'unicode2': 'yes'}
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_mandatory_rw():
    descr = make_description()
    async with await Config(descr) as cfg:
        await cfg.property.read_write()
        # not mandatory in rw
        await cfg.option('str1').value.get()
        await cfg.option('str1').value.set('yes')
        assert await cfg.option('str1').value.get() == 'yes'
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_mandatory_default():
    descr = make_description()
    async with await Config(descr) as cfg:
        await cfg.property.read_only()
        #not mandatory in rw
        await cfg.option('str').value.get()
        await cfg.property.read_write()
        await cfg.option('str').value.set('yes')
        await cfg.property.read_only()
        await cfg.option('str').value.get()
        await cfg.property.read_write()
        await cfg.option('str').value.set(None)
        await cfg.property.read_only()
        prop = []
        try:
            await cfg.option('str').value.get()
        except PropertiesOptionError as err:
            prop = err.proptype
        assert 'mandatory' in prop
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_mandatory_delete():
    descr = make_description()
    async with await Config(descr) as cfg:
        await cfg.property.read_only()
        await cfg.option('str').value.get()
        try:
            await cfg.option('str1').value.get()
        except PropertiesOptionError as err:
            prop = err.proptype
        assert 'mandatory' in prop
        await cfg.property.read_write()
        await cfg.option('str1').value.set('yes')
        await cfg.property.read_only()
        assert await cfg.option('str1').value.get() == 'yes'
        await cfg.property.pop('everything_frozen')
        prop = []
        try:
            await cfg.option('str1').value.reset()
        except PropertiesOptionError as err:
            prop = err.proptype
        assert 'mandatory' in prop
        await cfg.option('str').value.reset()

        assert await cfg.option('str1').value.get() == 'yes'
    assert not await list_sessions()


#valeur vide : None, '', u'', ...
@pytest.mark.asyncio
async def test_mandatory_none():
    descr = make_description()
    async with await Config(descr) as cfg:
        await cfg.option('str1').value.set(None)
        assert await cfg.option('str1').owner.get() == 'user'
        await cfg.property.read_only()
        prop = []
        try:
            await cfg.option('str1').value.get()
        except PropertiesOptionError as err:
            prop = err.proptype
        assert 'mandatory' in prop
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_mandatory_empty():
    descr = make_description()
    async with await Config(descr) as cfg:
        await cfg.option('str1').value.set('')
        assert await cfg.option('str1').owner.get() == 'user'
        await cfg.property.read_only()
        prop = []
        try:
            await cfg.option('str1').value.get()
        except PropertiesOptionError as err:
            prop = err.proptype
        assert 'mandatory' in prop
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_mandatory_multi_none():
    descr = make_description()
    async with await Config(descr) as cfg:
        await cfg.option('str3').value.set([None])
        assert await cfg.option('str3').owner.get() == 'user'
        await cfg.property.read_only()
        prop = []
        try:
            await cfg.option('str3').value.get()
        except PropertiesOptionError as err:
            prop = err.proptype
        assert 'mandatory' in prop
        await cfg.property.read_write()
        await cfg.option('str3').value.set(['yes', None])
        assert await cfg.option('str3').owner.get() == 'user'
        await cfg.property.read_only()
        prop = []
        try:
            await cfg.option('str3').value.get()
        except PropertiesOptionError as err:
            prop = err.proptype
        assert 'mandatory' in prop
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_mandatory_multi_empty():
    descr = make_description()
    async with await Config(descr) as cfg:
        await cfg.option('str3').value.set([])
        assert await cfg.option('str3').owner.get() == 'user'
        await cfg.property.read_only()
        prop = []
        try:
            await cfg.option('str3').value.get()
        except PropertiesOptionError as err:
            prop = err.proptype
        assert 'mandatory' in prop
        #
        await cfg.property.read_write()
        await cfg.option('str3').value.set([''])
        assert await cfg.option('str3').owner.get() == 'user'
        await cfg.property.read_only()
        prop = []
        try:
            await cfg.option('str3').value.get()
        except PropertiesOptionError as err:
            prop = err.proptype
        assert 'mandatory' in prop
        #
        await cfg.property.read_write()
        await cfg.option('str3').value.set(['yes', ''])
        assert await cfg.option('str3').owner.get() == 'user'
        await cfg.property.read_only()
        prop = []
        try:
            await cfg.option('str3').value.get()
        except PropertiesOptionError as err:
            prop = err.proptype
        assert 'mandatory' in prop
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_mandatory_multi_append():
    descr = make_description()
    async with await Config(descr) as cfg:
        await cfg.option('str3').value.set(['yes'])
        await cfg.property.read_write()
        ret = await cfg.option('str3').value.get()
        ret.append(None)
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_mandatory_disabled():
    descr = make_description()
    async with await Config(descr) as cfg:
        await cfg.option('str1').value.get()
        await cfg.option('str1').property.add('disabled')
        await cfg.property.read_only()
        pop = []
        try:
            await cfg.option('str1').value.get()
        except PropertiesOptionError as err:
            prop = err.proptype
        search_prop = {'disabled'}
        assert set(prop) == search_prop
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_mandatory_unicode():
    descr = make_description()
    async with await Config(descr) as cfg:
        await cfg.option('unicode2').value.get()
        await cfg.property.read_only()
        prop = []
        try:
            await cfg.option('unicode2').value.get()
        except PropertiesOptionError as err:
            prop = err.proptype
        assert 'mandatory' in prop
        await cfg.property.read_write()
        await cfg.option('unicode2').value.set(u'')
        await cfg.property.read_only()
        prop = []
        try:
            await cfg.option('unicode2').value.get()
        except PropertiesOptionError as err:
            prop = err.proptype
        assert 'mandatory' in prop
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_mandatory_warnings_ro():
    descr = make_description()
    async with await Config(descr) as cfg:
        await cfg.option('str').value.set('')
        await cfg.property.read_only()
        proc = []
        try:
            await cfg.option('str').value.get()
        except PropertiesOptionError as err:
            prop = err.proptype
        assert 'mandatory' in prop
        assert list(await cfg.value.mandatory()) == ['str', 'str1', 'unicode2', 'str3']
        await cfg.property.read_write()
        await cfg.option('str').value.set('a')
        await cfg.property.read_only()
        assert list(await cfg.value.mandatory()) == ['str1', 'unicode2', 'str3']
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_mandatory_warnings_rw():
    descr = make_description()
    async with await Config(descr) as cfg:
        await cfg.option('str').value.set('')
        await cfg.property.read_write()
        await cfg.option('str').value.get()
        assert list(await cfg.value.mandatory()) == ['str', 'str1', 'unicode2', 'str3']
        await cfg.option('str').value.set('a')
        assert list(await cfg.value.mandatory()) == ['str1', 'unicode2', 'str3']
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_mandatory_warnings_disabled():
    descr = make_description()
    async with await Config(descr) as cfg:
        await cfg.option('str').value.set('')
        await cfg.property.read_write()
        await cfg.option('str').value.get()
        assert set(await cfg.value.mandatory()) == {'str', 'str1', 'unicode2', 'str3'}
        await cfg.option('str').property.add('disabled')
        assert set(await cfg.value.mandatory()) == {'str1', 'unicode2', 'str3'}
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_mandatory_warnings_hidden():
    descr = make_description()
    async with await Config(descr) as cfg:
        await cfg.option('str').value.set('')
        await cfg.property.read_write()
        await cfg.permissive.add('hidden')
        await cfg.option('str').value.get()
        assert set(await cfg.value.mandatory()) == {'str', 'str1', 'unicode2', 'str3'}
        await cfg.option('str').property.add('hidden')
        assert set(await cfg.value.mandatory()) == {'str', 'str1', 'unicode2', 'str3'}
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_mandatory_warnings_frozen():
    descr = make_description()
    async with await Config(descr) as cfg:
        await cfg.option('str').value.set('')
        await cfg.property.read_write()
        await cfg.option('str').value.get()
        assert set(await cfg.value.mandatory()) == {'str', 'str1', 'unicode2', 'str3'}
        await cfg.option('str').property.add('frozen')
        await cfg.property.read_only()
        assert set(await cfg.value.mandatory()) == {'str', 'str1', 'unicode2', 'str3'}
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_mandatory_leader():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True,
                              properties=('mandatory', ))
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau",
                                   multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    descr = OptionDescription('o', '', [interface1])
    async with await Config(descr) as cfg:
        await cfg.property.read_only()
        with pytest.raises(PropertiesOptionError):
            await cfg.option('ip_admin_eth0.ip_admin_eth0').value.get()
        with pytest.raises(PropertiesOptionError):
            await cfg.value.dict()
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_mandatory_warnings_leader():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True,
                              properties=('mandatory', ))
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau",
                                   multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    descr = OptionDescription('o', '', [interface1])
    async with await Config(descr) as cfg:
        assert list(await cfg.value.mandatory()) == ['ip_admin_eth0.ip_admin_eth0']
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_mandatory_leader_empty():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau",
                                   multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    descr = OptionDescription('o', '', [interface1])
    async with await Config(descr) as cfg:
        await cfg.property.read_write()
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
        #
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set([undefined])
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').value.get() == [None]
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == None
        await cfg.property.read_only()
        with pytest.raises(PropertiesOptionError):
            await cfg.option('ip_admin_eth0.ip_admin_eth0').value.get()
        with pytest.raises(PropertiesOptionError):
            await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get()
        await cfg.property.read_write()
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.reset()
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
        #
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set([''])
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['']
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == None
        await cfg.property.read_only()
        with pytest.raises(PropertiesOptionError):
            await cfg.option('ip_admin_eth0.ip_admin_eth0').value.get()
        with pytest.raises(PropertiesOptionError):
            await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get()
        await cfg.property.read_write()
        #
        await cfg.property.read_write()
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(['ip'])
        await cfg.property.read_only()
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['ip']
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == None
        #
        await cfg.property.read_write()
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(['ip2'])
        await cfg.property.read_only()
        with pytest.raises(PropertiesOptionError):
            await cfg.option('ip_admin_eth0.ip_admin_eth0').value.reset()
        await cfg.property.read_write()
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.reset()
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_mandatory_warnings_leader_empty():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau",
                                   multi=True)
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    descr = OptionDescription('o', '', [interface1])
    async with await Config(descr) as cfg:
        await cfg.property.read_write()
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set([undefined])
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').value.get() == [None]
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == None
        assert list(await cfg.value.mandatory()) == ['ip_admin_eth0.ip_admin_eth0']
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.reset()
        #
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set([''])
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['']
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == None
        assert list(await cfg.value.mandatory()) == ['ip_admin_eth0.ip_admin_eth0']
        #
        await cfg.property.read_write()
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(['ip'])
        assert list(await cfg.value.mandatory()) == []
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_mandatory_follower():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau",
                                   multi=True, properties=('mandatory', ))
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    descr = OptionDescription('o', '', [interface1])
    async with await Config(descr) as cfg:
        await cfg.property.read_only()
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
        assert await cfg.value.dict() == {'ip_admin_eth0.ip_admin_eth0': [],
                                         'ip_admin_eth0.netmask_admin_eth0': []}
        #
        await cfg.property.read_write()
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(['ip'])
        await cfg.property.read_only()
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['ip']
        with pytest.raises(PropertiesOptionError):
            await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get()
        #
        await cfg.property.read_write()
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set('')
        await cfg.property.read_only()
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['ip']
        with pytest.raises(PropertiesOptionError):
            await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get()
        #
        await cfg.property.read_write()
        await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.set('ip')
        await cfg.property.read_only()
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').value.get() == ['ip']
        assert await cfg.option('ip_admin_eth0.netmask_admin_eth0', 0).value.get() == 'ip'
        assert await cfg.value.dict() == {'ip_admin_eth0.ip_admin_eth0': ['ip'],
                                          'ip_admin_eth0.netmask_admin_eth0': ['ip']}
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_mandatory_warnings_follower():
    ip_admin_eth0 = StrOption('ip_admin_eth0', "ip réseau autorisé", multi=True)
    netmask_admin_eth0 = StrOption('netmask_admin_eth0', "masque du sous-réseau",
                                   multi=True, properties=('mandatory', ))
    interface1 = Leadership('ip_admin_eth0', '', [ip_admin_eth0, netmask_admin_eth0])
    descr = OptionDescription('o', '', [interface1])
    async with await Config(descr) as cfg:
        await cfg.property.read_only()
        assert await cfg.option('ip_admin_eth0.ip_admin_eth0').value.get() == []
        #
        await cfg.property.read_write()
        assert list(await cfg.value.mandatory()) == []
        await cfg.option('ip_admin_eth0.ip_admin_eth0').value.set(['ip'])
        assert list(await cfg.value.mandatory()) == ['ip_admin_eth0.netmask_admin_eth0']
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_mandatory_warnings_symlink():
    descr = make_description_sym()
    async with await Config(descr) as cfg:
        await cfg.option('str').value.set('')
        await cfg.property.read_write()
        await cfg.option('str').value.get()
        assert list(await cfg.value.mandatory()) == ['str', 'str1', 'str3']
        await cfg.option('str').property.add('frozen')
        await cfg.property.read_only()
        assert list(await cfg.value.mandatory()) == ['str', 'str1', 'str3']
    assert not await list_sessions()


#@pytest.mark.asyncio
#async def test_mandatory_warnings_validate():
#    descr = make_description3()
#    cfg = await Config(descr)
#    await cfg.option('str').value.set('')
#    raises(ValueError, "list(await cfg.value.mandatory())")
#    await cfg.option('str').value.set('test')
#    raises(ValueError, "list(await cfg.value.mandatory())")


@pytest.mark.asyncio
async def test_mandatory_warnings_validate_empty():
    descr = make_description2()
    async with await Config(descr) as cfg:
        await cfg.option('str').value.set('')
        await cfg.property.read_only()
        assert list(await cfg.value.mandatory()) == ['str', 'str1', 'str3']
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_mandatory_warnings_requires():
    stroption = StrOption('str', 'Test string option', default="abc",
                          properties=('mandatory', ))
    stroption1 = StrOption('str1', 'Test string option',
                           properties=('mandatory', ))
    stroption2 = StrOption('unicode2', 'Test string option',
                               properties=('mandatory', ))
    mandatory_property = Calculation(calc_value,
                                     Params(ParamValue('mandatory'),
                                            kwargs={'condition': ParamOption(stroption, notraisepropertyerror=True),
                                                    'expected': ParamValue('yes'),
                                                    'no_condition_is_invalid': ParamValue(True)}))
    stroption3 = StrOption('str3', 'Test string option', multi=True, properties=(mandatory_property,))
    descr = OptionDescription('tiram', '', [stroption, stroption1, stroption2, stroption3])
    async with await Config(descr) as cfg:
        await cfg.option('str').value.set('')
        await cfg.property.read_write()
        await cfg.option('str').value.get()
        assert list(await cfg.value.mandatory()) == ['str', 'str1', 'unicode2']
        await cfg.property.read_only()
        assert list(await cfg.value.mandatory()) == ['str', 'str1', 'unicode2']
        await cfg.property.read_write()
        await cfg.option('str').value.set('yes')
        assert list(await cfg.value.mandatory()) == ['str1', 'unicode2', 'str3']
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_mandatory_warnings_requires_leadership():
    stroption = StrOption('str', 'Test string option', default="abc",
                          properties=('mandatory', ))
    stroption1 = StrOption('str1', 'Test string option', multi=True)
    mandatory_property = Calculation(calc_value,
                                     Params(ParamValue(None),
                                            kwargs={'condition': ParamOption(stroption),
                                                    'expected': ParamValue('yes'),
                                                    'reverse_condition': ParamValue(True),
                                                    'default': ParamValue('mandatory')}))
    stroption2 = StrOption('str2', 'Test string option', multi=True, properties=(mandatory_property,))
    leadership = Leadership('leader', 'leadership', [stroption1, stroption2])
    descr = OptionDescription('tiram', '', [stroption, leadership])
    async with await Config(descr) as cfg:
        await cfg.option('str').value.set('')
        await cfg.option('leader.str1').value.set(['str'])
        assert list(await cfg.value.mandatory()) == ['str']
        await cfg.option('str').value.set('yes')
        assert list(await cfg.value.mandatory()) == ['leader.str2']
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_mandatory_warnings_requires_leadership_follower():
    stroption = StrOption('str', 'Test string option', multi=True)
    stroption1 = StrOption('str1', 'Test string option', multi=True)
    mandatory_property = Calculation(calc_value,
                                     Params(ParamValue(None),
                                            kwargs={'condition': ParamOption(stroption1),
                                                    'expected': ParamValue('yes'),
                                                    'reverse_condition': ParamValue(True),
                                                    'default': ParamValue('mandatory')}))
    stroption2 = StrOption('str2', 'Test string option', multi=True, properties=(mandatory_property,))
    leadership = Leadership('leader', 'leadership', [stroption, stroption1, stroption2])
    descr = OptionDescription('tiram', '', [leadership])
    async with await Config(descr) as cfg:
        await cfg.option('leader.str').value.set(['str'])
        assert list(await cfg.value.mandatory()) == []
        await cfg.option('leader.str1', 0).value.set('yes')
        assert list(await cfg.value.mandatory()) == ['leader.str2']
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_mandatory_od_disabled():
    descr = make_description()
    descr = OptionDescription('od', '', [descr])
    async with await Config(descr) as cfg:
        await cfg.property.read_only()
        assert list(await cfg.value.mandatory()) == ['tiram.str1', 'tiram.unicode2', 'tiram.str3']
        await cfg.option('tiram').property.add('disabled')
        assert list(await cfg.value.mandatory()) == []
    assert not await list_sessions()
