# coding: utf-8
from py.test import raises
import pytest

from .autopath import do_autopath
do_autopath()
from .config import config_type, get_config, value_list, global_owner, event_loop

from tiramisu import ChoiceOption, StrOption, OptionDescription, Config, owners, Calculation, \
                     undefined, Params, ParamValue, ParamOption, list_sessions
from tiramisu.error import ConfigError


def return_val(val):
    return val


def return_list():
    return ['val1', 'val2']


def return_calc_list(val):
    return [val]


def return_error(*args, **kwargs):
    raise Exception('test')


@pytest.mark.asyncio
async def test_choiceoption(config_type):
    choice = ChoiceOption('choice', '', values=('val1', 'val2'))
    odesc = OptionDescription('od', '', [choice])
    async with await Config(odesc) as cfg:
        await cfg.property.read_write()
        cfg = await get_config(cfg, config_type)
        owner = await global_owner(cfg, config_type)
        assert await cfg.option('choice').owner.get() == owners.default
        assert await cfg.option('choice').owner.isdefault()
        #
        await cfg.option('choice').value.set('val1')
        assert await cfg.option('choice').owner.get() == owner
        assert not await cfg.option('choice').owner.isdefault()
        #
        await cfg.option('choice').value.reset()
        assert await cfg.option('choice').owner.get() == owners.default
        assert await cfg.option('choice').owner.isdefault()
        #
        with pytest.raises(ValueError):
            await cfg.option('choice').value.set('no')
        assert await cfg.option('choice').owner.get() == owners.default
        assert await cfg.option('choice').owner.isdefault()
        #
        assert value_list(await cfg.option('choice').value.list()) == ('val1', 'val2')
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_choiceoption_function(config_type):
    choice = ChoiceOption('choice', '', values=Calculation(return_list))
    odesc = OptionDescription('od', '', [choice])
    async with await Config(odesc) as cfg:
        await cfg.property.read_write()
        cfg = await get_config(cfg, config_type)
        owner = await global_owner(cfg, config_type)
        assert await cfg.option('choice').owner.isdefault()
        #
        await cfg.option('choice').value.set('val1')
        assert await cfg.option('choice').owner.get() == owner
        #
        await cfg.option('choice').value.reset()
        assert await cfg.option('choice').owner.isdefault()
        #
        with pytest.raises(ValueError):
            await cfg.option('choice').value.set('no')
        assert await cfg.option('choice').owner.isdefault()
        #
        assert value_list(await cfg.option('choice').value.list()) == ('val1', 'val2')
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_choiceoption_function_error():
    choice = ChoiceOption('choice', '', values=Calculation(return_error))
    odesc = OptionDescription('od', '', [choice])
    async with await Config(odesc) as cfg:
        await cfg.property.read_write()
        with pytest.raises(ConfigError):
            await cfg.option('choice').value.set('val1')
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_choiceoption_function_error_args():
    choice = ChoiceOption('choice', '', values=Calculation(return_error, Params(ParamValue('val1'))))
    odesc = OptionDescription('od', '', [choice])
    async with await Config(odesc) as cfg:
        await cfg.property.read_write()
        with pytest.raises(ConfigError):
            await cfg.option('choice').value.set('val1')
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_choiceoption_function_error_kwargs():
    choice = ChoiceOption('choice', '', values=Calculation(return_error, Params(kwargs={'kwargs': ParamValue('val1')})))
    odesc = OptionDescription('od', '', [choice])
    async with await Config(odesc) as cfg:
        await cfg.property.read_write()
        with pytest.raises(ConfigError):
            await cfg.option('choice').value.set('val1')
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_choiceoption_calc_function(config_type):
    choice = ChoiceOption('choice', "", values=Calculation(return_calc_list, Params(ParamValue('val1'))))
    odesc = OptionDescription('od', '', [choice])
    async with await Config(odesc) as cfg:
        await cfg.property.read_write()
        cfg = await get_config(cfg, config_type)
        owner = await global_owner(cfg, config_type)
        assert await cfg.option('choice').owner.isdefault()
        #
        await cfg.option('choice').value.set('val1')
        assert await cfg.option('choice').owner.get() == owner
        #
        await cfg.option('choice').value.reset()
        assert await cfg.option('choice').owner.isdefault()
        #
        with pytest.raises(ValueError):
            await cfg.option('choice').value.set('no')
        assert await cfg.option('choice').owner.isdefault()
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_choiceoption_calc_opt_function(config_type):
    str_ = StrOption('str', '', 'val1')
    choice = ChoiceOption('choice',
                          "",
                          values=Calculation(return_calc_list, Params(ParamOption(str_))))
    odesc = OptionDescription('od', '', [str_, choice])
    async with await Config(odesc) as cfg:
        await cfg.property.read_write()
        owner = await cfg.owner.get()
        cfg = await get_config(cfg, config_type)
        assert await cfg.option('choice').owner.isdefault()
        #
        await cfg.option('choice').value.set('val1')
        assert await cfg.option('choice').owner.get() == owner
        #
        await cfg.option('choice').value.reset()
        assert await cfg.option('choice').owner.isdefault()
        #
        with pytest.raises(ValueError):
            await cfg.option('choice').value.set('no')
        assert await cfg.option('choice').owner.isdefault()
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_choiceoption_calc_opt_function_propertyerror():
    str_ = StrOption('str', '', 'val1', properties=('disabled',))
    choice = ChoiceOption('choice',
                          "",
                          values=Calculation(return_calc_list, Params(ParamOption(str_))))
    odesc = OptionDescription('od', '', [str_, choice])
    async with await Config(odesc) as cfg:
        await cfg.property.read_write()
        with pytest.raises(ConfigError):
            await cfg.option('choice').value.set('no')
    assert not await list_sessions()


#def test_choiceoption_calc_opt_multi_function(config_type):
@pytest.mark.asyncio
async def test_choiceoption_calc_opt_multi_function():
    # FIXME
    config_type = 'tiramisu'
    str_ = StrOption('str', '', ['val1'], multi=True)
    choice = ChoiceOption('choice',
                          "",
                          default_multi='val2',
                          values=Calculation(return_val, Params(ParamOption(str_))),
                          multi=True)
    ch2 = ChoiceOption('ch2',
                       "",
                       default=['val2'],
                       values=Calculation(return_val, Params(ParamOption(str_))),
                       multi=True)
    odesc = OptionDescription('od', '', [str_, choice, ch2])
    async with await Config(odesc) as cfg:
        await cfg.property.read_write()
        owner = await cfg.owner.get()
        cfg = await get_config(cfg, config_type, True)
        assert await cfg.option('choice').owner.isdefault()
        assert await cfg.option('choice').value.get() == []
        #
        await cfg.option('choice').value.set(['val1'])
        assert await cfg.option('choice').owner.get() == owner
        #
        with pytest.raises(ValueError):
            await cfg.option('choice').value.set([undefined])
        #
        await cfg.option('choice').value.set(['val1'])
        assert await cfg.option('choice').owner.get() == owner
        #
        await cfg.option('choice').value.reset()
        assert await cfg.option('choice').owner.isdefault()
        #
        with pytest.raises(ValueError):
            await cfg.option('choice').value.set('no')
        assert await cfg.option('choice').owner.isdefault()
        #
        with pytest.raises(ValueError):
            await cfg.option('ch2').value.get()
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_choiceoption_calc_opt_multi_function_kwargs(config_type):
    str_ = StrOption('str', '', ['val1'], multi=True)
    choice = ChoiceOption('choice',
                          "",
                          default_multi='val2',
                          values=Calculation(return_val, Params(kwargs={'val': ParamOption(str_)})),
                          multi=True)
    ch2 = ChoiceOption('ch2',
                       "",
                       default=['val2'],
                       values=Calculation(return_val, Params(kwargs={'val': ParamOption(str_)})),
                       multi=True)
    odesc = OptionDescription('od', '', [str_, choice, ch2])
    async with await Config(odesc) as cfg:
        await cfg.property.read_write()
        owner = await cfg.owner.get()
        # FIXME cfg = await get_config(cfg, config_type)
        assert await cfg.option('choice').owner.isdefault()
        assert await cfg.option('choice').value.get() == []
        #
        await cfg.option('choice').value.set(['val1'])
        assert await cfg.option('choice').owner.get() == owner
        #
        with pytest.raises(ValueError):
            await cfg.option('choice').value.set([undefined])
        #
        await cfg.option('choice').value.set(['val1'])
        assert await cfg.option('choice').owner.get() == owner
        #
        await cfg.option('choice').value.reset()
        assert await cfg.option('choice').owner.isdefault()
        #
        with pytest.raises(ValueError):
            await cfg.option('choice').value.set('no')
        assert await cfg.option('choice').owner.isdefault()
        #
        with pytest.raises(ValueError):
            await cfg.option('ch2').value.get()
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_choiceoption_calc_not_list():
    str_ = StrOption('str', '', 'val1')
    choice = ChoiceOption('choice',
                          "",
                          default_multi='val2',
                          values=Calculation(return_val, Params(ParamOption(str_))),
                          multi=True)
    odesc = OptionDescription('od', '', [str_, choice])
    async with await Config(odesc) as cfg:
        await cfg.property.read_write()
        with pytest.raises(ConfigError):
            await cfg.option('choice').value.set(['val1'])
    assert not await list_sessions()
