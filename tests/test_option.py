"""these tests are here to create some :class:`tiramisu.option.Option`'s
and to compare them
"""
from .autopath import do_autopath
do_autopath()

import pytest
import warnings

from tiramisu.error import APIError, ConfigError
from tiramisu import IntOption, SymLinkOption, OptionDescription, Config, Calculation, groups, list_sessions
from tiramisu.i18n import _
from .config import event_loop

try:
    groups.family
except:
    groups.family = groups.GroupType('family')


def a_func():
    return None


@pytest.mark.asyncio
async def test_option_valid_name():
    IntOption('test', '')
    with pytest.raises(ValueError):
        IntOption(1, "")
    i = IntOption("test1", "")
    with pytest.raises(ValueError):
        SymLinkOption(1, i)
    i = SymLinkOption("test1", i)
#
#
#@pytest.mark.asyncio
#async def test_option_unvalid_name():
#    with pytest.raises(ValueError):
#        IntOption('test.', '')
#    with pytest.raises(ValueError):
#        IntOption('test.val', '')
#    with pytest.raises(ValueError):
#        IntOption('.test', '')
#    with pytest.raises(ValueError):
#        OptionDescription('.test', '', [])


@pytest.mark.asyncio
async def test_option_get_information():
    description = "it's ok"
    string = 'some informations'
    i = IntOption('test', description)
    with pytest.raises(ValueError):
        i.impl_get_information('noinfo')
    i.impl_set_information('info', string)
    assert i.impl_get_information('info') == string
    with pytest.raises(ValueError):
        i.impl_get_information('noinfo')
    assert i.impl_get_information('noinfo', 'default') == 'default'
    assert i.impl_get_information('doc') == description


@pytest.mark.asyncio
async def test_option_get_information_config():
    description = "it's ok"
    string = 'some informations'
    i = IntOption('test', description)
    od = OptionDescription('od', '', [i])
    async with await Config(od) as cfg:
        pass
    with pytest.raises(ValueError):
        i.impl_get_information('noinfo')
    with pytest.raises(AttributeError):
        i.impl_set_information('info', string)
    with pytest.raises(ValueError):
        i.impl_get_information('noinfo')
    assert i.impl_get_information('noinfo', 'default') == 'default'
    assert i.impl_get_information('doc') == description
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_option_get_information_default():
    description = "it's ok"
    string = 'some informations'
    i = IntOption('test', description)
    i.impl_set_information('noinfo', 'optdefault')
    od = OptionDescription('od', '', [i])
    async with await Config(od) as cfg:
        #
        assert await cfg.option('test').information.get('noinfo', 'falsedefault') == 'optdefault'
        #
        await cfg.option('test').information.set('noinfo', 'notdefault')
        assert await cfg.option('test').information.get('noinfo', 'falsedefault') == 'notdefault'
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_option_get_information_config2():
    description = "it's ok"
    string = 'some informations'
    i = IntOption('test', description)
    i.impl_set_information('info', string)
    od = OptionDescription('od', '', [i])
    async with await Config(od) as cfg:
        pass
    with pytest.raises(ValueError):
        i.impl_get_information('noinfo')
    with pytest.raises(AttributeError):
        i.impl_set_information('info', 'hello')
    assert i.impl_get_information('info') == string
    with pytest.raises(ValueError):
        i.impl_get_information('noinfo')
    assert i.impl_get_information('noinfo', 'default') == 'default'
    assert i.impl_get_information('doc') == description
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_optiondescription_get_information():
    description = "it's ok"
    string = 'some informations'
    o = OptionDescription('test', description, [])
    o.impl_set_information('info', string)
    assert o.impl_get_information('info') == string
    with pytest.raises(ValueError):
        o.impl_get_information('noinfo')
    assert o.impl_get_information('noinfo', 'default') == 'default'
    assert o.impl_get_information('doc') == description
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_option_isoptiondescription():
    i = IntOption('test', '')
    od = OptionDescription('od', '', [i])
    od = OptionDescription('od', '', [od])
    async with await Config(od) as cfg:
        assert await cfg.option('od').option.isoptiondescription()
        assert not await cfg.option('od.test').option.isoptiondescription()
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_option_double():
    i = IntOption('test', '')
    od = OptionDescription('od1', '', [i])
    od = OptionDescription('od2', '', [od])
    od = OptionDescription('od3', '', [od])
    async with await Config(od) as cfg:
        assert await cfg.option('od2.od1.test').value.get() is None
        assert await cfg.option('od2').option('od1').option('test').value.get() is None
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_option_multi():
    IntOption('test', '', multi=True)
    IntOption('test', '', multi=True, default_multi=1)
    IntOption('test', '', default=[1], multi=True, default_multi=1)
    #add default_multi to not multi's option
    with pytest.raises(ValueError):
        IntOption('test', '', default_multi=1)
    #unvalid default_multi
    with pytest.raises(ValueError):
        IntOption('test', '', multi=True, default_multi='yes')
    assert not await list_sessions()


#@pytest.mark.asyncio
#async def test_option_multi_legacy():
#    #not default_multi with callback
#    #with pytest.raises(ValueError):
#        IntOption('test', '', multi=True, default_multi=1, callback=a_func)")


@pytest.mark.asyncio
async def test_unknown_option():
    i = IntOption('test', '')
    od1 = OptionDescription('od', '', [i])
    od2 = OptionDescription('od', '', [od1])
    async with await Config(od2) as cfg:
        # test is an option, not an optiondescription
        with pytest.raises(TypeError):
            await cfg.option('od.test.unknown').value.get()
        # unknown is an unknown option
        with pytest.raises(AttributeError):
            await cfg.option('unknown').value.get()
        # unknown is an unknown option
        with pytest.raises(AttributeError):
            await cfg.option('od.unknown').value.get()
        # unknown is an unknown optiondescription
        with pytest.raises(AttributeError):
            await cfg.option('od.unknown.suboption').value.get()
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_optiondescription_list():
    groups.notfamily1 = groups.GroupType('notfamily1')
    i = IntOption('test', '')
    i2 = IntOption('test', '')
    od1 = OptionDescription('od', '', [i])
    od1.impl_set_group_type(groups.family)
    od3 = OptionDescription('od2', '', [i2])
    od3.impl_set_group_type(groups.notfamily1)
    od2 = OptionDescription('od', '', [od1, od3])
    od4 = OptionDescription('od', '', [od2])
    async with await Config(od4) as cfg:
        assert len(list(await cfg.option('od').list('option'))) == 0
        assert len(list(await cfg.option('od').list('optiondescription'))) == 2
        assert len(list(await cfg.option('od').list('optiondescription', group_type=groups.family))) == 1
        assert len(list(await cfg.option('od').list('optiondescription', group_type=groups.notfamily1))) == 1
        assert len(list(await cfg.option('od.od').list('option'))) == 1
        assert len(list(await cfg.option('od.od2').list('option'))) == 1
        try:
            list(await cfg.option('od').list('unknown'))
        except AssertionError:
            pass
        else:
            raise Exception('must raise')
        try:
            list(await cfg.option('od').list('option', group_type='toto'))
        except AssertionError:
            pass
        else:
            raise Exception('must raise')
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_optiondescription_group():
    groups.notfamily = groups.GroupType('notfamily')
    i = IntOption('test', '')
    i2 = IntOption('test', '')
    od1 = OptionDescription('od', '', [i])
    od1.impl_set_group_type(groups.family)
    od3 = OptionDescription('od2', '', [i2])
    od3.impl_set_group_type(groups.notfamily)
    od2 = OptionDescription('od', '', [od1, od3])
    async with await Config(od2) as cfg:
        assert len(list(await cfg.option.list('option'))) == 0
        assert len(list(await cfg.option.list('optiondescription'))) == 2
        assert len(list(await cfg.option.list('optiondescription', group_type=groups.family))) == 1
        assert len(list(await cfg.option.list('optiondescription', group_type=groups.notfamily))) == 1
        try:
            list(await cfg.option.list('unknown'))
        except AssertionError:
            pass
        else:
            raise Exception('must raise')
        try:
            list(await cfg.option.list('option', group_type='toto'))
        except AssertionError:
            pass
        else:
            raise Exception('must raise')
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_optiondescription_group_redefined():
    try:
        groups.notfamily = groups.GroupType('notfamily')
    except:
        pass
    i = IntOption('test', '')
    od1 = OptionDescription('od', '', [i])
    od1.impl_set_group_type(groups.family)
    with pytest.raises(ValueError):
        od1.impl_set_group_type(groups.notfamily)
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_optiondescription_group_leadership():
    i = IntOption('test', '')
    od1 = OptionDescription('od', '', [i])
    with pytest.raises(ConfigError):
        od1.impl_set_group_type(groups.leadership)
    assert not await list_sessions()



@pytest.mark.asyncio
async def test_asign_optiondescription():
    i = IntOption('test', '')
    od1 = OptionDescription('od', '', [i])
    od2 = OptionDescription('od', '', [od1])
    async with await Config(od2) as cfg:
        with pytest.raises(APIError):
            await cfg.option('od').value.set('test')
        with pytest.raises(APIError):
            await cfg.option('od').value.reset()
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_intoption():
    i1 = IntOption('test1', 'description', min_number=3)
    i2 = IntOption('test2', 'description', max_number=3)
    od = OptionDescription('od', '', [i1, i2])
    async with await Config(od) as cfg:
        with pytest.raises(ValueError):
            await cfg.option('test1').value.set(2)
        await cfg.option('test1').value.set(3)
        await cfg.option('test1').value.set(4)
        await cfg.option('test2').value.set(2)
        await cfg.option('test2').value.set(3)
        with pytest.raises(ValueError):
            await cfg.option('test2').value.set(4)
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_get_display_type():
    i1 = IntOption('test1', 'description', min_number=3)
    assert i1.get_display_type() == _('integer')
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_option_not_in_config():
    i1 = IntOption('test1', 'description', min_number=3)
    with pytest.raises(AttributeError):
        i1.impl_getpath()
    assert not await list_sessions()
