from tiramisu import StrOption, OptionDescription, Leadership, Calculation, ParamValue, ParamOption, Params, calc_value


def get_description():
    """generate description for this test
    """
    option = StrOption('unicode', "Values 'test' must show 'Unicode follower 3'", multi=True)
    option1 = StrOption('unicode1', "Unicode follower 1", multi=True)
    option2 = StrOption('unicode2', "Unicode follower 2", multi=True)
    hidden_property = Calculation(calc_value,
                                  Params(ParamValue('hidden'),
                                         kwargs={'condition': ParamOption(option, todict=True),
                                                 'expected': ParamValue('test'),
                                                 'reverse_condition': ParamValue(True)}))
    option3 = StrOption('unicode3', "Unicode follower 3", properties=(hidden_property,), multi=True)
    descr1 = Leadership("unicode", "Common configuration 1",
                          [option, option1, option2, option3])
    descr = OptionDescription("options", "Common configuration 2", [descr1])
    descr = OptionDescription("unicode1_leadership_requires_value", "Leader followers with Unicode follower 3 hidden when Unicode follower 2 is test and modified value", [descr])
    return descr


async def get_values(api, allpath=False):
    if allpath:
        root = 'unicode1_leadership_requires_value.'
    else:
        root = ''
    await api.option(root + 'options.unicode.unicode').value.set([u'test', u'val2'])
    await api.option(root + 'options.unicode.unicode1', 0).value.set(u'super1')
    await api.option(root + 'options.unicode.unicode1', 1).value.set(u'super2')
    await api.option(root + 'options.unicode.unicode2', 0).value.set(u'pas test')
    await api.option(root + 'options.unicode.unicode2', 1).value.set(u'test')
    await api.option(root + 'options.unicode.unicode3', 1).value.set(u'super')
