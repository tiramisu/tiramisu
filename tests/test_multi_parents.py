from tiramisu import IntOption, OptionDescription, MetaConfig, list_sessions
from tiramisu.error import ConfigError
import pytest
from .config import delete_sessions, event_loop


async def make_metaconfig():
    i1 = IntOption('i1', '')
    i2 = IntOption('i2', '', default=1)
    i3 = IntOption('i3', '')
    i4 = IntOption('i4', '', default=2)
    i5 = IntOption('i5', '', default=[2], multi=True)
    i6 = IntOption('i6', '', properties=('disabled',))
    od1 = OptionDescription('od1', '', [i1, i2, i3, i4, i5, i6])
    od2 = OptionDescription('od2', '', [od1])
    return await MetaConfig([], optiondescription=od2, session_id='metacfg1', delete_old_session=True)


@pytest.mark.asyncio
async def test_multi_parents_path():
    """
    metacfg1 (1) ---
                   | -- cfg1
    metacfg2 (2) ---
    """
    metacfg1 = await make_metaconfig()
    cfg1 = await metacfg1.config.new(type='config', session_id="cfg1")
    metacfg2 = await MetaConfig([cfg1], session_id='metacfg2', delete_old_session=True)
    #
    assert await metacfg1.config.path() == 'metacfg1'
    assert await metacfg2.config.path() == 'metacfg2'
    assert await cfg1.config.path() == 'metacfg2.metacfg1.cfg1'
    await delete_sessions([metacfg1, metacfg2])


@pytest.mark.asyncio
async def test_multi_parents_path_same():
    """
               --- metacfg2 (1) ---
    metacfg1 --|                  | -- cfg1
               --- metacfg3 (2) ---
    """
    metacfg1 = await make_metaconfig()
    metacfg2 = await metacfg1.config.new(type='metaconfig', session_id="metacfg2")
    metacfg3 = await metacfg1.config.new(type='metaconfig', session_id="metacfg3")
    cfg1 = await metacfg2.config.new(type='config', session_id="cfg1")
    await metacfg3.config.add(cfg1)
    #
    assert await metacfg2.config.path() == 'metacfg1.metacfg2'
    assert await metacfg3.config.path() == 'metacfg1.metacfg3'
    assert await cfg1.config.path() == 'metacfg1.metacfg3.metacfg1.metacfg2.cfg1'
    await metacfg1.option('od1.i1').value.set(1)
    await metacfg3.option('od1.i1').value.set(2)
    assert await cfg1.option('od1.i1').value.get() == 1
    orideep = await cfg1.config.deepcopy(metaconfig_prefix="test_", session_id='test_cfg1')
    deep = orideep
    while True:
        try:
            children = list(await deep.config.list())
        except:
            break
        assert len(children) < 2
        deep = children[0]
    assert await deep.config.path() == 'test_metacfg3.test_metacfg1.test_metacfg2.test_cfg1'
    assert await cfg1.option('od1.i1').value.get() == 1
    await delete_sessions([metacfg1, orideep])



@pytest.mark.asyncio
async def test_multi_parents_value():
    metacfg1 = await make_metaconfig()
    cfg1 = await metacfg1.config.new(type='config', session_id="cfg1")
    metacfg2 = await MetaConfig([cfg1], session_id='metacfg2', delete_old_session=True)
    #
    assert await cfg1.option('od1.i1').value.get() == None
    assert await cfg1.option('od1.i2').value.get() == 1
    assert await cfg1.option('od1.i3').value.get() == None
    #
    assert await metacfg1.option('od1.i1').value.get() == None
    assert await metacfg1.option('od1.i2').value.get() == 1
    assert await metacfg1.option('od1.i3').value.get() == None
    #
    assert await metacfg2.option('od1.i1').value.get() == None
    assert await metacfg2.option('od1.i2').value.get() == 1
    assert await metacfg2.option('od1.i3').value.get() == None
    #
    await metacfg1.option('od1.i3').value.set(3)
    assert await metacfg1.option('od1.i3').value.get() == 3
    assert await cfg1.option('od1.i3').value.get() == 3
    assert await metacfg2.option('od1.i2').value.get() == 1
    #
    await metacfg2.option('od1.i2').value.set(4)
    assert await metacfg2.option('od1.i2').value.get() == 4
    assert await metacfg1.option('od1.i2').value.get() == 1
    assert await cfg1.option('od1.i2').value.get() == 4
    await delete_sessions([metacfg1, metacfg2])
