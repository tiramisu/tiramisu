# coding: utf-8
from .autopath import do_autopath
do_autopath()


import pytest
from tiramisu import IntOption, StrOption, OptionDescription, Config
from tiramisu.error import PropertiesOptionError, ConfigError
from tiramisu.storage import list_sessions, delete_session
from .config import config_type, get_config, event_loop


def make_description():
    u1 = IntOption('u1', '', properties=('frozen', 'mandatory', 'disabled', ))
    u2 = IntOption('u2', '', properties=('frozen', 'mandatory', 'disabled', ))
    return OptionDescription('od1', '', [u1, u2])


@pytest.mark.asyncio
async def test_permissive(config_type):
    descr = make_description()
    async with await Config(descr) as cfg_ori:
        await cfg_ori.property.read_write()
        await cfg_ori.property.read_write()
        cfg = await get_config(cfg_ori, config_type)
        props = frozenset()
        try:
            await cfg.option('u1').value.get()
        except PropertiesOptionError as err:
            props = err.proptype
        assert set(props) == {'disabled'}
        if config_type == 'tiramisu-api':
            await cfg.send()
        await cfg_ori.unrestraint.permissive.add('disabled')
        await cfg_ori.unrestraint.permissive.pop('hidden')
        assert await cfg_ori.unrestraint.permissive.get() == frozenset(['disabled'])
        cfg = await get_config(cfg_ori, config_type)
        props = frozenset()
        try:
            await cfg.option('u1').value.get()
        except PropertiesOptionError as err:
            props = err.proptype
        assert set(props) == {'disabled'}
        if config_type == 'tiramisu-api':
            await cfg.send()
        await cfg_ori.property.add('permissive')
        cfg = await get_config(cfg_ori, config_type)
        await cfg.option('u1').value.get()
        if config_type == 'tiramisu-api':
            await cfg.send()
        await cfg_ori.property.pop('permissive')
        cfg = await get_config(cfg_ori, config_type)
        props = frozenset()
        try:
            await cfg.option('u1').value.get()
        except PropertiesOptionError as err:
            props = err.proptype
        assert set(props) == {'disabled'}
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_permissive_add(config_type):
    descr = make_description()
    async with await Config(descr) as cfg_ori:
        await cfg_ori.property.read_write()
        await cfg_ori.property.read_write()
        cfg = await get_config(cfg_ori, config_type)
        props = frozenset()
        try:
            await cfg.option('u1').value.get()
        except PropertiesOptionError as err:
            props = err.proptype
        assert set(props) == {'disabled'}
        if config_type == 'tiramisu-api':
            await cfg.send()
        await cfg_ori.unrestraint.permissive.add('disabled')
        assert await cfg_ori.unrestraint.permissive.get() == frozenset(['hidden', 'disabled'])
        cfg = await get_config(cfg_ori, config_type)
        props = frozenset()
        try:
            await cfg.option('u1').value.get()
        except PropertiesOptionError as err:
            props = err.proptype
        assert set(props) == {'disabled'}
        if config_type == 'tiramisu-api':
            await cfg.send()
        await cfg_ori.property.add('permissive')
        cfg = await get_config(cfg_ori, config_type)
        await cfg.option('u1').value.get()
        if config_type == 'tiramisu-api':
            await cfg.send()
        await cfg_ori.property.pop('permissive')
        cfg = await get_config(cfg_ori, config_type)
        props = frozenset()
        try:
            await cfg.option('u1').value.get()
        except PropertiesOptionError as err:
            props = err.proptype
        assert set(props) == {'disabled'}
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_permissive_pop():
    descr = make_description()
    async with await Config(descr) as cfg:
        await cfg.property.read_write()
        await cfg.property.read_write()
        props = frozenset()
        try:
            await cfg.forcepermissive.option('u1').value.get()
        except PropertiesOptionError as err:
            props = err.proptype
        assert set(props) == {'disabled'}
        await cfg.unrestraint.permissive.add('disabled')
        assert await cfg.unrestraint.permissive.get() == frozenset(['hidden', 'disabled'])
        await cfg.forcepermissive.option('u1').value.get()
        await cfg.unrestraint.permissive.pop('disabled')
        props = frozenset()
        try:
            await cfg.forcepermissive.option('u1').value.get()
        except PropertiesOptionError as err:
            props = err.proptype
        assert set(props) == {'disabled'}
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_permissive_reset():
    descr = make_description()
    async with await Config(descr) as cfg:
        await cfg.property.read_write()
        assert await cfg.unrestraint.permissive.get() == frozenset(['hidden'])
        #
        await cfg.unrestraint.permissive.add('disabled')
        await cfg.unrestraint.permissive.pop('hidden')
        assert await cfg.unrestraint.permissive.get() == frozenset(['disabled'])
        #
        await cfg.unrestraint.permissive.reset()
        assert await cfg.unrestraint.permissive.get() == frozenset()
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_permissive_mandatory():
    descr = make_description()
    async with await Config(descr) as cfg:
        await cfg.property.read_only()
        props = frozenset()
        try:
            await cfg.option('u1').value.get()
        except PropertiesOptionError as err:
            props = err.proptype
        assert frozenset(props) == frozenset(['disabled'])
        await cfg.unrestraint.permissive.add('mandatory')
        await cfg.unrestraint.permissive.add('disabled')
        assert await cfg.unrestraint.permissive.get() == frozenset(['mandatory', 'disabled'])
        await cfg.property.add('permissive')
        await cfg.option('u1').value.get()
        await cfg.property.pop('permissive')
        try:
            await cfg.option('u1').value.get()
        except PropertiesOptionError as err:
            props = err.proptype
        assert frozenset(props) == frozenset(['disabled'])
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_permissive_frozen():
    descr = make_description()
    async with await Config(descr) as cfg:
        await cfg.property.read_write()
        await cfg.unrestraint.permissive.pop('hidden')
        await cfg.unrestraint.permissive.add('frozen')
        await cfg.unrestraint.permissive.add('disabled')
        assert await cfg.unrestraint.permissive.get() == frozenset(['frozen', 'disabled'])
        assert await cfg.permissive.get() == frozenset(['frozen', 'disabled'])
        try:
            await cfg.option('u1').value.set(1)
        except PropertiesOptionError as err:
            props = err.proptype
        assert frozenset(props) == frozenset(['disabled'])
        await cfg.property.add('permissive')
        await cfg.option('u1').value.set(1)
        assert await cfg.option('u1').value.get() == 1
        await cfg.property.pop('permissive')
        try:
            await cfg.option('u1').value.set(1)
        except PropertiesOptionError as err:
            props = err.proptype
        assert frozenset(props) == frozenset(['disabled'])
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_invalid_permissive():
    descr = make_description()
    async with await Config(descr) as cfg:
        await cfg.property.read_write()
        # FIXME with pytest.raises(TypeError):
        #    await cfg.unrestraint.permissive.set(['frozen', 'disabled'])")
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_forbidden_permissive():
    descr = make_description()
    async with await Config(descr) as cfg:
        await cfg.property.read_write()
        with pytest.raises(ConfigError):
            await cfg.permissive.add('force_default_on_freeze')
        with pytest.raises(ConfigError):
            await cfg.permissive.add('force_metaconfig_on_freeze')
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_permissive_option(config_type):
    descr = make_description()
    async with await Config(descr) as cfg_ori:
        await cfg_ori.property.read_write()
        cfg = await get_config(cfg_ori, config_type)

        props = frozenset()
        try:
            await cfg.option('u1').value.get()
        except PropertiesOptionError as err:
            props = err.proptype
        assert set(props) == {'disabled'}
        props = frozenset()
        try:
            await cfg.option('u2').value.get()
        except PropertiesOptionError as err:
            props = err.proptype
        assert set(props) == {'disabled'}

        if config_type == 'tiramisu-api':
            await cfg.send()
        await cfg_ori.unrestraint.option('u1').permissive.set(frozenset(['disabled']))
        cfg = await get_config(cfg_ori, config_type)
        props = frozenset()
        try:
            await cfg.option('u1').value.get()
        except PropertiesOptionError as err:
            props = err.proptype
        assert frozenset(props) == frozenset()
        props = frozenset()
        try:
            await cfg.option('u2').value.get()
        except PropertiesOptionError as err:
            props = err.proptype
        assert set(props) == {'disabled'}

        if config_type == 'tiramisu-api':
            await cfg.send()
        await cfg_ori.property.add('permissive')
        cfg = await get_config(cfg_ori, config_type)
        await cfg.option('u1').value.get()
        props = frozenset()
        try:
            await cfg.option('u2').value.get()
        except PropertiesOptionError as err:
            props = err.proptype
        assert set(props) == {'disabled'}

        if config_type == 'tiramisu-api':
            await cfg.send()
        await cfg_ori.property.pop('permissive')
        cfg = await get_config(cfg_ori, config_type)
        props = frozenset()
        try:
            await cfg.option('u1').value.get()
        except PropertiesOptionError as err:
            props = err.proptype
        assert frozenset(props) == frozenset()
        props = frozenset()
        try:
            await cfg.option('u2').value.get()
        except PropertiesOptionError as err:
            props = err.proptype
        assert set(props) == {'disabled'}
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_permissive_option_cache():
    descr = make_description()
    async with await Config(descr) as cfg:
        await cfg.property.read_write()

        props = frozenset()
        try:
            await cfg.option('u1').value.get()
        except PropertiesOptionError as err:
            props = err.proptype
        assert set(props) == {'disabled'}
        props = frozenset()
        try:
            await cfg.option('u2').value.get()
        except PropertiesOptionError as err:
            props = err.proptype
        assert set(props) == {'disabled'}

        await cfg.unrestraint.option('u1').permissive.set(frozenset(['disabled']))
        props = frozenset()
        try:
            await cfg.option('u1').value.get()
        except PropertiesOptionError as err:
            props = err.proptype
        assert frozenset(props) == frozenset()
        props = frozenset()
        try:
            await cfg.option('u2').value.get()
        except PropertiesOptionError as err:
            props = err.proptype
        assert set(props) == {'disabled'}

        await cfg.property.add('permissive')
        await cfg.option('u1').value.get()
        props = frozenset()
        try:
            await cfg.option('u2').value.get()
        except PropertiesOptionError as err:
            props = err.proptype
        assert set(props) == {'disabled'}

        await cfg.property.pop('permissive')
        props = frozenset()
        try:
            await cfg.option('u1').value.get()
        except PropertiesOptionError as err:
            props = err.proptype
        assert frozenset(props) == frozenset()
        props = frozenset()
        try:
            await cfg.option('u2').value.get()
        except PropertiesOptionError as err:
            props = err.proptype
        assert set(props) == {'disabled'}
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_permissive_option_mandatory():
    descr = make_description()
    async with await Config(descr) as cfg:
        await cfg.property.read_only()
        props = frozenset()
        try:
            await cfg.option('u1').value.get()
        except PropertiesOptionError as err:
            props = err.proptype
        assert frozenset(props) == frozenset(['disabled'])
        await cfg.unrestraint.option('u1').permissive.set(frozenset(['mandatory', 'disabled']))
        assert await cfg.unrestraint.option('u1').permissive.get() == frozenset(['mandatory', 'disabled'])
        await cfg.property.add('permissive')
        await cfg.option('u1').value.get()
        await cfg.property.pop('permissive')
        try:
            await cfg.option('u1').value.get()
        except PropertiesOptionError as err:
            props = err.proptype
        assert frozenset(props) == frozenset(['disabled'])
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_permissive_option_frozen():
    descr = make_description()
    async with await Config(descr) as cfg:
        await cfg.property.read_write()
        await cfg.unrestraint.option('u1').permissive.set(frozenset(['frozen', 'disabled']))
        await cfg.option('u1').value.set(1)
        assert await cfg.option('u1').value.get() == 1
        await cfg.property.add('permissive')
        assert await cfg.option('u1').value.get() == 1
        await cfg.property.pop('permissive')
        assert await cfg.option('u1').value.get() == 1
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_invalid_option_permissive():
    descr = make_description()
    async with await Config(descr) as cfg:
        await cfg.property.read_write()
        with pytest.raises(TypeError):
            await cfg.unrestraint.option('u1').permissive.set(['frozen', 'disabled'])
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_remove_option_permissive(config_type):
    var1 = StrOption('var1', '', u'value', properties=('hidden',))
    od1 = OptionDescription('od1', '', [var1])
    descr = OptionDescription('rootod', '', [od1])
    async with await Config(descr) as cfg_ori:
        await cfg_ori.property.read_write()
        cfg = await get_config(cfg_ori, config_type)
        with pytest.raises(PropertiesOptionError):
            await cfg.option('od1.var1').value.get()
        if config_type == 'tiramisu-api':
            await cfg.send()
        await cfg_ori.forcepermissive.option('od1.var1').permissive.set(frozenset(['hidden']))
        assert await cfg_ori.forcepermissive.option('od1.var1').permissive.get() == frozenset(['hidden'])
        cfg = await get_config(cfg_ori, config_type)
        assert await cfg.option('od1.var1').value.get() == 'value'
        if config_type == 'tiramisu-api':
            await cfg.send()
        await cfg_ori.forcepermissive.option('od1.var1').permissive.set(frozenset())
        assert await cfg_ori.forcepermissive.option('od1.var1').permissive.get() == frozenset()
        cfg = await get_config(cfg_ori, config_type)
        with pytest.raises(PropertiesOptionError):
            await cfg.option('od1.var1').value.get()
    assert not await list_sessions()


@pytest.mark.asyncio
async def test_reset_option_permissive(config_type):
    var1 = StrOption('var1', '', u'value', properties=('hidden',))
    od1 = OptionDescription('od1', '', [var1])
    descr = OptionDescription('rootod', '', [od1])
    async with await Config(descr) as cfg_ori:
        await cfg_ori.property.read_write()
        cfg = await get_config(cfg_ori, config_type)
        with pytest.raises(PropertiesOptionError):
            await cfg.option('od1.var1').value.get()
        if config_type == 'tiramisu-api':
            await cfg.send()
        await cfg_ori.forcepermissive.option('od1.var1').permissive.set(frozenset(['hidden']))
        assert await cfg_ori.forcepermissive.option('od1.var1').permissive.get() == frozenset(['hidden'])
        cfg = await get_config(cfg_ori, config_type)
        assert await cfg.option('od1.var1').value.get() == 'value'
        if config_type == 'tiramisu-api':
            await cfg.send()
        await cfg_ori.forcepermissive.option('od1.var1').permissive.reset()
        assert await cfg_ori.forcepermissive.option('od1.var1').permissive.get() == frozenset()
        cfg = await get_config(cfg_ori, config_type)
        with pytest.raises(PropertiesOptionError):
            await cfg.option('od1.var1').value.get()
    assert not await list_sessions()
