![Logo Tiramisu](../logo.png "logo Tiramisu")

# Python3 Tiramisu library user documentation

## The tasting of `Tiramisu` --- `user documentation`

Tiramisu:

- is a cool, refreshing Italian dessert,
- it is also an [options controller tool](http://en.wikipedia.org/wiki/Configuration_management#Overview)

It's a pretty small, local (that is, straight on the operating system) options handler and controller.

- [Getting started](gettingstarted.md)
- [The Config](config.md)
- [Browse the Config](browse.md)
- [Manage values](api_value.md)

.. toctree::
    :maxdepth: 2

    api_property
    storage
    application
    quiz
    glossary

External project:

.. toctree::
    :maxdepth: 2

    cmdline_parser

.. FIXME ca veut rien dire : "AssertionError: type <class 'tiramisu.autolib.Calculation'> invalide pour des propriétés pour protocols, doit être un frozenset"


.. FIXME changer le display_name !
.. FIXME voir si warnings_only dans validator !
.. FIXME submulti dans les leadership
.. FIXME exemple avec default_multi (et undefined)
.. FIXME config, metaconfig, ...
.. FIXME fonction de base
.. FIXME information
.. FIXME demoting_error_warning, warnings, ...
.. FIXME class _TiramisuOptionOptionDescription(CommonTiramisuOption):
.. FIXME class _TiramisuOptionOption(_TiramisuOptionOptionDescription):
.. FIXME class TiramisuOptionInformation(CommonTiramisuOption):
.. FIXME class TiramisuContextInformation(TiramisuConfig):
.. FIXME expire
.. FIXME custom display_name
.. FIXME     assert await cfg.cache.get_expiration_time() == 5
.. FIXME    await cfg.cache.set_expiration_time(1)
.. FIXME convert_suffix_to_path



Indices and full bunch of code
===============================


* `All files for which code is available <_modules/index.html>`_
* :ref:`genindex`
* :ref:`search`
