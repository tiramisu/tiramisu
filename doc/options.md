# Default Options

Basic options

## Textual option: StrOption

Option that accept any textual data in Tiramisu:

```python
from tiramisu import StrOption
StrOption('str', 'str', 'value')
StrOption('str', 'str', '1')
```

Other type generate an error:

```python
try:
    StrOption('str', 'str', 1)
except ValueError as err:
    print(err)
```

returns:

```
"1" is an invalid string for "str"
```


## Integers option: IntOption

Option that accept any integers number in Tiramisu:

```python
from tiramisu import IntOption
IntOption('int', 'int', 1)
```

### min_number and max_number

This option can also verify minimal and maximal number:

```python
IntOption('int', 'int', 10, min_number=10, max_number=15)
```

```python
try:
    IntOption('int', 'int', 16, max_number=15)
except ValueError as err:
    print(err)
```

returns:

```
"16" is an invalid integer for "int", value must be less than "15"
```

> **_NOTE:_** If "warnings_only" parameter it set to True, it will only emit a warning.

## Floating point number option: FloatOption

Option that accept any floating point number in Tiramisu:

```python
from tiramisu import FloatOption
FloatOption('float', 'float', 10.1)
```

## Boolean option: BoolOption

Boolean values are the two constant objects False and True:

```python
from tiramisu import BoolOption 
BoolOption('bool', 'bool', True)
BoolOption('bool', 'bool', False)
```

## Choice option: ChoiceOption

Option that only accepts a list of possible choices.

For example, we just want allowed 1 or 'see later':

```python
from tiramisu import ChoiceOption
ChoiceOption('choice', 'choice', (1, 'see later'), 1)
ChoiceOption('choice', 'choice', (1, 'see later'), 'see later')
```

Any other value isn't allowed:

```python
try:
    ChoiceOption('choice', 'choice', (1, 'see later'), "i don't know")
except ValueError as err:
    print(err)
```

returns:

```
"i don't know" is an invalid choice for "choice", only "1" and "see later" are allowed
```

# Network options

## IPv4 address option: IPOption

An Internet Protocol address (IP address) is a numerical label assigned to each device connected to a computer network that uses the Internet Protocol for communication.

This option only support version 4 of the Internet Protocol.

```python
from tiramisu import IPOption
IPOption('ip', 'ip', '192.168.0.24')
```

### private_only

By default IP could be a private or a public address. It's possible to restrict to only private IPv4 address with "private_only" attributs:

```python
try:
    IPOption('ip', 'ip', '1.1.1.1', private_only=True)
except ValueError as err:
    print(err)
```

returns:

```
"1.1.1.1" is an invalid IP for "ip", must be private IP
```

> **_NOTE:_** If "warnings_only" parameter it set to True, it will only emit a warning.

### allow_reserved

By default, IETF reserved are not allowed. The "allow_reserved" can be used to allow this address:

```python
try:
    IPOption('ip', 'ip', '255.255.255.255')
except ValueError as err:
    print(err)
```

returns:

```
"255.255.255.255" is an invalid IP for "ip", mustn't be reserved IP
```

But this doesn't raises:

```python
IPOption('ip', 'ip', '255.255.255.255', allow_reserved=True)
```

> **_NOTE:_** If "warnings_only" parameter it set to True, it will only emit a warning.

### cidr

Classless Inter-Domain Routing (CIDR) is a method for allocating IP addresses and IP routing.

CIDR notation, in which an address or routing prefix is written with a suffix indicating the number of bits of the prefix, such as 192.168.0.1/24.

```python
IPOption('ip', 'ip', '192.168.0.1/24', cidr=True)
try:
    IPOption('ip', 'ip', '192.168.0.0/24', cidr=True)
except ValueError as err:
    print(err)
```

returns:

```
"192.168.0.0/24" is an invalid IP for "ip", it's in fact a network address
```

## Port option: PortOption

A port is a network communication endpoint. 

A port is a string object:

```python
from tiramisu import PortOption
PortOption('port', 'port', '80')
```

### allow_range

A range is a list of port where we specified first port and last port number. The separator is ":":

```python
PortOption('port', 'port', '2000', allow_range=True)
PortOption('port', 'port', '2000:3000', allow_range=True)
```

### allow_zero

By default, port 0 is not allowed, if you want allow it, use the parameter "allow_zero":

```python
try:
    PortOption('port', 'port', '0')
except ValueError as err:
    print(err)
```

returns:

```python
"0" is an invalid port for "port", must be between 1 and 49151
```

But this doesn't raises:

```python
PortOption('port', 'port', '0', allow_zero=True)
```

> **_NOTE:_** This option affect the minimal and maximal port number, if "warnings_only" parameter it set to True, it will only emit a warning.

### allow_wellknown

The well-known ports (also known as system ports) are those from 1 through 1023. This parameter is set to True by default:

```python
PortOption('port', 'port', '80')
```

```python
try:
    PortOption('port', 'port', '80', allow_wellknown=False)
except ValueError as err:
    print(err)
```

returns:

```
"80" is an invalid port for "port", must be between 1024 and 49151
```

> **_NOTE:_** This option affect the minimal and maximal port number, if "warnings_only" parameter it set to True, it will only emit a warning.

### allow_registred

The registered ports are those from 1024 through 49151. This parameter is set to True by default:

```python
PortOption('port', 'port', '1300')
```

```python
try:
    PortOption('port', 'port', '1300', allow_registred=False)
except ValueError as err:
    print(err)
```

returns:

```
"1300" is an invalid port for "port", must be between 1 and 1023
```

> **_NOTE:_** This option affect the minimal and maximal port number, if "warnings_only" parameter it set to True, it will only emit a warning.

### allow_protocol

```python
PortOption('port', 'port', '1300', allow_protocol="True")
PortOption('port', 'port', 'tcp:1300', allow_protocol="True")
PortOption('port', 'port', 'udp:1300', allow_protocol="True")
```

### allow_private

The dynamic or private ports are those from 49152 through 65535. One common use for this range is for ephemeral ports. This parameter is set to False by default:

```python
try:
    PortOption('port', 'port', '64000')
except ValueError as err:
    print(err)
```

returns:

```
"64000" is an invalid port for "port", must be between 1 and 49151
```

```python
PortOption('port', 'port', '64000', allow_private=True)
```

> **_NOTE:_** This option affect the minimal and maximal port number, if "warnings_only" parameter it set to True, it will only emit a warning.

## Subnetwork option: NetworkOption

IP networks may be divided into subnetworks:

```python
from tiramisu import NetworkOption
NetworkOption('net', 'net', '192.168.0.0')
```

### cidr

Classless Inter-Domain Routing (CIDR) is a method for allocating IP addresses and IP routing.

CIDR notation, in which an address or routing prefix is written with a suffix indicating the number of bits of the prefix, such as 192.168.0.0/24:

```python
NetworkOption('net', 'net', '192.168.0.0/24', cidr=True)
```

## Subnetwork mask option: NetmaskOption

For IPv4, a network may also be characterized by its subnet mask or netmask. This option allow you to enter a netmask:

```python
from tiramisu import NetmaskOption
NetmaskOption('mask', 'mask', '255.255.255.0')
```

## Broadcast option: BroadcastOption

The last address within a network broadcast transmission to all hosts on the link. This option allow you to enter a broadcast:

```python
from tiramisu import BroadcastOption
BroadcastOption('bcast', 'bcast', '192.168.0.254')
```

# Internet options

## Domain name option: DomainnameOption

Domain names are used in various networking contexts and for application-specific naming and addressing purposes.
The DomainnameOption allow different type of domain name:

```python
from tiramisu import DomainnameOption
DomainnameOption('domain', 'domain', 'foo.example.net')
DomainnameOption('domain', 'domain', 'foo', type='hostname')
```

### type

There is three type for a domain name:

- "domainname" (default): lowercase, number, "-" and "." characters are allowed, this must have at least one "."
- "hostname": lowercase, number and "-" characters are allowed, the maximum length is 63 characters
- "netbios": lowercase, number and "-" characters are allowed, the maximum length is 15 characters

> **_NOTE:_** If "warnings_only" parameter it set to True, it will raise if length is incorrect by only emit a warning character is not correct.

### allow_ip

If the option can contain a domain name or an IP, you can set the "allow_ip" to True, (False by default):

```python
DomainnameOption('domain', 'domain', 'foo.example.net', allow_ip=True)
DomainnameOption('domain', 'domain', '192.168.0.1', allow_ip=True)
```

In this case, IP is validate has IPOption would do.

### allow_cidr_network

If the option can contain a CIDR network, you can set "allow_cidr_network" to True, (False by default):

```python
DomainnameOption('domain', 'domain', 'foo.example.net', allow_cidr_network=True)
DomainnameOption('domain', 'domain', '192.168.0.0/24', allow_cidr_network=True)
```

### allow_without_dot

A domain name with domainname's type must have a dot. If "allow_without_dot" is True (False by default), we can set a domainname or an hostname:

```python
DomainnameOption('domain', 'domain', 'foo.example.net', allow_without_dot=True)
DomainnameOption('domain', 'domain', 'foo', allow_without_dot=True)
```

### allow_startswith_dot

A domain name with domainname's type mustn't start by a dot. .example.net is not a valid domain. In some case it could be interesting to allow domain name starts by a dot (for ACL in Squid, no proxy option in Firefox, ...):

```python
DomainnameOption('domain', 'domain', 'example.net', allow_startswith_dot=True)
DomainnameOption('domain', 'domain', '.example.net', allow_startswith_dot=True)
```

## MAC address option: MACOption

Validate MAC address:

```python
from tiramisu import MACOption
MACOption('mac', 'mac', '01:10:20:20:10:30')
```

## Uniform Resource Locator option: UrlOption

An Uniform Resource Locator is, in fact, a string starting with http:// or https://, a DomainnameOption, optionaly ':' and a PortOption, and finally filename:

```python
from tiramisu import URLOption
URLOption('url', 'url', 'http://foo.example.fr/index.php')
URLOption('url', 'url', 'https://foo.example.fr:4200/index.php?login=foo&pass=bar')
```

For parameters, see PortOption and DomainnameOption.

## Email address option: EmailOption

Electronic mail (email or e-mail) is a method of exchanging messages ("mail") between people using electronic devices. Here an example:

```python
from tiramisu import EmailOption
EmailOption('mail', 'mail', 'foo@example.net')
```

# Unix options

## Unix username: UsernameOption

An unix username option is a 32 characters maximum length with lowercase ASCII characters, number, '_' or '-'. The username have to start with lowercase ASCII characters or "_":

```python
from tiramisu import UsernameOption
UsernameOption('user', 'user', 'my_user')
```

## Unix groupname: GroupnameOption

Same condition for unix group name:

```python
from tiramisu import GroupnameOption
GroupnameOption('group', 'group', 'my_group')
```

## Password option: PasswordOption

Simple string with no other restriction:

```python
from tiramisu import PasswordOption
PasswordOption('pass', 'pass', 'oP$¨1jiJie')
```

## Unix filename option: FilenameOption

For this option, only lowercase and uppercas ASCII character, "-", ".", "_", "~", and "/" are allowed:

```python
from tiramisu import FilenameOption
FilenameOption('file', 'file', '/etc/tiramisu/tiramisu.conf')
```

## Unix file permissions: PermissionsOption

Valid the representing Unix permissions is an octal (base-8) notation.

```python
from tiramisu import PermissionsOption
PermissionsOption('perms', 'perms', 755)
PermissionsOption('perms', 'perms', 1755)
```

This option doesn't allow (or display a warning with warnings_only):

- 777 (two weak value)
- others have more right than group
- group has more right than user

# Date option

## Date option: DateOption

Date option waits for a date with format YYYY-MM-DD:

```python
from tiramisu import DateOption
DateOption('date', 'date', '2019-10-30')
```


