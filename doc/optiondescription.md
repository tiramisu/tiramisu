# Generic container: OptionDescription

## Option description's description

First of all, an option description is a name, a description and children.

### name

The "name" is important to retrieve this option description.

### description

The "description" allows the user to understand where this option description will contains.

### children

List of children option.

> **_NOTE:_** the option can be an option or an other option description

Let's try:

```python
from tiramisu import StrOption, OptionDescription
child1 = StrOption('first', 'First basic option')
child2 = StrOption('second', 'Second basic option')
OptionDescription('basic',
                  'Basic options',
                  [child1, child2])
```

## Option description's properties

See [property](property.md).

