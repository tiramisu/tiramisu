# The Config

Tiramisu is made of almost three main classes/concepts :

- the "Option" stands for the option types
- the "OptionDescription" is the schema, the option's structure
- the "Config" which is the whole configuration entry point

![The Config](config.png "The Config")

## The handling of options

The handling of options is split into two parts: the description of
which options are available, what their possible values and defaults are
and how they are organized into a tree. A specific choice of options is
bundled into a configuration object which has a reference to its option
description (and therefore makes sure that the configuration values
adhere to the option description).

- [Instanciate an option](option.md)
- [Default Options](options.md)
- [The symbolic link option: SymLinkOption](symlinkoption.md)
- [Create it's own option](own_option.md)

## Option description are nested Options

The Option (in this case the "BoolOption"),
are organized into a tree into nested "OptionDescription" objects.

Every option has a name, as does every option group.

- [Generic container: OptionDescription](optiondescription.md)
- [Dynamic option description: DynOptionDescription](dynoptiondescription.md)
- [Leadership OptionDescription: Leadership](leadership.md)

## Config

Getting started with the tiramisu library (it loads and prints properly).

Let's perform a *Getting started* code review :

```python
from asyncio import run
from tiramisu import Config
from tiramisu import OptionDescription, BoolOption

# let's create a group of options named "optgroup"
descr = OptionDescription("optgroup", "", [
                          # ... with only one option inside
                          BoolOption("bool", "", default=False)
                          ])

async def main():
    # Then, we make a Config with the OptionDescription` we
    cfg = await Config(descr)
    # the global help about the config
    cfg.help()


run(main())
```

returns:

```
Root config object that enables us to handle the configuration options

Settings:
    forcepermissive      Access to option without verifying permissive properties
    unrestraint          Access to option without property restriction

Commands:
    cache                Manage config cache
    config               Actions to Config
    information          Manage config informations
    option               Select an option
    owner                Global owner
    permissive           Manage config permissives
    property             Manage config properties
    session              Manage Config session
    value                Manage config value
```

Then let's print our "Option details.

```python
cfg.option.help()
```

returns:

```
Select an option

Call: Select an option by path

Commands:
    dict         Convert config and option to tiramisu format
    find         Find an or a list of options
    list         List options (by default list only option)
    updates      Updates value with tiramisu format
```

## Go futher with "Option" and "Config"

- [property](property.md)
- [validator](validator.md)
