# Instanciate an option

## Option's description

First of all, an option is a name and a description.

### name

The "name" is important to retrieve this option.

### description

The "description" allows the user to understand where this option will be used for.

Let's try:

```python
from tiramisu import StrOption
StrOption('welcome',
          'Welcome message to the user login')
```

## Option's default value

For each option, we can defined a default value. This value will be the value of this option until user customize it.

This default value is store directly in the option. So we can, at any moment we can go back to the default value.

```python
StrOption('welcome',
          'Welcome message to the user login',
	  'Hey guys, welcome here!')
```

The default value can be a calculation.

```python
from tiramisu import Calculation
def get_value():
    return 'Hey guys, welcome here'

StrOption('welcome',
          'Welcome message to the user login',
	  Calculation(get_value))
```

## Option with multiple value

### multi

There are cases where it can be interesting to have a list of values rather than just one.

The "multi" attribut is here for that.

In this case, the default value has to be a list:

```python
StrOption('shopping_list',
          'The shopping list',
	  ['1 kilogram of carrots', 'leeks', '1 kilogram of potatos'],
	  multi=True)
```

The option could be a list of list, which is could submulti:

```python
from tiramisu import submulti
StrOption('shopping_list',
          'The shopping list',
	  [['1 kilogram of carrots', 'leeks', '1 kilogram of potatos'], ['milk', 'eggs']],
	  multi=submulti)
```

The default value can be a calculation. For a multi, the function have to return a list or have to be in a list:

```python
def get_values():
    return ['1 kilogram of carrots', 'leeks', '1 kilogram of potatos']

StrOption('shopping_list',
          'The shopping list',
	  Calculation(get_values),
	  multi=True)
```

```python
def get_a_value():
    return 'leeks'

StrOption('shopping_list',
          'The shopping list',
	  ['1 kilogram of carrots', Calculation(get_a_value), '1 kilogram of potatos'],
	  multi=True)
```

### default_multi

A second default value is available for multi option, "default_multi". This value is used when we add new value without specified a value.
This "default_multi" must not be a list in multi purpose. For submulti, it has to be a list:

```python
StrOption('shopping_list',
          'The shopping list',
          ['1 kilogram of carrots', 'leeks', '1 kilogram of potatos'],
          default_multi='some vegetables',
          multi=True)
StrOption('shopping_list',
          'The shopping list',
          [['1 kilogram of carrots', 'leeks', '1 kilogram of potatos'], ['milk', 'eggs']],
          default_multi=['some', 'vegetables'],
          multi=submulti)
```

The default_multi value can be a calculation:

```python
def get_a_value():
    return 'some vegetables'

StrOption('shopping_list',
          'The shopping list',
          ['1 kilogram of carrots', 'leeks', '1 kilogram of potatos'],
          default_multi=Calculation(get_a_value),
          multi=True)
```

## Other option's parameters 

There are two other parameters.

We will see them later:

- [Property](property.md)
- [Validator](validator.md)
