# Dynamic option description: DynOptionDescription

## Dynamic option description's description

Dynamic option description is an OptionDescription which multiplies according to the return of a function.

First of all, an option description is a name, a description, children and suffixes.

### name

The "name" is important to retrieve this dynamic option description.

### description

The "description" allows the user to understand where this dynamic option description will contains.

### children

List of children option. 

> **_NOTE:_** the option has to be multi option or leadership but not option description.

### suffixes

Suffixes is a [calculation](calculation.md) that return the list of suffixes used to create dynamic option description.

Let's try:

```python
from tiramisu import StrOption, DynOptionDescription, Calculation
def return_suffixes():
   return ['1', '2']

child1 = StrOption('first', 'First basic option ')
child2 = StrOption('second', 'Second basic option ')
DynOptionDescription('basic ',
                     'Basic options ',
                     [child1, child2],
                     Calculation(return_suffixes))
```

This example will construct:

- Basic options 1:

  - First basic option 1
  - Second basic option 1

- Basic options 2:

  - First basic option 2
  - Second basic option 2

## Dynamic option description's properties

See [property](property.md).

