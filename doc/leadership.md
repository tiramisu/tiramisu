# Leadership OptionDescription: Leadership

A leadership is a special "OptionDescription" that wait a leader and one or multiple followers.

Leader and follower are multi option. The difference is that the length is defined by the length of the option leader.

If the length of leader is 3, all followers have also length 3.

An other different is that the follower is isolate. That means that you can only change on value on a specified index in the list of it's values.
If a value is mark as modified in a specified index, that not affect the other values in other index.

## The leadership's description

A leadership is an "OptionDescription"

First of all, an option leadership is a name, a description and children.

### name

The "name" is important to retrieve this dynamic option description.

### description

The "description" allows the user to understand where this dynamic option description will contains.

### children

List of children option.

> **_NOTE:_** the option has to be multi or submulti option and not other option description.

Let's try:

```python
from tiramisu import StrOption, Leadership
users = StrOption('users', 'User', multi=True)
passwords = StrOption('passwords', 'Password', multi=True)
Leadership('users',
           'User allow to connect',
           [users, passwords])
```

## The leadership's properties

See [property](property.md).
