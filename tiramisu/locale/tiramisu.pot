# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR ORGANIZATION
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2019-08-28 15:53+CEST\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING\n"
"Generated-By: pygettext.py 1.5\n"


#: tiramisu/api.py:59
msgid "Settings:"
msgstr ""

#: tiramisu/api.py:60
msgid "Access to option without verifying permissive properties"
msgstr ""

#: tiramisu/api.py:61
msgid "Access to option without property restriction"
msgstr ""

#: tiramisu/api.py:65
msgid "Call: {}"
msgstr ""

#: tiramisu/api.py:67
msgid "Commands:"
msgstr ""

#: tiramisu/api.py:104
msgid "index \"{}\" is greater than the leadership length \"{}\" for option \"{}\""
msgstr ""

#: tiramisu/api.py:109
msgid "option must not be an optiondescription"
msgstr ""

#: tiramisu/api.py:137
msgid "index must be set with the follower option \"{}\""
msgstr ""

#: tiramisu/api.py:140
msgid "unknown method {} in {}"
msgstr ""

#: tiramisu/api.py:398
msgid "cannot add this property: \"{0}\""
msgstr ""

#: tiramisu/api.py:554 tiramisu/config.py:239
msgid "can't delete a SymLinkOption"
msgstr ""

#: tiramisu/api.py:714 tiramisu/api.py:1515
msgid "please specify a valid sub function ({})"
msgstr ""

#: tiramisu/api.py:734
msgid "unknown config type {}"
msgstr ""

#: tiramisu/api.py:795 tiramisu/api.py:1271
msgid "unknown list type {}"
msgstr ""

#: tiramisu/api.py:797 tiramisu/api.py:1273
msgid "unknown group_type: {0}"
msgstr ""

#: tiramisu/api.py:1096
msgid "properties must be a frozenset"
msgstr ""

#: tiramisu/api.py:1102 tiramisu/api.py:1124
msgid "unknown when {} (must be in append or remove)"
msgstr ""

#: tiramisu/api.py:1114 tiramisu/api.py:1136 tiramisu/config.py:1269
msgid "unknown type {}"
msgstr ""

#: tiramisu/api.py:1440
msgid "cannot set session_id and config together"
msgstr ""

#: tiramisu/autolib.py:96
msgid "unable to carry out a calculation for \"{}\", {}"
msgstr ""

#: tiramisu/autolib.py:257
msgid "the \"{}\" function with positional arguments \"{}\" and keyword arguments \"{}\" must not return a list (\"{}\") for the follower option \"{}\""
msgstr ""

#: tiramisu/autolib.py:266
msgid "the \"{}\" function must not return a list (\"{}\") for the follower option \"{}\""
msgstr ""

#: tiramisu/autolib.py:295
msgid "unexpected error \"{0}\" in function \"{1}\" with arguments \"{3}\" and \"{4}\" for option \"{2}\""
msgstr ""

#: tiramisu/autolib.py:302
msgid "unexpected error \"{0}\" in function \"{1}\" for option \"{2}\""
msgstr ""

#: tiramisu/config.py:69
msgid "\"{0}\" must be an optiondescription, not an {1}"
msgstr ""

#: tiramisu/config.py:200
msgid "unknown option {}"
msgstr ""

#: tiramisu/config.py:208
msgid "there is no option description for this config (may be GroupConfig)"
msgstr ""

#: tiramisu/config.py:224
msgid "can't assign to a SymLinkOption"
msgstr ""

#: tiramisu/config.py:228
msgid "cannot reduce length of the leader \"{}\""
msgstr ""

#: tiramisu/config.py:308
msgid "the follower option \"{}\" has greater length ({}) than the leader length ({})"
msgstr ""

#: tiramisu/config.py:405
msgid "no option found in config with these criteria"
msgstr ""

#: tiramisu/config.py:455
msgid "make_dict can't filtering with value without option"
msgstr ""

#: tiramisu/config.py:508
msgid "unexpected path \"{0}\", should start with \"{1}\""
msgstr ""

#: tiramisu/config.py:660
msgid "cannot duplicate {}"
msgstr ""

#: tiramisu/config.py:716
msgid "parent of {} not already exists"
msgstr ""

#: tiramisu/config.py:755
msgid "cannot set leadership object has root optiondescription"
msgstr ""

#: tiramisu/config.py:757
msgid "cannot set dynoptiondescription object has root optiondescription"
msgstr ""

#: tiramisu/config.py:774 tiramisu/config.py:819
msgid "invalid session ID: {0} for config"
msgstr ""

#: tiramisu/config.py:803
msgid "groupconfig's children must be a list"
msgstr ""

#: tiramisu/config.py:807
msgid "groupconfig's children must be Config, MetaConfig or GroupConfig"
msgstr ""

#: tiramisu/config.py:814
msgid "config name must be uniq in groupconfig for \"{0}\""
msgstr ""

#: tiramisu/config.py:984
msgid "unknown config \"{}\""
msgstr ""

#: tiramisu/config.py:1005
msgid "child must be a Config, MixConfig or MetaConfig"
msgstr ""

#: tiramisu/config.py:1037
msgid "force_default, force_default_if_same or force_dont_change_value cannot be set with only_config"
msgstr ""

#: tiramisu/config.py:1059
msgid "force_default and force_dont_change_value cannot be set together"
msgstr ""

#: tiramisu/config.py:1190 tiramisu/config.py:1267
msgid "config name must be uniq in groupconfig for {0}"
msgstr ""

#: tiramisu/config.py:1207 tiramisu/config.py:1216
msgid "cannot find the config {}"
msgstr ""

#: tiramisu/config.py:1238
msgid "MetaConfig with optiondescription must have string has child, not {}"
msgstr ""

#: tiramisu/config.py:1250
msgid "child must be a Config or MetaConfig"
msgstr ""

#: tiramisu/config.py:1254
msgid "all config in metaconfig must have the same optiondescription"
msgstr ""

#: tiramisu/config.py:1305
msgid "metaconfig must have the same optiondescription"
msgstr ""

#: tiramisu/error.py:24
msgid "and"
msgstr ""

#: tiramisu/error.py:26
msgid "or"
msgstr ""

#: tiramisu/error.py:50
msgid " {} "
msgstr ""

#: tiramisu/error.py:105 tiramisu/setting.py:601
msgid "property"
msgstr ""

#: tiramisu/error.py:107 tiramisu/setting.py:603
msgid "properties"
msgstr ""

#: tiramisu/error.py:109
msgid "cannot access to {0} \"{1}\" because \"{2}\" has {3} {4}"
msgstr ""

#: tiramisu/error.py:116
msgid "cannot access to {0} \"{1}\" because has {2} {3}"
msgstr ""

#: tiramisu/error.py:192
msgid "invalid value"
msgstr ""

#: tiramisu/error.py:197
msgid "attention, \"{0}\" could be an invalid {1} for \"{2}\""
msgstr ""

#: tiramisu/error.py:201 tiramisu/error.py:205
msgid "\"{0}\" is an invalid {1} for \"{2}\""
msgstr ""

#: tiramisu/function.py:34
msgid "args in params must be a tuple"
msgstr ""

#: tiramisu/function.py:37 tiramisu/function.py:42
msgid "arg in params must be a Param"
msgstr ""

#: tiramisu/function.py:39
msgid "kwargs in params must be a dict"
msgstr ""

#: tiramisu/function.py:58
msgid "paramoption needs an option not {}"
msgstr ""

#: tiramisu/function.py:64
msgid "param must have a boolean not a {} for notraisepropertyerror"
msgstr ""

#: tiramisu/function.py:271
msgid "unexpected {} condition_operator in calc_value"
msgstr ""

#: tiramisu/option/baseoption.py:75 tiramisu/option/symlinkoption.py:33
msgid "\"{0}\" is an invalid name for an option"
msgstr ""

#: tiramisu/option/baseoption.py:93
msgid "invalid properties type {0} for {1}, must be a frozenset"
msgstr ""

#: tiramisu/option/baseoption.py:115
msgid "conflict: properties already set in requirement {0} for {1}"
msgstr ""

#: tiramisu/option/baseoption.py:162
msgid "{0} must be a function"
msgstr ""

#: tiramisu/option/baseoption.py:164
msgid "{0}_params must be a params"
msgstr ""

#: tiramisu/option/baseoption.py:205
msgid "cannot find those arguments \"{}\" in function \"{}\" for \"{}\""
msgstr ""

#: tiramisu/option/baseoption.py:224
msgid "missing those arguments \"{}\" in function \"{}\" for \"{}\""
msgstr ""

#: tiramisu/option/baseoption.py:258
msgid "params defined for a callback function but no callback defined yet for option \"{0}\""
msgstr ""

#: tiramisu/option/baseoption.py:350 tiramisu/storage/dictionary/value.py:256
#: tiramisu/storage/sqlite3/value.py:201
msgid "information's item not found: {0}"
msgstr ""

#: tiramisu/option/baseoption.py:363
msgid "'{0}' ({1}) object attribute '{2}' is read-only"
msgstr ""

#: tiramisu/option/baseoption.py:394
msgid "\"{}\" ({}) object attribute \"{}\" is read-only"
msgstr ""

#: tiramisu/option/baseoption.py:404
msgid "\"{}\" not part of any Config"
msgstr ""

#: tiramisu/option/baseoption.py:459
msgid "malformed requirements must be an option in option {0}"
msgstr ""

#: tiramisu/option/baseoption.py:462
msgid "malformed requirements multi option must not set as requires of non multi option {0}"
msgstr ""

#: tiramisu/option/baseoption.py:501
msgid "malformed requirements expected must have option and value for option {0}"
msgstr ""

#: tiramisu/option/baseoption.py:508 tiramisu/option/baseoption.py:524
msgid "malformed requirements expected value must be valid for option {0}: {1}"
msgstr ""

#: tiramisu/option/baseoption.py:538
msgid "malformed requirements for option: {0} action cannot be force_store_value"
msgstr ""

#: tiramisu/option/baseoption.py:546
msgid "malformed requirements for option: {0} inverse must be boolean"
msgstr ""

#: tiramisu/option/baseoption.py:553
msgid "malformed requirements for option: {0} transitive must be boolean"
msgstr ""

#: tiramisu/option/baseoption.py:560
msgid "malformed requirements for option: {0} same_action must be boolean"
msgstr ""

#: tiramisu/option/baseoption.py:567
msgid "malformed requirements for option: \"{0}\" operator must be \"or\" or \"and\""
msgstr ""

#: tiramisu/option/baseoption.py:580
msgid "malformed requirements type for option: {0}, must be a dict"
msgstr ""

#: tiramisu/option/baseoption.py:586
msgid "malformed requirements for option: {0} unknown keys {1}, must only {2}"
msgstr ""

#: tiramisu/option/baseoption.py:598
msgid "malformed requirements for option: {0} require must have option, expected and action keys"
msgstr ""

#: tiramisu/option/booloption.py:31
msgid "boolean"
msgstr ""

#: tiramisu/option/broadcastoption.py:32
msgid "broadcast address"
msgstr ""

#: tiramisu/option/broadcastoption.py:39 tiramisu/option/dateoption.py:38
#: tiramisu/option/domainnameoption.py:126 tiramisu/option/ipoption.py:83
#: tiramisu/option/netmaskoption.py:42 tiramisu/option/networkoption.py:68
#: tiramisu/option/passwordoption.py:39 tiramisu/option/portoption.py:107
#: tiramisu/option/urloption.py:41
msgid "invalid string"
msgstr ""

#: tiramisu/option/broadcastoption.py:57
msgid "invalid broadcast consistency, a network and a netmask are needed"
msgstr ""

#: tiramisu/option/broadcastoption.py:62
msgid "broadcast \"{4}\" invalid with network {0}/{1} (\"{2}\"/\"{3}\")"
msgstr ""

#: tiramisu/option/choiceoption.py:37
msgid "choice"
msgstr ""

#: tiramisu/option/choiceoption.py:66
msgid "values is not a function, so values_params must be None"
msgstr ""

#: tiramisu/option/choiceoption.py:68
msgid "values must be a tuple or a function for {0}"
msgstr ""

#: tiramisu/option/choiceoption.py:108
msgid "calculated values for {0} is not a list"
msgstr ""

#: tiramisu/option/choiceoption.py:123
msgid "only \"{0}\" is allowed"
msgstr ""

#: tiramisu/option/choiceoption.py:126
msgid "only {0} are allowed"
msgstr ""

#: tiramisu/option/dateoption.py:31
msgid "date"
msgstr ""

#: tiramisu/option/domainnameoption.py:38
msgid "domain name"
msgstr ""

#: tiramisu/option/domainnameoption.py:59
msgid "unknown type_ {0} for hostname"
msgstr ""

#: tiramisu/option/domainnameoption.py:62
msgid "allow_ip must be a boolean"
msgstr ""

#: tiramisu/option/domainnameoption.py:64
msgid "allow_without_dot must be a boolean"
msgstr ""

#: tiramisu/option/domainnameoption.py:73
msgid "only lowercase, number, \"-\" and \".\" are characters are allowed"
msgstr ""

#: tiramisu/option/domainnameoption.py:74
msgid "only lowercase, number, \"-\" and \".\" are characters are recommanded"
msgstr ""

#: tiramisu/option/domainnameoption.py:77
msgid "only lowercase, number and - are characters are allowed"
msgstr ""

#: tiramisu/option/domainnameoption.py:78
msgid "only lowercase, number and \"-\" are characters are recommanded"
msgstr ""

#: tiramisu/option/domainnameoption.py:80
#: tiramisu/option/domainnameoption.py:81
msgid "could be a IP, otherwise {}"
msgstr ""

#: tiramisu/option/domainnameoption.py:120
msgid "invalid length (min 1)"
msgstr ""

#: tiramisu/option/domainnameoption.py:122
msgid "invalid length (max {0})"
msgstr ""

#: tiramisu/option/domainnameoption.py:133
msgid "must not be an IP"
msgstr ""

#: tiramisu/option/domainnameoption.py:139
msgid "must have dot"
msgstr ""

#: tiramisu/option/domainnameoption.py:141
msgid "invalid length (max 255)"
msgstr ""

#: tiramisu/option/domainnameoption.py:149
msgid "some characters are uppercase"
msgstr ""

#: tiramisu/option/dynoptiondescription.py:56
msgid "cannot set optiondescription in a dynoptiondescription"
msgstr ""

#: tiramisu/option/dynoptiondescription.py:61
msgid "cannot set symlinkoption in a dynoptiondescription"
msgstr ""

#: tiramisu/option/dynoptiondescription.py:72
msgid "callback is mandatory for the dynoptiondescription \"{}\""
msgstr ""

#: tiramisu/option/dynoptiondescription.py:86
msgid "DynOptionDescription callback for option \"{}\", is not a list ({})"
msgstr ""

#: tiramisu/option/dynoptiondescription.py:92
msgid "invalid suffix \"{}\" for option \"{}\""
msgstr ""

#: tiramisu/option/dynoptiondescription.py:101
msgid "DynOptionDescription callback return a list with multiple value \"{}\""
msgstr ""

#: tiramisu/option/emailoption.py:32
msgid "email address"
msgstr ""

#: tiramisu/option/filenameoption.py:31
msgid "file name"
msgstr ""

#: tiramisu/option/floatoption.py:31
msgid "float"
msgstr ""

#: tiramisu/option/intoption.py:31
msgid "integer"
msgstr ""

#: tiramisu/option/intoption.py:55
msgid "value must be greater than \"{0}\""
msgstr ""

#: tiramisu/option/intoption.py:58
msgid "value must be less than \"{0}\""
msgstr ""

#: tiramisu/option/ipoption.py:36
msgid "IP"
msgstr ""

#: tiramisu/option/ipoption.py:89 tiramisu/option/networkoption.py:74
msgid "must use CIDR notation"
msgstr ""

#: tiramisu/option/ipoption.py:111
msgid "shouldn't be reserved IP"
msgstr ""

#: tiramisu/option/ipoption.py:113
msgid "mustn't be reserved IP"
msgstr ""

#: tiramisu/option/ipoption.py:117
msgid "should be private IP"
msgstr ""

#: tiramisu/option/ipoption.py:119
msgid "must be private IP"
msgstr ""

#: tiramisu/option/ipoption.py:147
msgid "IP not in network \"{0}\" (\"{1}\")"
msgstr ""

#: tiramisu/option/ipoption.py:162
msgid "ip_network needs an IP, a network and a netmask"
msgstr ""

#: tiramisu/option/ipoption.py:169
msgid "IP not in network \"{2}\"/\"{4}\" (\"{3}\"/\"{5}\")"
msgstr ""

#: tiramisu/option/ipoption.py:171
msgid "the network doest not match with IP \"{0}\" (\"{1}\") and network \"{4}\" (\"{5}\")"
msgstr ""

#: tiramisu/option/ipoption.py:173
msgid "the netmask does not match with IP \"{0}\" (\"{1}\") and broadcast \"{2}\" (\"{3}\")"
msgstr ""

#: tiramisu/option/leadership.py:56
msgid "a leader and a follower are mandatories in leadership \"{}\""
msgstr ""

#: tiramisu/option/leadership.py:62
msgid "leadership \"{0}\" shall not have a symlinkoption"
msgstr ""

#: tiramisu/option/leadership.py:65
msgid "leadership \"{0}\" shall not have a subgroup"
msgstr ""

#: tiramisu/option/leadership.py:68
msgid "only multi option allowed in leadership \"{0}\" but option \"{1}\" is not a multi"
msgstr ""

#: tiramisu/option/leadership.py:73
msgid "not allowed default value for follower option \"{0}\" in leadership \"{1}\""
msgstr ""

#: tiramisu/option/leadership.py:89
msgid "callback of leader's option shall not refered to a follower's ones"
msgstr ""

#: tiramisu/option/leadership.py:97
msgid "leader {} have requirement, but Leadership {} too"
msgstr ""

#: tiramisu/option/leadership.py:112
msgid "malformed requirements option \"{0}\" must not be in follower for \"{1}\""
msgstr ""

#: tiramisu/option/netmaskoption.py:35
msgid "netmask address"
msgstr ""

#: tiramisu/option/netmaskoption.py:60
msgid "network_netmask needs a network and a netmask"
msgstr ""

#: tiramisu/option/netmaskoption.py:69
msgid "the netmask \"{0}\" (\"{1}\") does not match"
msgstr ""

#: tiramisu/option/netmaskoption.py:72
msgid "the network \"{0}\" (\"{1}\") does not match"
msgstr ""

#: tiramisu/option/netmaskoption.py:83
msgid "ip_netmask needs an IP and a netmask"
msgstr ""

#: tiramisu/option/netmaskoption.py:91
msgid "this is a network with netmask \"{0}\" (\"{1}\")"
msgstr ""

#: tiramisu/option/netmaskoption.py:95
msgid "this is a broadcast with netmask \"{0}\" (\"{1}\")"
msgstr ""

#: tiramisu/option/netmaskoption.py:100
msgid "IP \"{0}\" (\"{1}\") is the network"
msgstr ""

#: tiramisu/option/netmaskoption.py:104
msgid "IP \"{0}\" (\"{1}\") is the broadcast"
msgstr ""

#: tiramisu/option/networkoption.py:32
msgid "network address"
msgstr ""

#: tiramisu/option/networkoption.py:91
msgid "shouldn't be reserved network"
msgstr ""

#: tiramisu/option/networkoption.py:93
msgid "mustn't be reserved network"
msgstr ""

#: tiramisu/option/option.py:78
msgid "default_multi is set whereas multi is False in option: {0}"
msgstr ""

#: tiramisu/option/option.py:95
msgid "invalid multi type \"{}\""
msgstr ""

#: tiramisu/option/option.py:118
msgid "unique must be a boolean, not \"{}\""
msgstr ""

#: tiramisu/option/option.py:120
msgid "unique must be set only with multi value"
msgstr ""

#: tiramisu/option/option.py:131
msgid "invalid default_multi value {0} for option {1}: {2}"
msgstr ""

#: tiramisu/option/option.py:137
msgid "invalid default_multi value \"{0}\" for option \"{1}\", must be a list for a submulti"
msgstr ""

#: tiramisu/option/option.py:259
msgid "invalid value \"{}\", this value is already in \"{}\""
msgstr ""

#: tiramisu/option/option.py:289
msgid "which must not be a list"
msgstr ""

#: tiramisu/option/option.py:323 tiramisu/option/option.py:332
msgid "which must be a list"
msgstr ""

#: tiramisu/option/option.py:337
msgid "which \"{}\" must be a list of list"
msgstr ""

#: tiramisu/option/option.py:379
msgid "default value not allowed if option \"{0}\" is calculated"
msgstr ""

#: tiramisu/option/option.py:427
msgid "'{0}' ({1}) cannot add consistency, option is read-only"
msgstr ""

#: tiramisu/option/option.py:435
msgid "consistency {0} not available for this option"
msgstr ""

#: tiramisu/option/option.py:442
msgid "unknown parameter {0} in consistency"
msgstr ""

#: tiramisu/option/option.py:554 tiramisu/option/option.py:559
msgid "cannot add consistency with submulti option"
msgstr ""

#: tiramisu/option/option.py:560
msgid "consistency must be set with an option, not {}"
msgstr ""

#: tiramisu/option/option.py:563 tiramisu/option/option.py:571
msgid "almost one option in consistency is in a dynoptiondescription but not all"
msgstr ""

#: tiramisu/option/option.py:567
msgid "option in consistency must be in same dynoptiondescription"
msgstr ""

#: tiramisu/option/option.py:574
msgid "cannot add consistency with itself"
msgstr ""

#: tiramisu/option/option.py:576
msgid "every options in consistency must be multi or none"
msgstr ""

#: tiramisu/option/option.py:616
msgid "unexpected length of \"{}\" in constency \"{}\", should be \"{}\""
msgstr ""

#: tiramisu/option/option.py:715
msgid "should be different from the value of {}"
msgstr ""

#: tiramisu/option/option.py:717
msgid "must be different from the value of {}"
msgstr ""

#: tiramisu/option/option.py:720
msgid "value for {} should be different"
msgstr ""

#: tiramisu/option/option.py:722
msgid "value for {} must be different"
msgstr ""

#: tiramisu/option/optiondescription.py:63
#: tiramisu/option/optiondescription.py:181
msgid "option description seems to be part of an other config"
msgstr ""

#: tiramisu/option/optiondescription.py:87
msgid "the follower \"{0}\" cannot have \"force_store_value\" property"
msgstr ""

#: tiramisu/option/optiondescription.py:91
msgid "the dynoption \"{0}\" cannot have \"force_store_value\" property"
msgstr ""

#: tiramisu/option/optiondescription.py:99 tiramisu/setting.py:689
msgid "a leader ({0}) cannot have \"force_default_on_freeze\" or \"force_metaconfig_on_freeze\" property without \"frozen\""
msgstr ""

#: tiramisu/option/optiondescription.py:108
msgid "malformed consistency option \"{0}\" must be in same leadership"
msgstr ""

#: tiramisu/option/optiondescription.py:116
msgid "malformed consistency option \"{0}\" must not be a multi for \"{1}\""
msgstr ""

#: tiramisu/option/optiondescription.py:120
msgid "malformed consistency option \"{0}\" must be in same leadership as \"{1}\""
msgstr ""

#: tiramisu/option/optiondescription.py:151
msgid "malformed requirements option \"{0}\" must be in same leadership for \"{1}\""
msgstr ""

#: tiramisu/option/optiondescription.py:155
msgid "malformed requirements option \"{0}\" must not be a multi for \"{1}\""
msgstr ""

#: tiramisu/option/optiondescription.py:159
msgid "duplicate option: {0}"
msgstr ""

#: tiramisu/option/optiondescription.py:170
msgid "consistency with option {0} which is not in Config"
msgstr ""

#: tiramisu/option/optiondescription.py:225
msgid "unknown option \"{0}\" in optiondescription \"{1}\""
msgstr ""

#: tiramisu/option/optiondescription.py:279
msgid "children in optiondescription \"{}\" must be a list"
msgstr ""

#: tiramisu/option/optiondescription.py:303
msgid "duplicate option name: \"{0}\""
msgstr ""

#: tiramisu/option/optiondescription.py:308
msgid "the option's name \"{}\" start as the dynoptiondescription's name \"{}\""
msgstr ""

#: tiramisu/option/optiondescription.py:333
msgid "cannot change group_type if already set (old {0}, new {1})"
msgstr ""

#: tiramisu/option/optiondescription.py:337
msgid "group_type: {0} not allowed"
msgstr ""

#: tiramisu/option/passwordoption.py:32
msgid "password"
msgstr ""

#: tiramisu/option/portoption.py:44
msgid "port"
msgstr ""

#: tiramisu/option/portoption.py:81
msgid "inconsistency in allowed range"
msgstr ""

#: tiramisu/option/portoption.py:86
msgid "max value is empty"
msgstr ""

#: tiramisu/option/portoption.py:111
msgid "range must have two values only"
msgstr ""

#: tiramisu/option/portoption.py:113
msgid "first port in range must be smaller than the second one"
msgstr ""

#: tiramisu/option/portoption.py:123
msgid "must be an integer between {0} and {1}"
msgstr ""

#: tiramisu/option/stroption.py:33
msgid "string"
msgstr ""

#: tiramisu/option/symlinkoption.py:37
msgid "malformed symlinkoption must be an option for symlink {0}"
msgstr ""

#: tiramisu/option/syndynoptiondescription.py:68
msgid "unknown option \"{0}\" in syndynoptiondescription \"{1}\""
msgstr ""

#: tiramisu/option/urloption.py:34
msgid "URL"
msgstr ""

#: tiramisu/option/urloption.py:44
msgid "must start with http:// or https://"
msgstr ""

#: tiramisu/option/urloption.py:62
msgid "port must be an between 0 and 65536"
msgstr ""

#: tiramisu/option/urloption.py:71
msgid "must ends with a valid resource name"
msgstr ""

#: tiramisu/option/usernameoption.py:32
msgid "username"
msgstr ""

#: tiramisu/setting.py:262
msgid "can't rebind {0}"
msgstr ""

#: tiramisu/setting.py:267
msgid "can't unbind {0}"
msgstr ""

#: tiramisu/setting.py:541
msgid "malformed requirements imbrication detected for option: '{0}' with requirement on: '{1}'"
msgstr ""

#: tiramisu/setting.py:604
msgid "cannot access to option \"{0}\" because required option \"{1}\" has {2} {3}"
msgstr ""

#: tiramisu/setting.py:632
msgid "the calculated value is {0}"
msgstr ""

#: tiramisu/setting.py:634
msgid "the calculated value is not {0}"
msgstr ""

#: tiramisu/setting.py:638
msgid "the value of \"{0}\" is {1}"
msgstr ""

#: tiramisu/setting.py:640
msgid "the value of \"{0}\" is not {1}"
msgstr ""

#: tiramisu/setting.py:679
msgid "cannot set property {} for option \"{}\" this property is calculated"
msgstr ""

#: tiramisu/setting.py:684
msgid "can't assign property to the symlinkoption \"{}\""
msgstr ""

#: tiramisu/setting.py:716
msgid "permissive must be a frozenset"
msgstr ""

#: tiramisu/setting.py:720
msgid "can't assign permissive to the symlinkoption \"{}\""
msgstr ""

#: tiramisu/setting.py:727
msgid "cannot add those permissives: {0}"
msgstr ""

#: tiramisu/setting.py:744
msgid "can't reset properties to the symlinkoption \"{}\""
msgstr ""

#: tiramisu/setting.py:759
msgid "can't reset permissives to the symlinkoption \"{}\""
msgstr ""

#: tiramisu/storage/__init__.py:61
msgid "cannot import the storage {0}"
msgstr ""

#: tiramisu/storage/__init__.py:73
msgid "storage_type is already set, cannot rebind it"
msgstr ""

#: tiramisu/storage/dictionary/storage.py:44
#: tiramisu/storage/sqlite3/storage.py:129
msgid "session \"{}\" already exists"
msgstr ""

#: tiramisu/storage/dictionary/storage.py:46
msgid "a dictionary cannot be persistent"
msgstr ""

#: tiramisu/storage/dictionary/value.py:265
#: tiramisu/storage/sqlite3/value.py:213
msgid "information's item not found {0}"
msgstr ""

#: tiramisu/storage/dictionary/value.py:284
msgid "cannot delete none persistent session"
msgstr ""

#: tiramisu/storage/sqlite3/storage.py:44
msgid "cannot change setting when connexion is already opened"
msgstr ""

#: tiramisu/todict.py:67 tiramisu/todict.py:563
msgid "context is not supported from now for {}"
msgstr ""

#: tiramisu/todict.py:345
msgid "option {} only works when remotable is not \"none\""
msgstr ""

#: tiramisu/todict.py:489
msgid "unable to transform tiramisu object to dict: {}"
msgstr ""

#: tiramisu/todict.py:795 tiramisu/todict.py:927
msgid "unknown form {}"
msgstr ""

#: tiramisu/todict.py:840
msgid "not in current area"
msgstr ""

#: tiramisu/todict.py:860
msgid "only multi option can have action \"add\", but \"{}\" is not a multi"
msgstr ""

#: tiramisu/todict.py:862
msgid "unknown action"
msgstr ""

#: tiramisu/value.py:425
msgid "can't set owner for the symlinkoption \"{}\""
msgstr ""

#: tiramisu/value.py:428 tiramisu/value.py:638
msgid "set owner \"{0}\" is forbidden"
msgstr ""

#: tiramisu/value.py:431
msgid "no value for {0} cannot change owner to {1}"
msgstr ""

#: tiramisu/value.py:509
msgid "index {} is greater than the length {} for option \"{}\""
msgstr ""

